import { NgModule } from "@angular/core";
import { Routes, RouterModule, UrlSegment } from '@angular/router';
import { RegistrationComponent } from "./components/registration/registration.component";
import { RegistrationVerificationComponent } from "./components/registration-verification/registration-verification.component";
import { HomeComponent } from "./components/home/home.component";
import { AuthGuardService } from "./services/auth/auth-guard/auth-guard.service";
import { PasswordResetComponent } from "./components/password-reset/password-reset.component";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { ProfileCreateComponent } from "./components/profile-create/profile-create.component";
import { LandingComponent } from "./components/landing/landing.component";
import { ProviderCreateComponent } from "./components/provider-create/provider-create.component";
import { PermissionGuardService } from "./services/auth/permission-guard/permission-guard.service";
import { ChangePasswordComponent } from "./components/change-password/change-password.component";
import { ProfileShowComponent } from "./components/profile-show/profile-show.component";
import { ProfileGuardService } from "./services/profile/profile-guard/profile-guard.service";
import { ProfileEditComponent } from "./components/profile-edit/profile-edit.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { UserRolesComponent } from "./components/user-roles/user-roles.component";
import { SystemPermissionsComponent } from "./components/system-permissions/system-permissions.component";
import { AccountGroupsComponent } from "./components/account-groups/account-groups.component";
import { GroupHomeViewComponent } from "./components/group-home-view/group-home-view.component";
import { GroupUsersComponent } from "./components/group-users/group-users.component";
import { GroupPermissionsComponent } from "./components/group-permissions/group-permissions.component";
import { UsersComponent } from "./components/users/users.component";
import { UsersUserComponent } from "./components/users-user/users-user.component";
import { ServiceCategoriesComponent } from "./components/service-categories/service-categories.component";
import { ProviderServicesComponent } from "./components/provider-services/provider-services.component";
import { InsurancesComponent } from "./components/insurances/insurances.component";
import { AgeGroupsComponent } from "./components/age-groups/age-groups.component";
import { ProvidersComponent } from "./components/providers/providers.component";
import { ProviderInfoCreateComponent } from "./components/provider-info-create/provider-info-create.component";
import { ProvidersProviderHomeComponent } from "./components/providers-provider-home/providers-provider-home.component";
import { ProvidersProviderInfoShowComponent } from "./components/providers-provider-info-show/providers-provider-info-show.component";
import { ProviderFilesComponent } from "./components/provider-files/provider-files.component";
import { ProviderNotesComponent } from "./components/provider-notes/provider-notes.component";
import { EventsComponent } from "./components/events/events.component";
import { EventViewComponent } from "./components/events/event-view.component";
import { PublicQueryComponent } from "./components/public-query/public-query.component";
import { SystemSettingsComponent } from "./components/system-settings/system-settings.component";
import { ProvidersProviderLocationAddComponent } from "./components/providers-provider-location-add/providers-provider-location-add.component";
import { ProviderGuardService } from "./services/provider/provider-guard/provider-guard.service";
import { ProviderCreateGuardService } from "./services/provider/provider-create-guard/provider-create-guard.service";
import { UserProviderShowComponent } from "./components/user-provider-show/user-provider-show.component";
import { UserProvidersHomeComponent } from "./components/user-providers-home/user-providers-home.component";
import { UserProvidersComponent } from "./components/user-providers/user-providers.component";
import { RegionsComponent } from "./components/regions/regions.component";
import { ServiceDeliveriesComponent } from "./components/service-deliveries/service-deliveries.component";
import { ProviderAuditComponent } from "./components/provider-audit/provider-audit.component";
import { ProviderLinkAccountsComponent } from "./components/provider-link-accounts/provider-link-accounts.component";
import { MessagingComponent } from "./components/messaging/messaging.component";
import { QueryLocationShowComponent } from "./components/query-location-show/query-location-show.component";
import { ApprovalsHomeComponent } from "./components/approvals-home/approvals-home.component";
import { UserProviderLocationAddComponent } from "./components/user-provider-location-add/user-provider-location-add.component";
import { ProviderWaitTimesComponent } from "./components/provider-wait-times/provider-wait-times.component";
import { ApprovalModifiedProviderComponent } from "./components/approval-modified-provider/approval-modified-provider.component";
import { PublicHolderComponent } from "./components/public-holder/public-holder.component";
import { AboutPageComponent } from "./components/about-page/about-page.component";
import { ReportsComponent } from "./components/reports/reports.component";

const routes: Routes = [
  {
    path: "login",
    component: LandingComponent,
  },
  {
    path: "public/provider/:providerInfoId",
    component: PublicHolderComponent,
    children: [
      {
        path: "",
        redirectTo: "files",
        pathMatch: "full",
      },
      {
        path: "files",
        component: ProviderFilesComponent,
      },
    ],
  },

  {
    path: "",
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: "",
        redirectTo: "profile/show",
        pathMatch: "full",
      },

      {
        path: "admin/provider/:providerInfoId",
        component: ProvidersProviderHomeComponent,
        canActivate: [AuthGuardService],
        data: {
          expectedPermissions: ["PERM_VIEW_PROVIDER_INFO"],
        },
        children: [
          {
            path: "",
            redirectTo: "show",
            pathMatch: "full",
          },
          { path: "show", component: ProvidersProviderInfoShowComponent },
          {
            path: "addlocation",
            component: ProvidersProviderLocationAddComponent,
          },
          { path: "files", component: ProviderFilesComponent },
          { path: "notes", component: ProviderNotesComponent },
          { path: "waittimes", component: ProviderWaitTimesComponent },
          { path: "audits", component: ProviderAuditComponent },
          { path: "accounts", component: ProviderLinkAccountsComponent },
        ],
      },
      { path: "profile/create", component: ProfileCreateComponent },
      {
        path: "profile/show",
        canActivate: [ProfileGuardService],
        component: ProfileShowComponent,
      },
      {
        path: "profile/edit",
        canActivate: [ProfileGuardService],
        component: ProfileEditComponent,
      },
      {
        path: "provider/create",
        component: ProviderCreateComponent,
        canActivate: [ProviderCreateGuardService],
      },
      {
        path: "providers",
        component: UserProvidersComponent,
        canActivate: [ProviderGuardService],
      },
      // {
      //   path: "events",
      //   component: EventsComponent
      //   // canActivate: [ProviderGuardService]
      // },
      {
        path: "user/provider/:providerInfoId",
        component: UserProvidersHomeComponent,
        canActivate: [ProviderGuardService],
        children: [
          {
            path: "",
            redirectTo: "show",
            pathMatch: "full",
          },
          {
            path: "show",
            component: UserProviderShowComponent,
          },
          {
            path: "files",
            component: ProviderFilesComponent,
          },
          {
            path: "notes",
            component: ProviderNotesComponent,
          },
          {
            path: "waittimes",
            component: ProviderWaitTimesComponent,
          },
          {
            path: "accounts",
            component: ProviderLinkAccountsComponent,
          },
          {
            path: "addlocation",
            component: UserProviderLocationAddComponent,
          },
        ],
      },
    ],
  },
  { path: "register", component: RegistrationComponent },
  {
    path: "register/verification/:result",
    component: RegistrationVerificationComponent,
  },
  {
    path: "login",
    component: LandingComponent,
  },
  { path: "forgotpassword", component: ForgotPasswordComponent },
  {
    path: "password/reset",
    component: PasswordResetComponent,
  },
  {
    path: "changepassword",
    component: ChangePasswordComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "admin/roles/view",
    component: UserRolesComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_SYSTEM_ROLES"],
    },
  },
  {
    path: "admin/permissions/view",
    component: SystemPermissionsComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_SYSTEM_PERMISSIONS"],
    },
  },
  {
    path: "admin/groups/view",
    component: AccountGroupsComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_GROUPS"],
    },
  },
  {
    path: "admin/users/view",
    component: UsersComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_USERS"],
    },
  },
  {
    path: "admin/users/user/:username",
    component: UsersUserComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_USERS"],
    },
  },
  {
    path: "admin/group/:accountGroup",
    component: GroupHomeViewComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_GROUPS"],
    },
    children: [
      {
        path: "",
        redirectTo: "users",
        pathMatch: "full",
      },
      { path: "users", component: GroupUsersComponent },
      {
        path: "permissions",
        component: GroupPermissionsComponent,
      },
    ],
  },
  {
    path: "admin/service/categories",
    component: ServiceCategoriesComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_SERVICE_CATEGORIES"],
    },
  },
  {
    path: "admin/service/deliveries",
    component: ServiceDeliveriesComponent,
  },
  {
    path: "admin/services",
    component: ProviderServicesComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_SERVICES"],
    },
  },
  {
    path: "admin/insurances",
    component: InsurancesComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_INSURANCES"],
    },
  },
  {
    path: "admin/agegroups",
    component: AgeGroupsComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_AGE_GROUPS"],
    },
  },
  {
    path: "navigator-admin/providers",
    component: ProvidersComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_PROVIDER_INFO"],
    },
  },
  {
    path: "admin-navigator/provider/create",
    component: ProviderInfoCreateComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_CREATE_OR_MODIFY_UNAPPROVED_PROVIDER_INFO"],
    },
  },
  {
    path: "admin/provider/:providerInfoId",
    component: ProvidersProviderHomeComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_VIEW_PROVIDER_INFO"],
    },
    children: [
      {
        path: "",
        redirectTo: "show",
        pathMatch: "full",
      },
      { path: "show", component: ProvidersProviderInfoShowComponent },
      {
        path: "addlocation",
        component: ProvidersProviderLocationAddComponent,
      },
      { path: "files", component: ProviderFilesComponent },
      { path: "notes", component: ProviderNotesComponent },
      { path: "waittimes", component: ProviderWaitTimesComponent },
      { path: "audits", component: ProviderAuditComponent },
      { path: "accounts", component: ProviderLinkAccountsComponent },
    ],
  },
  {
    path: "admin/about",
    component: AboutPageComponent,
  },
  // {
  //   path: "events/:eventId",
  //   component: EventViewComponent
  //   // canActivate: [ProviderGuardService]
  // },
  {
    path: "providers/search",
    component: PublicQueryComponent,
  },
  {
    path: "admin/settings",
    component: SystemSettingsComponent,
  },
  {
    path: "regions",
    component: RegionsComponent,
  },
  {
    path: "approvals",
    component: ApprovalsHomeComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_PROCESS_APPROVALS"],
    },
  },
  {
    path: "reports",
    component: ReportsComponent,
  },
  {
    path: "approval/modified/provider/:providerInfoId",
    component: ApprovalModifiedProviderComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedPermissions: ["PERM_PROCESS_APPROVALS"],
    },
  },
  {
    path: "query/provider/location/:locationId",
    component: QueryLocationShowComponent,
  },
  {
    path: "user/messages",
    component: MessagingComponent,
  },
  { path: "404", component: NotFoundComponent },
  { path: "**", redirectTo: "/404" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
