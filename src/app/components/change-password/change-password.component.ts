import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PasswordValidator } from "../registration/validators/password.validator";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { Router } from "@angular/router";
import { ProfileShowService } from "src/app/services/profile/profile-show/profile-show.service";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { environment as ENV } from "../../../environments/environment";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"]
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  serverMessage: string;
  accountRole: string;
  firstName: string;
  lastName: string;
  imageBuffer: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private profileShowService: ProfileShowService,
    private formBuilder: FormBuilder,
    private formTrimService: FormTrimService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ["", [Validators.required]],
      newPassword: [
        "",
        [Validators.required, Validators.minLength(8), PasswordValidator.strong]
      ],
      confirmNewPassword: ["", [Validators.required]]
    });

    if (this.auth.getRole()) {
      this.accountRole = this.formTrimService.processNamesToDisplay(
        this.auth.getRole(),
        5
      );
    }

    this.profileShowService.getProfile().subscribe(
      response => {
        if (!response) {
          this.firstName = "New";
          this.lastName = "User";
          this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;
        } else {
          if (response.image) {
            this.imageBuffer = response.image;
          } else {
            this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;
          }

          this.firstName = response.firstName;
          this.lastName = response.lastName;
        }
      },
      error => {
        this.auth.logout();
        throw error;
      }
    );
  }

  onSubmit(changePasswordForm: FormGroup): void {
    if (changePasswordForm.valid && this.passwordsMatch) {
      const {
        oldPassword,
        newPassword,
        confirmNewPassword
      } = changePasswordForm.value;

      this.auth
        .changeAccountPassword(oldPassword, newPassword, confirmNewPassword)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            this.router.navigate(["/"]);
          },
          error => {
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.errorAlert.fire();
    }
  }

  get oldPassword() {
    return this.changePasswordForm.get("oldPassword");
  }

  get newPassword() {
    return this.changePasswordForm.get("newPassword");
  }

  get confirmNewPassword() {
    return this.changePasswordForm.get("confirmNewPassword");
  }

  get passwordsMatch() {
    return (
      this.changePasswordForm.get("newPassword").value ===
      this.changePasswordForm.get("confirmNewPassword").value
    );
  }
}
