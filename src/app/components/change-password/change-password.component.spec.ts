import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ChangePasswordComponent } from "./change-password.component";
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ProfileShowComponent } from '../profile-show/profile-show.component';
import { ProfileShowService } from 'src/app/services/profile/profile-show/profile-show.service';
import { of } from 'rxjs';

describe("ChangePasswordComponent", () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  let injectedAuthService: AuthService;
  let injectedProfileShowService: ProfileShowService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        ProfileShowService,
        { provide: Router, useValue: mockRouter },
        FormBuilder
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [ChangePasswordComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    injectedAuthService = TestBed.inject(AuthService);
    injectedProfileShowService = TestBed.inject(ProfileShowService);
    spyOn(injectedAuthService, 'getRole').and.returnValue('role1234_role5678');
    spyOn(injectedProfileShowService, 'getProfile').and.returnValue(of<any>());
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
