import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-registration-verification",
  templateUrl: "./registration-verification.component.html",
  styleUrls: ["./registration-verification.component.scss"]
})
export class RegistrationVerificationComponent implements OnInit {
  result: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.result = this.route.snapshot.params["result"];

    this.route.params.subscribe((params: Params) => {
      this.result = params["result"];
    });
  }
}
