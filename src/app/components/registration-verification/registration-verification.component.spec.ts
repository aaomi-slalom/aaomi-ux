import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegistrationVerificationComponent } from "./registration-verification.component";
import { FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

describe("RegistrationVerificationComponent", () => {
  let component: RegistrationVerificationComponent;
  let fixture: ComponentFixture<RegistrationVerificationComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const fakeActivatedRoute = {
    snapshot: {
      params: {
        result: 'some result'
      }
    },
    params: of<Params>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
        FormBuilder,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute }
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      declarations: [RegistrationVerificationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
