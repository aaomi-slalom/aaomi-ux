import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AgeGroupService } from 'src/app/services/age-group/age-group.service';

import { AgeGroupsComponent } from './age-groups.component';

describe('AgeGroupsComponent', () => {
  let component: AgeGroupsComponent;
  let fixture: ComponentFixture<AgeGroupsComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgeGroupsComponent],
      providers: [AgeGroupService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
