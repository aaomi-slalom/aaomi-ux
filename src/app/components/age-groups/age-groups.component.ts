import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faEdit,
  faPlus,
  faEye,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";
import { IntegerValidator } from "./validators/integer.validator";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-age-groups",
  templateUrl: "./age-groups.component.html",
  styleUrls: ["./age-groups.component.scss"]
})
export class AgeGroupsComponent implements OnInit {
  createAgeGroupForm: FormGroup;
  editAgeGroupForm: FormGroup;
  faEye = faEye;
  faEdit = faEdit;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  ageGroupDtos = [];
  errorMessage: string;
  serverMessage: string;
  editPrevAgeStart: number;
  editPrevAgeEnd: number;
  editAgeGroupId: number;
  ageGroupIdToDelete: number;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("editAgeGroupModal") editAgeGroupModal: SwalComponent;
  @ViewChild("confirmDeleteAgeGroupAlert")
  confirmDeleteAgeGroupAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private ageGroupService: AgeGroupService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.ageGroupService.getAllAgeGroups().subscribe(ageGroupDtos => {
      this.ageGroupDtos = ageGroupDtos;
    });

    this.createAgeGroupForm = this.formBuilder.group({
      ageStart: [
        null,
        [
          Validators.required,
          IntegerValidator.integer,
          Validators.min(0),
          Validators.max(255)
        ]
      ],
      ageEnd: [
        null,
        [
          Validators.required,
          IntegerValidator.integer,
          Validators.min(0),
          Validators.max(255)
        ]
      ]
    });

    this.editAgeGroupForm = this.formBuilder.group({
      editAgeStart: [
        null,
        [
          Validators.required,
          IntegerValidator.integer,
          Validators.min(0),
          Validators.max(255)
        ]
      ],
      editAgeEnd: [
        null,
        [
          Validators.required,
          IntegerValidator.integer,
          Validators.min(0),
          Validators.max(255)
        ]
      ]
    });
  }

  createAgeGroup(): void {
    if (this.createAgeGroupForm.valid && this.ageEndGreater) {
      const { ageStart, ageEnd } = this.createAgeGroupForm.value;
      this.ageGroupService
        .createAgeGroup(ageStart, ageEnd)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create age group...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editAgeGroup(event): void {
    this.editAgeGroupId = event.currentTarget.dataset.agegroupid;
    this.editPrevAgeStart = parseInt(event.currentTarget.dataset.agestart);
    this.editPrevAgeEnd = parseInt(event.currentTarget.dataset.ageend);

    this.editAgeStart.setValue(this.editPrevAgeStart);
    this.editAgeEnd.setValue(this.editPrevAgeEnd);
    this.editAgeGroupModal.fire();
  }

  editAgeGroupSubmit(): void {
    if (this.editAgeGroupForm.valid && this.editAgeEndGreater) {
      this.ageGroupService
        .updateAgeGroup(
          this.editAgeGroupId,
          this.editAgeStart.value,
          this.editAgeEnd.value
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update age group...";
            if (error.error.message) {
              this.errorMessage = error.error.message;
            }

            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  cancel(): void {
    Swal.close();
  }

  deleteAgeGroup(event): void {
    this.ageGroupIdToDelete = event.currentTarget.dataset.agegroupid;
    this.confirmDeleteAgeGroupAlert.fire();
  }

  confirmDeleteAgeGroup(): void {
    this.ageGroupService
      .deleteAgeGroup(this.ageGroupIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete age group...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get ageStart() {
    return this.createAgeGroupForm.get("ageStart");
  }

  get ageEnd() {
    return this.createAgeGroupForm.get("ageEnd");
  }

  get ageEndGreater() {
    return this.ageEnd.value - this.ageStart.value >= 0;
  }

  get editAgeStart() {
    return this.editAgeGroupForm.get("editAgeStart");
  }

  get editAgeEnd() {
    return this.editAgeGroupForm.get("editAgeEnd");
  }

  get editAgeEndGreater() {
    return this.editAgeEnd.value - this.editAgeStart.value >= 0;
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
