import { FormControl } from "@angular/forms";

export interface ValidationResult {
  [key: string]: boolean;
}

export class IntegerValidator {
  public static integer(control: FormControl): ValidationResult {
    let isInteger = Number.isInteger(control.value);

    const valid = isInteger || !control.value;

    if (!valid) {
      return { integer: true };
    }

    return null;
  }
}
