import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { PermissionService } from "src/app/services/permission/permission.service";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-system-permissions",
  templateUrl: "./system-permissions.component.html",
  styleUrls: ["./system-permissions.component.scss"]
})
export class SystemPermissionsComponent implements OnInit {
  permissions;

  constructor(
    private auth: AuthService,
    private permissionService: PermissionService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.permissionService.getAllPermissions().subscribe(permissionsRaw => {
      const permissions = permissionsRaw.map(permission => {
        const permissionNameDisplay = this.formTrimService.processNamesToDisplay(
          permission.permission,
          5
        );
        return { ...permission, permissionNameDisplay };
      });

      this.permissions = permissions;
    });
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
