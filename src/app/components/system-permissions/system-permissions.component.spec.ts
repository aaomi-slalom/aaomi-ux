import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SystemPermissionsComponent } from './system-permissions.component';
import { PermissionService } from "src/app/services/permission/permission.service";
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { Router } from '@angular/router';

describe('SystemPermissionsComponent', () => {
  let component: SystemPermissionsComponent;
  let fixture: ComponentFixture<SystemPermissionsComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SystemPermissionsComponent],
      providers: [PermissionService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
