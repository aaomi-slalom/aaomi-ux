import { Component, OnInit, ViewChild } from "@angular/core";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { ServiceDeliveryService } from "src/app/services/service-delivery/service-delivery.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { faEdit, faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { map, flatMap } from "rxjs/operators";
import { timer } from "rxjs";
import Swal from "sweetalert2";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-service-deliveries",
  templateUrl: "./service-deliveries.component.html",
  styleUrls: ["./service-deliveries.component.scss"]
})
export class ServiceDeliveriesComponent implements OnInit {
  createServiceDeliveryForm: FormGroup;
  editServiceDeliveryForm: FormGroup;
  faEdit = faEdit;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  serviceDeliveryDtos: any[];
  errorMessage: string;
  serverMessage: string;
  currServiceDeliveryId: number;
  serviceDeliveryIdToDelete: number;
  editPrevServiceDeliveryName: string;
  @ViewChild("editServiceDeliveryModal")
  editServiceDeliveryModal: SwalComponent;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteServiceDeliveryModal")
  confirmDeleteServiceDeliveryModal: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private serviceDeliveryService: ServiceDeliveryService,
    private auth: AuthService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.serviceDeliveryService
      .getExistingServiceDeliveries()
      .subscribe(serviceDeliveryDto => {
        this.serviceDeliveryDtos = serviceDeliveryDto;
      });

    this.createServiceDeliveryForm = this.formBuilder.group({
      createServiceDeliveryName: ["", [Validators.required]]
    });

    this.editServiceDeliveryForm = this.formBuilder.group({
      editServiceDeliveryName: ["", [Validators.required]]
    });
  }

  createServiceDelivery(): void {
    if (
      this.createServiceDeliveryForm.valid &&
      !this.serviceDeliveryAlreadyExist
    ) {
      const createServiceDeliveryName = this.createServiceDeliveryForm.value;
      this.serviceDeliveryService
        .createNewServiceDelivery(this.createServiceDeliveryName.value.trim())
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to create service delivery...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editServiceDelivery(event): void {
    this.currServiceDeliveryId = event.currentTarget.dataset.servicedeliveryid;
    this.editPrevServiceDeliveryName = event.currentTarget.dataset.delivery;

    this.editServiceDeliveryName.setValue(this.editPrevServiceDeliveryName);
    this.editServiceDeliveryModal.fire();
  }

  editServiceDeliverySubmit(): void {
    if (
      this.editServiceDeliveryForm.valid &&
      !this.editServiceDeliveryAlreadyExists
    ) {
      this.serviceDeliveryService
        .updateExistingServiceDelivery(
          this.currServiceDeliveryId,
          this.editServiceDeliveryName.value.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update service delivery...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  deleteServiceDelivery(event): void {
    this.serviceDeliveryIdToDelete =
      event.currentTarget.dataset.servicedeliveryid;
    this.confirmDeleteServiceDeliveryModal.fire();
  }

  deleteServiceDeliverySubmit(): void {
    this.serviceDeliveryService
      .deleteServiceDelivery(this.serviceDeliveryIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete service delivery...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancel(): void {
    Swal.close();
  }

  get createServiceDeliveryName() {
    return this.createServiceDeliveryForm.get("createServiceDeliveryName");
  }

  get editServiceDeliveryName() {
    return this.editServiceDeliveryForm.get("editServiceDeliveryName");
  }

  get serviceDeliveryAlreadyExist() {
    return this.serviceDeliveryDtos
      .map(serviceDelivery => serviceDelivery.delivery)
      .includes(this.createServiceDeliveryName.value.trim());
  }

  get editServiceDeliveryAlreadyExists() {
    return this.serviceDeliveryDtos
      .filter(
        serviceDelivery =>
          serviceDelivery.delivery !== this.editPrevServiceDeliveryName
      )
      .map(serviceDelivery => serviceDelivery.delivery)
      .includes(this.editServiceDeliveryName.value.trim());
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
