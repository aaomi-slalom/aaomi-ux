import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { ServiceDeliveryService } from "src/app/services/service-delivery/service-delivery.service";
import { ServiceDeliveriesComponent } from "./service-deliveries.component";

describe("ServiceDeliveriesComponent", () => {
  let component: ServiceDeliveriesComponent;
  let fixture: ComponentFixture<ServiceDeliveriesComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceDeliveriesComponent],
      providers: [ServiceDeliveryService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceDeliveriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
