import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { EventsService } from "src/app/services/events/events.service";
import { ActivatedRoute } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UrlValidator } from "../profile-create/validators/url.validator";
import { timer } from "rxjs";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import * as moment from "moment";
import Swal from "sweetalert2";
import { environment as ENV } from "../../../environments/environment";
import {
  faSearch,
  faTimes,
  faTrashAlt,
  faEye
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-events",
  templateUrl: "./events.component.html",
  styleUrls: ["./events.component.scss"]
})
export class EventsComponent implements OnInit {
  filterEventsForm: FormGroup;
  createEventForm: FormGroup;
  imageBuffer: string | ArrayBuffer;
  filteredEvents = [];
  events = [];
  serverMessage: string;
  errorMessage: string;
  faSearch = faSearch;
  faTimes = faTimes;
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  eventIdToDelete;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  moment = moment;
  wait1Second = timer(1000);

  constructor(
    private auth: AuthService,
    private eventsService: EventsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();
    this.imageBuffer = "";

    this.filterEventsForm = this.formBuilder.group({
      source: ["", [Validators.required]]
    });

    this.createEventForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      description: ["", []],
      website: ["", [UrlValidator.isValidUrl]],
      cost: ["", [Validators.required]],
      free: [false, []],
      image: ["", []],
      startDate: [new Date(), [Validators.required]],
      startTime: ["", [Validators.required]],
      endDate: [new Date(), [Validators.required]],
      endTime: ["", [Validators.required]]
    });

    this.route.parent.url
      .pipe(
        flatMap(() => {
          const events = this.eventsService.getAllMyEvents();

          return events;
        })
      )
      .subscribe(results => {
        this.events = results;

        this.filteredEvents = this.events;
      });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  openEventView(event) {
    this.router.navigate(["/events/", event.currentTarget.dataset.eventid]);
  }

  createEvent(): void {
    if (this.createEventForm.valid) {
      let {
        title,
        description,
        website,
        cost,
        free,
        image,
        startDate,
        startTime,
        endDate,
        endTime
      } = this.createEventForm.value;

      image = this.imageBuffer;

      const startDateTime = new Date(
        startDate.getFullYear(),
        startDate.getMonth(),
        startDate.getDate(),
        startTime.getHours(),
        startTime.getMinutes()
      );

      const endDateTime = new Date(
        endDate.getFullYear(),
        endDate.getMonth(),
        endDate.getDate(),
        endTime.getHours(),
        endTime.getMinutes()
      );

      this.eventsService
        .createEvent(
          title,
          description,
          website,
          cost,
          free,
          image,
          startDateTime,
          endDateTime
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create event...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  filterEvents(): void {
    if (this.filterEventsForm.valid) {
      const { source } = this.filterEventsForm.value;
      this.filteredEvents = this.events.filter(
        event => event.source === source
      );
    }
  }

  canModifyEvent(event): boolean {
    return (
      this.auth.hasPermissions(["PERM_MODIFY_EVENTS"]) ||
      this.auth.getUsername() === event.organizer
    );
  }

  canAddEvent(): boolean {
    return this.auth.hasPermissions(["PERM_CREATE_EVENTS"]);
  }

  cancelFilter(): void {
    this.filteredEvents = this.events;
    this.filterEventsForm.reset();
  }

  deleteEvent(event): void {
    this.eventIdToDelete = event.currentTarget.dataset.eventid;

    this.confirmDeleteAlert.fire();
  }

  cancelDelete(): void {
    Swal.close();
  }

  confirmDelete(): void {
    this.eventsService
      .deleteEventByEventId(this.eventIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete event...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get title() {
    return this.createEventForm.get("title");
  }

  get cost() {
    return this.createEventForm.get("cost");
  }

  get image() {
    return this.createEventForm.get("image");
  }

  get startDate() {
    return this.createEventForm.get("startDate");
  }

  get endDate() {
    return this.createEventForm.get("endDate");
  }

  get website() {
    return this.createEventForm.get("website");
  }
}
