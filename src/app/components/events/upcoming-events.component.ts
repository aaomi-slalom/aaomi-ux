import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { EventsService } from "src/app/services/events/events.service";
import { flatMap } from "rxjs/operators";
import * as moment from "moment";
import { faEye } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "upcoming-events",
  templateUrl: "./upcoming-events.component.html",
  styleUrls: ["./upcoming-events.component.scss"]
})
export class UpcomingEventsComponent implements OnInit {
  events = [];
  faEye = faEye;
  moment = moment;

  constructor(
    private eventsService: EventsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.parent.url
      .pipe(
        flatMap(() => {
          const events = this.eventsService.getAllEvents();

          return events;
        })
      )
      .subscribe(results => {
        this.events = results.filter(
          event => new Date(event.startDate) >= new Date()
        );
      });
  }

  openEventView(event) {
    this.router.navigate(["/events/", event.currentTarget.dataset.eventid]);
  }
}
