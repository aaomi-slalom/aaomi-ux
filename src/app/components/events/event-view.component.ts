import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UrlValidator } from '../profile-create/validators/url.validator';
import { EventsService } from 'src/app/services/events/events.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, flatMap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import * as moment from 'moment';
import { timer } from 'rxjs';

@Component({
  selector: 'app-events-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.scss']
})
export class EventViewComponent implements OnInit {
  eventId;
  event;
  canModifyEvent;
  imageBuffer: string | ArrayBuffer;
  updateEventForm: FormGroup;
  serverMessage: string;
  errorMessage: string;
  moment = moment;
  wait1Second = timer(1000);
  @ViewChild('infoAlert') infoAlert: SwalComponent;
  @ViewChild('errorAlert') errorAlert: SwalComponent;

  constructor(
    private auth: AuthService,
    private eventsService: EventsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    // work-around to make this page work for authenticated and anonymous users
    if (this.auth.isAuthenticated()) {
      this.auth.validateCredentialsOnServer();
    }

    this.route.url
    .pipe(
      map(url => (this.eventId = url[1].path)),
      flatMap(() => this.eventsService.getEventByEventId(this.eventId))
    )
    .subscribe(event => {
      this.event = event;
      this.canModifyEvent = this.auth.isAuthenticated() && (this.auth.hasPermissions(['PERM_MODIFY_EVENTS'])
      || (this.auth.getUsername() === this.event.organizer));

      this.imageBuffer = this.event.image;
      this.updateEventForm.patchValue({
        'title': this.event.title,
        'description': this.event.description,
        'website': this.event.website,
        'cost': this.event.cost,
        'free': this.event.free,
        'startDate': new Date(this.event.startDate),
        'startTime': new Date(this.event.startDate),
        'endDate': new Date(this.event.endDate),
        'endTime': new Date(this.event.endDate),
        'organizer': this.event.organizer
      });
    });

    this.updateEventForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      website: ['', [UrlValidator.isValidUrl]],
      cost: ['', [Validators.required]],
      free: [false, []],
      image: ['', []],
      startDate: [new Date(), [Validators.required]],
      startTime: [new Date(), [Validators.required]],
      endDate: [new Date(), [Validators.required]],
      endTime: [new Date(), [Validators.required]],
      organizer: [{value: '', disabled: true}, [Validators.required]]
    });
  }

  updateEvent(): void {
    if (this.updateEventForm.valid) {
      let { title, description, website, cost, free, image, startDate, startTime, endDate, endTime } = this.updateEventForm.value;

      image = this.imageBuffer;

      const startDateTime = new Date(
        startDate.getFullYear(),
        startDate.getMonth(),
        startDate.getDate(),
        startTime.getHours(),
        startTime.getMinutes()
        );

      const endDateTime = new Date(
        endDate.getFullYear(),
        endDate.getMonth(),
        endDate.getDate(),
        endTime.getHours(),
        endTime.getMinutes()
        );

      this.eventsService
        .updateEvent(this.eventId, title, description, website, cost, free, image, startDateTime, endDateTime)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = 'Error when trying to update event...';
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  get title() {
    return this.updateEventForm.get('title');
  }

  get cost() {
    return this.updateEventForm.get('cost');
  }

  get image() {
    return this.updateEventForm.get('image');
  }

  get startDate() {
    return this.updateEventForm.get('startDate');
  }

  get endDate() {
    return this.updateEventForm.get('endDate');
  }
}
