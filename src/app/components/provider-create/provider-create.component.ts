import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  faEdit,
  faPlus,
  faEye,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { timer, forkJoin } from "rxjs";
import Swal from "sweetalert2";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";

import { ContactTypeService } from "src/app/services/contact-type/contact-type.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { environment as ENV } from "../../../environments/environment";
import scus from "state-counties-us";
import { UrlValidator } from "../profile-create/validators/url.validator";
import { CartesianService } from "src/app/services/cartesian/cartesian.service";
import { v4 as uuidv4 } from "uuid";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { map, flatMap } from "rxjs/operators";
import { DeliveryService } from "src/app/services/delivery/delivery.service";
import { Router } from "@angular/router";
import { MiCountyService } from "src/app/services/mi-county/mi-county.service";
import { ZipcodeValidator } from "../provider-info-create/validators/zipcode.validator";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "app-provider-create",
  templateUrl: "./provider-create.component.html",
  styleUrls: ["./provider-create.component.scss"]
})
export class ProviderCreateComponent implements OnInit {
  createProviderForm: FormGroup;
  createDemographicsForm: FormGroup;
  createLocationForm: FormGroup;
  createEmailForm: FormGroup;
  createContactNumberForm: FormGroup;
  createServiceCountyForm: FormGroup;
  createServiceForm: FormGroup;
  emails = [];
  adminUsers = [];
  contactNumbers = [];
  serviceCounties = [];
  services = [];
  locations = [];
  showLocationForm: boolean = false;
  usernameAvailable: boolean = true;
  emailAvailable: boolean = true;
  accountEnabled: boolean = false;
  hasProviderName: boolean = false;
  serviceOptions = [];
  serviceCategoryOptions = [];
  serviceDeliveryOptions = [];
  insuranceOptions = [];
  ageGroupOptions = [];
  contactTypeOptions = [];
  serviceOptionsUnderCategories = [];
  deliverySelectSettings = {};
  insuranceSelectSettings = {};
  ageGroupSelectSettings = {};
  countySelectSettings = {};
  faEye = faEye;
  faEdit = faEdit;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  stateOptions: string[] = [];
  michiganCounties: string[] = [];
  adminUsernames: string[] = [];
  serverMessage: string;
  approvalNotifSubject: string;
  approvalNotifMessage: string;
  errorMessage: string;
  imageBuffer: string | ArrayBuffer;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  @ViewChild("createEmailModal") createEmailModal: SwalComponent;

  showNotSavedLocationId;
  showUnsavedCounties: boolean = false;
  wait1Second = timer(1000);
  JSON = JSON;
  pn = this.usPhoneNumberService.formatPhoneNumber;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "150",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    sanitize: false,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      ["fontName"],
      [
        "insertImage",
        "insertVideo",
        "insertHorizontalRule",
        "backgroundColor",
        "customClasses",
        "fontSize",
        "toggleEditorMode"
      ]
    ]
  };

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private providerService: ProviderService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private insuranceService: InsuranceService,
    private ageGroupService: AgeGroupService,
    private contactTypeService: ContactTypeService,
    private deliveryService: DeliveryService,
    private cartesianService: CartesianService,
    private miCountyService: MiCountyService,
    private usPhoneNumberService: UsPhoneNumberService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.imageBuffer = ENV.EMPTY_LOGO_IMG_URL;

    this.createProviderForm = this.formBuilder.group({
      providerInfo: [null, [Validators.required]],
      providerLocations: [[], [Validators.required]]
    });

    this.createDemographicsForm = this.formBuilder.group({
      providerName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      logo: ["", []],
      description: ["", []],
      facebook: ["", [UrlValidator.isValidUrl]],
      linkedin: ["", [UrlValidator.isValidUrl]],
      twitter: ["", [UrlValidator.isValidUrl]],
      instagram: ["", [UrlValidator.isValidUrl]],
      youtube: ["", [UrlValidator.isValidUrl]]
    });

    this.createLocationForm = this.formBuilder.group({
      locationName: ["", [Validators.required]],
      internalOnly: [false, []],
      serviceStreetAddress: ["", []],
      serviceCity: ["", []],
      serviceState: ["MI", []],
      serviceZip: ["", [ZipcodeValidator.zipcode]],
      serviceAddressSame: [false, []],
      mailingStreetAddress: ["", [Validators.required]],
      mailingCity: ["", [Validators.required]],
      mailingState: ["MI", [Validators.required]],
      mailingZip: ["", [Validators.required, ZipcodeValidator.zipcode]],
      emails: [[], [Validators.required]],
      contactNumbers: [[], [Validators.required]],
      serviceCounties: [[], [Validators.required]],
      services: [[], [Validators.required]]
    });

    this.createEmailForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      contactType: ["", [Validators.required]]
    });

    this.createContactNumberForm = this.formBuilder.group({
      contactNumber: [
        "",
        [Validators.required, Validators.pattern("^\\+?\\d{0,15}")]
      ],
      contactType: ["", [Validators.required]]
    });

    this.createServiceCountyForm = this.formBuilder.group({
      counties: [[], Validators.required]
    });

    this.createServiceForm = this.formBuilder.group({
      service: ["", [Validators.required]],
      serviceDisplay: ["", []],
      serviceCategory: ["", []],
      internalOnly: [false, []],
      serviceDeliveries: [[], [Validators.required]],
      insurances: [[], [Validators.required]],
      ageGroups: [[], [Validators.required]]
    });

    forkJoin([
      this.providerServiceService.getAllPublicServiceOptions(),
      this.insuranceService.getAllInsuranceOptions(),
      this.ageGroupService.getAllAgeGroupOptions(),
      this.contactTypeService.getAllContactTypeOptions(),
      this.serviceCategoryService.getAllServiceCategoryOptions(),
      this.providerServiceService.getAllNonInternalServices(),
      this.deliveryService.getAllDeliveryOptions(),
      this.miCountyService.getAllMICountyOptions()
    ]).subscribe(results => {
      this.serviceOptions = results[0];
      this.insuranceOptions = results[1];
      this.ageGroupOptions = results[2];
      this.contactTypeOptions = results[3];
      this.serviceCategoryOptions = results[4];
      this.serviceOptionsUnderCategories = results[5];
      this.serviceDeliveryOptions = results[6];
      this.michiganCounties = results[7];

      this.deliverySelectSettings = {
        singleSelection: false,
        idField: "serviceDeliveryId",
        textField: "delivery",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.insuranceSelectSettings = {
        singleSelection: false,
        idField: "insuranceId",
        textField: "insuranceName",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.ageGroupSelectSettings = {
        singleSelection: false,
        idField: "serviceAgeGroupId",
        textField: "ageGroupDisplay",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.countySelectSettings = {
        singleSelection: false,
        idField: "countyId",
        textField: "county",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    });

    this.stateOptions = scus.getStates();
  }

  checkProviderName(): void {
    this.providerService
      .checkProviderName(this.providerName.value)
      .subscribe(response => {
        this.hasProviderName = response.hasProviderName;
      });
  }

  cancelCreateLocationForm(): void {
    this.showLocationForm = false;

    this.createLocationForm.reset({
      internalOnly: false,
      serviceAddressSame: false,
      mailingState: "MI",
      serviceState: "MI",
      emails: [],
      contactNumbers: [],
      serviceCounties: [],
      services: []
    });

    this.createServiceCountyForm.reset({ counties: [] });

    this.emails = [];
    this.contactNumbers = [];
    this.serviceCounties = [];
    this.services = [];
  }

  submitCreateLocation(): void {
    const lastIndex = this.locations.length;
    this.locations.push({
      ...this.createLocationForm.value,
      locationId: lastIndex
    });

    this.providerLocations.setValue(this.locations);

    this.createLocationForm.reset({
      internalOnly: false,
      serviceAddressSame: false,
      mailingState: "MI",
      serviceState: "MI",
      emails: [],
      contactNumbers: [],
      serviceCounties: [],
      services: []
    });

    this.createServiceCountyForm.reset({ counties: [] });

    this.emails = [];
    this.contactNumbers = [];
    this.serviceCounties = [];
    this.services = [];

    this.showLocationForm = false;

    this.serverMessage = "Location Added";
    this.infoAlert.fire();
    this.wait1Second.subscribe(() => Swal.close());
  }

  deleteLocation(event): void {
    this.locations = this.locations.filter(
      location => location.locationId != event.currentTarget.dataset.locationid
    );

    this.providerLocations.setValue(this.locations);
  }

  submitCreateEmail(): void {
    const lastIndex = this.emails.length;
    this.emails.push({
      ...this.createEmailForm.value,
      emailId: lastIndex
    });

    this.locationEmails.setValue(this.emails);
    this.createEmailForm.reset();

    Swal.close();
  }

  deleteEmail(event): void {
    this.emails = this.emails.filter(
      email => email.emailId != event.currentTarget.dataset.emailid
    );

    this.locationEmails.setValue(this.emails);
  }

  submitCreateContactNumber(): void {
    const lastIndex = this.contactNumbers.length;
    this.contactNumbers.push({
      ...this.createContactNumberForm.value,
      contactNumberId: lastIndex
    });
    this.locationContactNumbers.setValue(this.contactNumbers);
    this.createContactNumberForm.reset();

    Swal.close();
  }

  deleteContactNumber(event) {
    this.contactNumbers = this.contactNumbers.filter(
      contactNumber =>
        contactNumber.contactNumberId !=
        event.currentTarget.dataset.contactnumberid
    );

    this.locationContactNumbers.setValue(this.contactNumbers);
  }

  submitCreateServiceCounty(): void {
    this.serviceCounties = [...this.counties.value];
    this.locationServiceCounties.setValue(this.serviceCounties);

    Swal.close();
  }

  submitCreateService(): void {
    const serviceOption = this.serviceOptions.find(
      serviceOption => serviceOption.serviceId == this.serviceId.value
    );

    const serviceDisplay = serviceOption.service;
    const serviceCategory = serviceOption.category;

    this.serviceDisplay.setValue(serviceDisplay);
    this.serviceCategory.setValue(serviceCategory);

    const lastIndex = this.services.length;
    this.services.push({
      ...this.createServiceForm.value,
      serviceId: lastIndex
    });
    this.locationServices.setValue(this.services);
    this.createServiceForm.reset();

    Swal.close();
  }

  deleteService(event) {
    this.services = this.services.filter(
      service => service.serviceId != event.currentTarget.dataset.serviceid
    );

    this.locationServices.setValue(this.services);
  }

  markServiceAddressSame(): void {
    this.serviceStreetAddress.setValue("");
    this.serviceCity.setValue("");
    this.serviceState.setValue("MI");
    this.serviceZip.setValue("");

    if (this.serviceAddressSame.value) {
      this.serviceStreetAddress.setValue(this.mailingStreetAddress.value);
      this.serviceCity.setValue(this.mailingCity.value);
      this.serviceState.setValue(this.mailingState.value);
      this.serviceZip.setValue(this.mailingZip.value);
    }
  }

  // demographics
  submitOrEditDemographicsForm(): void {
    this.checkProviderName();

    if (this.createDemographicsForm.valid && !this.hasProviderName) {
      const demographicsValues = { ...this.createDemographicsForm.value };
      demographicsValues.logo = this.imageBuffer;

      this.providerInfo.setValue(demographicsValues);
      Swal.close();
    }
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  // provider
  cancelCreateProviderForm(): void {
    this.createProviderForm.reset({
      providerLocations: []
    });

    this.locations = [];
  }

  submitCreateProvider(): void {
    if (this.createProviderForm.valid) {
      const providerFormValuesRaw = this.createProviderForm.value;

      // providerInfo
      const providerInfoRaw = providerFormValuesRaw.providerInfo;
      const providerInfo = { ...providerInfoRaw };

      // providerLocations
      const providerLocationsRaw = providerFormValuesRaw.providerLocations;
      const providerLocations = providerLocationsRaw.map(
        providerLocationRaw => {
          const providerLocation = {
            locationName: providerLocationRaw.locationName,
            internal: false,
            addresses: [
              {
                addressType: "Mailing",
                city: providerLocationRaw.mailingCity,
                state: providerLocationRaw.mailingState,
                street: providerLocationRaw.mailingStreetAddress,
                zip: providerLocationRaw.mailingZip
              },
              {
                addressType: "Service",
                city: providerLocationRaw.serviceCity,
                state: providerLocationRaw.serviceState,
                street: providerLocationRaw.serviceStreetAddress,
                zip: providerLocationRaw.serviceZip
              }
            ],
            emails: [],
            contactNumbers: [],
            counties: [],
            locationSrvcLinks: []
          };
          //// emails
          const emailsRaw = providerLocationRaw.emails;
          providerLocation.emails = emailsRaw.map(email => {
            return {
              email: email.email,
              contactType: email.contactType
            };
          });

          //// contactNumbers
          const contactNumbersRaw = providerLocationRaw.contactNumbers;
          providerLocation.contactNumbers = contactNumbersRaw.map(
            contactNumber => {
              return {
                number: contactNumber.contactNumber,
                contactType: contactNumber.contactType
              };
            }
          );

          //// serviceCounties
          const countiesRaw = providerLocationRaw.serviceCounties;
          providerLocation.counties = countiesRaw.map(county => {
            return {
              county: county.county
            };
          });

          //// services
          const locationSrvcLinksRaw = providerLocationRaw.services;
          providerLocation.locationSrvcLinks = locationSrvcLinksRaw.flatMap(
            locationSrvcLinkRaw => {
              const locationSrvcLink = {
                serviceId: parseInt(locationSrvcLinkRaw.service),
                internal: false,
                serviceDeliveryIds: [],
                insuranceIds: [],
                ageGroupIds: [],
                identifier: uuidv4()
              };

              locationSrvcLink.serviceDeliveryIds = locationSrvcLinkRaw.serviceDeliveries.map(
                serviceDelivery => serviceDelivery.serviceDeliveryId
              );
              locationSrvcLink.insuranceIds = locationSrvcLinkRaw.insurances.map(
                insurance => insurance.insuranceId
              );
              locationSrvcLink.ageGroupIds = locationSrvcLinkRaw.ageGroups.map(
                ageGroup => ageGroup.serviceAgeGroupId
              );

              return this.cartesianService
                .cartesian(
                  [locationSrvcLink.serviceId],
                  [locationSrvcLink.internal],
                  [...locationSrvcLink.serviceDeliveryIds],
                  [...locationSrvcLink.insuranceIds],
                  [...locationSrvcLink.ageGroupIds],
                  [locationSrvcLink.identifier]
                )
                .map(e => {
                  return {
                    serviceId: e[0],
                    internal: e[1],
                    serviceDeliveryId: e[2],
                    insuranceId: e[3],
                    ageGroupId: e[4],
                    identifier: e[5]
                  };
                });
            }
          );

          return providerLocation;
        }
      );

      const createProviderRequest = {
        providerInfo,
        providerLocations
      };

      this.providerService
        .createOwnProvider(createProviderRequest)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            this.router.navigate(["/providers"]);
          },
          error => {
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  showNotSavedLocation(event): void {
    this.showNotSavedLocationId = parseInt(
      event.currentTarget.dataset.locationid
    );
  }

  // hasAdministrativeContacts
  get hasAdministrativeEmails() {
    return (
      this.locationEmails.value.findIndex(
        locationEmail => locationEmail.contactType === "Administrative"
      ) !== -1
    );
  }

  get hasAdministrativeContactNumbers() {
    return (
      this.locationContactNumbers.value.findIndex(
        locationContactNumber =>
          locationContactNumber.contactType === "Administrative"
      ) !== -1
    );
  }

  // tooltip error
  get locationTooltip() {
    const validations = [];

    if (!this.hasAdministrativeContactNumbers) {
      validations.push("Missing administrative contact numbers");
    }

    if (!this.hasAdministrativeEmails) {
      validations.push("Missing administrative emails");
    }

    if (this.locationServiceCounties.value.length === 0) {
      validations.push("Missing service counties");
    }

    if (this.locationServices.value.length === 0) {
      validations.push("Missing services");
    }

    if (validations.length === 0) {
      return "";
    } else if (validations.length === 1) {
      return validations[0];
    }

    return validations.join("; ");
  }

  get providerTooltip() {
    const validations = [];

    if (!this.createDemographicsForm.valid) {
      validations.push("Demographics form invalid");
    }

    if (this.providerLocations.value.length === 0) {
      validations.push("Missing locations");
    }

    if (validations.length === 0) {
      return "";
    } else if (validations.length === 1) {
      return validations[0];
    }

    return validations.join("; ");
  }

  //

  get locationName() {
    return this.createLocationForm.get("locationName");
  }

  get serviceStreetAddress() {
    return this.createLocationForm.get("serviceStreetAddress");
  }

  get serviceCity() {
    return this.createLocationForm.get("serviceCity");
  }

  get serviceState() {
    return this.createLocationForm.get("serviceState");
  }

  get serviceZip() {
    return this.createLocationForm.get("serviceZip");
  }

  get serviceAddressSame() {
    return this.createLocationForm.get("serviceAddressSame");
  }

  get mailingStreetAddress() {
    return this.createLocationForm.get("mailingStreetAddress");
  }

  get mailingCity() {
    return this.createLocationForm.get("mailingCity");
  }

  get mailingState() {
    return this.createLocationForm.get("mailingState");
  }

  get mailingZip() {
    return this.createLocationForm.get("mailingZip");
  }

  get locationEmails() {
    return this.createLocationForm.get("emails");
  }

  get locationContactNumbers() {
    return this.createLocationForm.get("contactNumbers");
  }

  get locationServiceCounties() {
    return this.createLocationForm.get("serviceCounties");
  }

  get locationServices() {
    return this.createLocationForm.get("services");
  }

  get email() {
    return this.createEmailForm.get("email");
  }

  get contactNumber() {
    return this.createContactNumberForm.get("contactNumber");
  }

  get serviceId() {
    return this.createServiceForm.get("service");
  }

  get serviceDisplay() {
    return this.createServiceForm.get("serviceDisplay");
  }

  get serviceCategory() {
    return this.createServiceForm.get("serviceCategory");
  }

  // provider demographics

  get providerName() {
    return this.createDemographicsForm.get("providerName");
  }

  get website() {
    return this.createDemographicsForm.get("website");
  }

  get facebook() {
    return this.createDemographicsForm.get("facebook");
  }

  get twitter() {
    return this.createDemographicsForm.get("twitter");
  }

  get linkedin() {
    return this.createDemographicsForm.get("linkedin");
  }

  get instagram() {
    return this.createDemographicsForm.get("instagram");
  }

  get youtube() {
    return this.createDemographicsForm.get("youtube");
  }

  // provider
  get providerInfo() {
    return this.createProviderForm.get("providerInfo");
  }

  get providerLocations() {
    return this.createProviderForm.get("providerLocations");
  }

  get loggingDebug() {
    return JSON.stringify(this.createLocationForm.value);
  }

  // counties
  get counties() {
    return this.createServiceCountyForm.get("counties");
  }
}
