import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersUserComponent } from './users-user.component';

describe('UsersUserComponent', () => {
  let component: UsersUserComponent;
  let fixture: ComponentFixture<UsersUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
