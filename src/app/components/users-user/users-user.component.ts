import { Component, OnInit, ViewChild } from "@angular/core";
import { faEye, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UsersService } from "src/app/services/users/users.service";
import { map, flatMap } from "rxjs/operators";
import * as moment from "moment";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import Swal from "sweetalert2";
import { timer, forkJoin } from "rxjs";
import { FormGroup, FormBuilder, Validators, Form } from "@angular/forms";
import { PasswordValidator } from "../registration/validators/password.validator";
import { Location } from "@angular/common";
import { environment as ENV } from "../../../environments/environment";
import { ProviderAccountService } from "src/app/services/provider-account/provider-account.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-users-user",
  templateUrl: "./users-user.component.html",
  styleUrls: ["./users-user.component.scss"]
})
export class UsersUserComponent implements OnInit {
  changePasswordForm: FormGroup;
  changeEmailForm: FormGroup;
  changeUsernameForm: FormGroup;
  username: string;
  serverMessage: string;
  errorMessage: string;
  groupToDelete: string;
  permissionToDelete: string;
  defaultGroupSelected: string;
  defaultPermissionSelected: string;
  userViewDto = {
    username: "",
    firstName: "",
    lastName: "",
    email: "",
    image: "",
    adminGroup: "",
    enabled: false,
    rootAdmin: false,
    lastLoggedIn: "",
    groups: [],
    permissions: [],
    inheritedPermissionDtos: [],
    providers: []
  };
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteGroupAlert") confirmDeleteGroupAlert: SwalComponent;
  @ViewChild("confirmDeletePermissionAlert")
  confirmDeletePermissionAlert: SwalComponent;
  @ViewChild("confirmDeleteProviderAlert")
  confirmDeleteProviderAlert: SwalComponent;

  wait1Second = timer(1000);
  notInGroups;
  notInPermissions;
  emailAvailable: boolean = true;
  usernameAvailable: boolean = true;
  newUsernameUpdate: string;
  unlinkProviderId;

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private usersService: UsersService,
    private formTrimService: FormTrimService,
    private providerAccountService: ProviderAccountService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  checkUsername(): void {
    this.auth.checkUsername(this.newUsername.value).subscribe(response => {
      this.usernameAvailable = response.usernameAvailable;
    });
  }

  checkEmailAndEnabled(): void {
    this.auth.checkEmailAndEnabled(this.email.value).subscribe(response => {
      this.emailAvailable = response.emailAvailable;
    });
  }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.username = this.route.snapshot.url[3].path;

    this.route.url
      .pipe(
        map(url => (this.username = url[3].path)),
        flatMap(() => this.usersService.viewSpecificUser(this.username))
      )
      .subscribe(response => {
        this.userViewDto = response;

        if (!response.image) {
          this.userViewDto.image = ENV.EMPTY_PROFILE_IMG_URL;
        }

        this.userViewDto.groups = this.userViewDto.groups.map(group => {
          return {
            group,
            groupDisplay: this.formTrimService.processNamesToDisplay(group, 6)
          };
        });

        this.userViewDto.permissions = this.userViewDto.permissions.map(
          permission => {
            return {
              permission,
              permissionDisplay: this.formTrimService.processNamesToDisplay(
                permission,
                5
              )
            };
          }
        );

        this.userViewDto.inheritedPermissionDtos = this.userViewDto.inheritedPermissionDtos.map(
          iPermission => {
            return {
              ...iPermission,
              iPermissionDisplay: this.formTrimService.processNamesToDisplay(
                iPermission.permission,
                5
              ),
              iGroupDisplay: this.formTrimService.processNamesToDisplay(
                iPermission.group,
                6
              )
            };
          }
        );

        if (response.lastLoggedIn) {
          this.userViewDto.lastLoggedIn = moment(response.lastLoggedIn).format(
            "llll"
          );
        } else {
          this.userViewDto.lastLoggedIn = "No data yet";
        }
      });

    this.changePasswordForm = this.formBuilder.group({
      newPassword: [
        "",
        [Validators.required, Validators.minLength(8), PasswordValidator.strong]
      ],
      confirmNewPassword: ["", [Validators.required]]
    });

    this.changeEmailForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });

    this.changeUsernameForm = this.formBuilder.group({
      newUsername: [
        "",
        [
          Validators.required,
          Validators.pattern("^(\\d|\\w)+$"),
          Validators.maxLength(50)
        ]
      ]
    });

    const groupsServ = this.usersService.getAvailableGroupsToAdd(this.username);
    const permissionsServ = this.usersService.getAvailablePermissionsToAdd(
      this.username
    );

    forkJoin([groupsServ, permissionsServ]).subscribe(result => {
      this.notInGroups = result[0].map(group => {
        return {
          group,
          groupNameDisplay: this.formTrimService.processNamesToDisplay(group, 6)
        };
      });

      this.notInPermissions = result[1].map(permission => {
        return {
          permission,
          permissionNameDisplay: this.formTrimService.processNamesToDisplay(
            permission,
            5
          )
        };
      });
    });
  }

  goBack(): void {
    this.location.back();
  }

  cancel(): void {
    Swal.close();
  }

  confirmEnable(): void {
    this.usersService
      .enableUser(this.username)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to enable user...";
          throw error;
        }
      );
  }

  confirmDisable(): void {
    this.usersService
      .disableUser(this.username)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to disable user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  changePassword(changePasswordForm: FormGroup): void {
    if (changePasswordForm.valid && this.passwordsMatch) {
      const { newPassword, confirmNewPassword } = changePasswordForm.value;

      this.usersService
        .changeUserPassword(this.username, newPassword, confirmNewPassword)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to change password...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  changeEmail(changeEmailForm: FormGroup): void {
    if (changeEmailForm.valid && this.emailAvailable) {
      const { email } = changeEmailForm.value;

      this.usersService
        .changeUserEmail(this.username, email)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to change email...";

            if (error.error) {
              if (error.error.message) this.errorMessage = error.error.message;
            }

            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  changeUsername(changeUsernameForm: FormGroup): void {
    if (changeUsernameForm.valid && this.usernameAvailable) {
      const { newUsername } = changeUsernameForm.value;

      this.usersService
        .changeUserUsername(this.username, newUsername)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.newUsernameUpdate = newUsername;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.router.navigate([`../${this.newUsernameUpdate}`], {
              relativeTo: this.route
            });
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to change username...";

            if (error.error) {
              if (error.error.message) this.errorMessage = error.error.message;
            }

            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  deleteUserGroup(event): void {
    this.groupToDelete = event.currentTarget.dataset.group;

    this.confirmDeleteGroupAlert.fire();
  }

  deleteUserPermission(event): void {
    this.permissionToDelete = event.currentTarget.dataset.permission;

    this.confirmDeletePermissionAlert.fire();
  }

  confirmDeleteGroup(): void {
    this.usersService
      .deleteGroupFromUser(this.username, this.groupToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete group from user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  confirmDeletePermission(): void {
    this.usersService
      .deletePermissionFromUser(this.username, this.permissionToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage =
            "Error when trying to delete permission from user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  addGroupToUser(): void {
    this.usersService
      .addGroupToUser(this.username, this.defaultGroupSelected)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.defaultGroupSelected = null;
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to add group to user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  addPermissionToUser(): void {
    this.usersService
      .addPermissionToUser(this.username, this.defaultPermissionSelected)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.defaultPermissionSelected = null;
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to add permission to user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  viewProviderByProviderInfoId(event) {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`admin/provider/${providerInfoId}`]);
  }

  unlinkProviderAccount(event): void {
    this.unlinkProviderId = parseInt(
      event.currentTarget.dataset.providerinfoid
    );

    this.confirmDeleteProviderAlert.fire();
  }

  confirmDeleteProvider(): void {
    this.providerAccountService
      .unlinkProviderAccount(this.unlinkProviderId, this.userViewDto.username)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to unlink provider...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get newPassword() {
    return this.changePasswordForm.get("newPassword");
  }

  get confirmNewPassword() {
    return this.changePasswordForm.get("confirmNewPassword");
  }

  get passwordsMatch() {
    return (
      this.changePasswordForm.get("newPassword").value ===
      this.changePasswordForm.get("confirmNewPassword").value
    );
  }

  get email() {
    return this.changeEmailForm.get("email");
  }

  get newUsername() {
    return this.changeUsernameForm.get("newUsername");
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
