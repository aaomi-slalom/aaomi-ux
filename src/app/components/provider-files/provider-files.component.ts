import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ActivatedRoute } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { ProviderFileService } from "src/app/services/provider-file/provider-file.service";
import { DomSanitizer } from "@angular/platform-browser";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { faDownload, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import * as moment from "moment";
import Swal from "sweetalert2";

@Component({
  selector: "app-provider-files",
  templateUrl: "./provider-files.component.html",
  styleUrls: ["./provider-files.component.scss"]
})
export class ProviderFilesComponent implements OnInit {
  errorMessage: string;
  serverMessage: string;
  files = [];
  providerInfoId;
  providerFileName;
  providerFileBuffer;
  providerFileId;
  moment = moment;
  faDownload = faDownload;
  faTrashAlt = faTrashAlt;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    private auth: AuthService,
    private providerFileService: ProviderFileService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() =>
          this.providerFileService.getAllFilesByProviderInfoId(
            this.providerInfoId
          )
        )
      )
      .subscribe(files => {
        this.files = files;
      });
  }

  sanitize(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      this.providerFileName = event.target.files[0].name;

      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.providerFileBuffer = event.target.result;

        this.providerFileService
          .createProviderFile(
            this.providerInfoId,
            this.providerFileName,
            this.providerFileBuffer
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to upload file...";
              this.errorAlert.fire();
              throw error;
            }
          );
      };
    }
  }

  deleteProviderFile(event): void {
    this.providerFileId = event.currentTarget.dataset.providerfileid;

    this.confirmDeleteAlert.fire();
  }

  cancelDelete(): void {
    Swal.close();
  }

  confirmDelete(): void {
    this.providerFileService
      .deleteProviderFileById(this.providerFileId)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete file...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get hasCreatePermission(): boolean {
    if (!this.auth.isAuthenticated()) return false;

    return (
      this.auth.hasPermissions(["PERM_CREATE_PROVIDER_FILE"]) ||
      this.auth.hasPermissions(["PERM_CREATE_OWN_PROVIDER_FILE"])
    );
  }

  get hasDeletePermission(): boolean {
    if (!this.auth.isAuthenticated()) return false;

    return (
      this.auth.hasPermissions(["PERM_DELETE_PROVIDER_FILE"]) ||
      this.auth.hasPermissions(["PERM_DELETE_OWN_PROVIDER_FILE"])
    );
  }
}
