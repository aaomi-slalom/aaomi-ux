import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderFilesComponent } from './provider-files.component';

describe('ProviderFilesComponent', () => {
  let component: ProviderFilesComponent;
  let fixture: ComponentFixture<ProviderFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
