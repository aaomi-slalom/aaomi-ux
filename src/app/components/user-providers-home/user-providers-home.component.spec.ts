import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProvidersHomeComponent } from './user-providers-home.component';

describe('UserProvidersHomeComponent', () => {
  let component: UserProvidersHomeComponent;
  let fixture: ComponentFixture<UserProvidersHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProvidersHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProvidersHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
