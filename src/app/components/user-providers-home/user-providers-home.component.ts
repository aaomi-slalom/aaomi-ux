import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-user-providers-home",
  templateUrl: "./user-providers-home.component.html",
  styleUrls: ["./user-providers-home.component.scss"]
})
export class UserProvidersHomeComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {}

  goBackToAllProviders(): void {
    this.router.navigate(["/providers"]);
  }

  goToInfoPage(): void {
    this.router.navigate(["./"], { relativeTo: this.route });
  }

  goToFilesPage(): void {
    this.router.navigate(["./files"], { relativeTo: this.route });
  }

  goToWaitTimesPage(): void {
    this.router.navigate(["./waittimes"], { relativeTo: this.route });
  }

  goToAccountsPage(): void {
    this.router.navigate(["./accounts"], { relativeTo: this.route });
  }
}
