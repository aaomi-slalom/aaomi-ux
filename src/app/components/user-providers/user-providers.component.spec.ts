import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { UserProvidersComponent } from './user-providers.component';
import { Router } from "@angular/router";
import { HttpClientTestingModule } from "@angular/common/http/testing"

describe('UserProvidersComponent', () => {
  let component: UserProvidersComponent;
  let fixture: ComponentFixture<UserProvidersComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserProvidersComponent],
      providers: [ProviderService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
