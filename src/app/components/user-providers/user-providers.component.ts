import { Component, OnInit, Provider } from "@angular/core";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { environment as ENV } from "../../../environments/environment";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { Router } from "@angular/router";

@Component({
  selector: "app-user-providers",
  templateUrl: "./user-providers.component.html",
  styleUrls: ["./user-providers.component.scss"]
})
export class UserProvidersComponent implements OnInit {
  providers = [];
  faEye = faEye;

  constructor(
    private providerService: ProviderService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.providerService
      .getAllAccountProviderProfiles()
      .subscribe(providers => {
        this.providers = providers;
      });
  }

  openProviderView(event) {
    const providerInfoId = event.currentTarget.dataset.providerid;

    this.router.navigate([`../user/provider/${providerInfoId}`]);
  }

  get providersDisplay() {
    if (this.providers) {
      return this.providers.map(provider => {
        if (!provider.logo) {
          provider.logo = ENV.EMPTY_LOGO_IMG_URL;
        }

        return { ...provider };
      });
    }
  }
}
