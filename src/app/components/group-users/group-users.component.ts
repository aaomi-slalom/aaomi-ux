import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GroupService } from "src/app/services/group/group.service";
import { map, flatMap } from "rxjs/operators";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { faEye, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import Swal from "sweetalert2";
import { timer } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Component({
  selector: "app-group-users",
  templateUrl: "./group-users.component.html",
  styleUrls: ["./group-users.component.scss"]
})
export class GroupUsersComponent implements OnInit {
  accountGroup: string;
  usernameDelete: string;
  serverMessage: string;
  errorMessage: string;
  totalPages: number;
  totalElements: number;
  pageSize: number = 5;
  faEye = faEye;
  faTrashAlt = faTrashAlt;
  groups;
  users = [];
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private groupService: GroupService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.parent.url
      .pipe(
        map(url => (this.accountGroup = url[2].path)),
        flatMap(() =>
          this.groupService.getGroupUsers(this.accountGroup, 0, this.pageSize)
        )
      )
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.users = response.groupAccountDtos;
      });
  }

  pageChanged(event: PageChangedEvent): void {
    this.groupService
      .getGroupUsers(this.accountGroup, event.page - 1, this.pageSize)
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.users = response.groupAccountDtos;
      });
  }

  openGroupUserView(event): void {
    const username = event.currentTarget.dataset.username;

    this.router.navigate([`/admin/users/user/${username}`]);
  }

  deleteGroupUser(event): void {
    this.usernameDelete = event.currentTarget.dataset.username;

    this.confirmDeleteAlert.fire();
  }

  confirmDelete(): void {
    this.groupService
      .deleteGroupUser(this.accountGroup, this.usernameDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete user from group...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancelDelete(): void {
    Swal.close();
  }

  get usersDisplay(): any[] {
    if (this.users) {
      return this.users.map(user => {
        if (!user.image) {
          user.image = ENV.EMPTY_PROFILE_IMG_URL;
        }

        return { ...user };
      });
    }
  }
}
