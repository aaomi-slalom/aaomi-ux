import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApprovalsHomeComponent } from './approvals-home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";
import { ApprovalService } from "src/app/services/approval/approval.service"

describe('ApprovalsHomeComponent', () => {
  let component: ApprovalsHomeComponent;
  let fixture: ComponentFixture<ApprovalsHomeComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApprovalsHomeComponent],
      imports: [HttpClientTestingModule],
      providers: [ApprovalService, { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
