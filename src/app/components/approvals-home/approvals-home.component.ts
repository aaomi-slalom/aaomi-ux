import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ApprovalService } from "src/app/services/approval/approval.service";
import { forkJoin, timer } from "rxjs";
import { faEye, faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import * as moment from "moment";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-approvals-home",
  templateUrl: "./approvals-home.component.html",
  styleUrls: ["./approvals-home.component.scss"]
})
export class ApprovalsHomeComponent implements OnInit {
  faCheck = faCheck;
  faTimes = faTimes;
  faEye = faEye;
  serverMessage: string;
  errorMessage: string;
  unapprovedAccounts = [];
  unapprovedProviders = [];
  unapprovedLocations = [];
  unapprovedModifiedProviders = [];
  moment = moment;
  displayRole = this.formTrimService.processNamesToDisplay;

  approveAccountUsername: string;
  disapproveAccountUsername: string;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmApproveAccountAlert")
  confirmApproveAccountAlert: SwalComponent;
  @ViewChild("confirmDisapproveAccountAlert")
  confirmDisapproveAccountAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    private auth: AuthService,
    private approvalService: ApprovalService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    const unapprovedAccounts = this.approvalService.getAllUnapprovedAccounts();
    const unapprovedProviders = this.approvalService.getAllUnapprovedProviders();
    const unapprovedLocations = this.approvalService.getAllUnapprovedLocations();
    const unapprovedModifiedProviders = this.approvalService.getAllUnapprovedModifiedProviders();

    forkJoin([
      unapprovedAccounts,
      unapprovedProviders,
      unapprovedLocations,
      unapprovedModifiedProviders
    ]).subscribe(results => {
      this.unapprovedAccounts = results[0];
      this.unapprovedProviders = results[1];
      this.unapprovedLocations = results[2];
      this.unapprovedModifiedProviders = results[3];
    });
  }

  viewModifiedProviderForApproval(event): void {
    const providerInfoId = event.currentTarget.dataset.providerid;

    this.router.navigate([`/approval/modified/provider/${providerInfoId}`]);
  }

  approveAccount(event): void {
    this.approveAccountUsername = event.currentTarget.dataset.username;

    this.confirmApproveAccountAlert.fire();
  }

  disapproveAccount(event): void {
    this.disapproveAccountUsername = event.currentTarget.dataset.username;

    this.confirmDisapproveAccountAlert.fire();
  }

  cancelApprove(): void {
    Swal.close();
  }

  cancelDisapprove(): void {
    Swal.close();
  }

  confirmApproveAccount(): void {
    this.approvalService
      .approveAccountByUsername(this.approveAccountUsername)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to approve account...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  confirmDisapproveAccount(): void {
    this.approvalService
      .disapproveAccountByUsername(this.disapproveAccountUsername)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage =
            "Error when trying to disapprove and remove account...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
