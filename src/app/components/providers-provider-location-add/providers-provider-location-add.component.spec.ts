import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidersProviderLocationAddComponent } from './providers-provider-location-add.component';

describe('ProvidersProviderLocationAddComponent', () => {
  let component: ProvidersProviderLocationAddComponent;
  let fixture: ComponentFixture<ProvidersProviderLocationAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidersProviderLocationAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidersProviderLocationAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
