import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ProviderAccountService } from 'src/app/services/provider-account/provider-account.service';
import { ProviderLinkAccountsComponent } from './provider-link-accounts.component';

describe('ProviderLinkAccountsComponent', () => {
  let component: ProviderLinkAccountsComponent;
  let fixture: ComponentFixture<ProviderLinkAccountsComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderLinkAccountsComponent],
      providers: [ProviderAccountService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderLinkAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
