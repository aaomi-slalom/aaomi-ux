import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderAccountService } from "src/app/services/provider-account/provider-account.service";
import { ActivatedRoute } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: "app-provider-link-accounts",
  templateUrl: "./provider-link-accounts.component.html",
  styleUrls: ["./provider-link-accounts.component.scss"]
})
export class ProviderLinkAccountsComponent implements OnInit {
  linkAccountForm: FormGroup;
  providerInfoId;
  accounts = [];
  faTrashAlt = faTrashAlt;
  unlinkUsername: string;
  errorMessage: string;
  serverMessage: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;

  wait1Second = timer(1000);

  constructor(
    private auth: AuthService,
    private providerAccountService: ProviderAccountService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.linkAccountForm = this.formBuilder.group({
      username: ""
    });

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() =>
          this.providerAccountService.getAllAccountsByProviderInfoId(
            this.providerInfoId
          )
        )
      )
      .subscribe(accounts => {
        this.accounts = accounts;
      });
  }

  unlinkProviderAccount(event): void {
    this.unlinkUsername = event.currentTarget.dataset.username;

    this.confirmDeleteAlert.fire();
  }

  cancelDelete(): void {
    Swal.close();
  }

  confirmDelete(): void {
    this.providerAccountService
      .unlinkProviderAccount(this.providerInfoId, this.unlinkUsername)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to unlink account...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  searchAndLinkAccount(): void {
    const username = this.linkAccountForm.get("username").value;
    this.providerAccountService
      .linkProviderAccount(this.providerInfoId, username)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to link account...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get isAllowedToLinkOrUnlink(): boolean {
    return this.auth.hasPermissions(["PERM_LINK_OR_UNLINK_ACCOUNT"]);
  }
}
