import { FormControl } from "@angular/forms";

export interface ValidationResult {
  [key: string]: boolean;
}

export class PasswordValidator {
  public static strong(control: FormControl): ValidationResult {
    let hasDigit = /(?=.*[0-9]).+/.test(control.value);
    let hasUpper = /(?=.*[A-Z]).+/.test(control.value);
    let hasLower = /(?=.*[a-z]).+/.test(control.value);
    let hasNoWhitespace = /(?=\S+$).+/.test(control.value);

    const valid =
      (hasDigit && hasUpper && hasLower && hasNoWhitespace) || !control.value;

    if (!valid) {
      return { strong: true };
    }

    return null;
  }
}
