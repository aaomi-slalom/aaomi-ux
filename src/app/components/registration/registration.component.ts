import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { PasswordValidator } from "./validators/password.validator";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { Router } from "@angular/router";
import { timer } from "rxjs";

@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"]
})
export class RegistrationComponent implements OnInit {
  signUpForm: FormGroup;
  usernameAvailable: boolean = true;
  emailAvailable: boolean = true;
  accountEnabled: boolean = false;
  resendClicked: boolean;
  registrationMessage: string;
  verificationResendConfirmation: string;
  resendEmail: string;
  @ViewChild("signupSubmit") private signupSubmit: SwalComponent;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  wait30Seconds = timer(30000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  checkUsername(): void {
    this.auth.checkUsername(this.username.value).subscribe(response => {
      this.usernameAvailable = response.usernameAvailable;
    });
  }

  checkEmailAndEnabled(): void {
    this.auth.checkEmailAndEnabled(this.email.value).subscribe(response => {
      this.emailAvailable = response.emailAvailable;
      this.accountEnabled = response.accountEnabled;
    });
  }

  ngOnInit(): void {
    if (this.auth.isAuthenticated()) {
      this.router.navigate([""]);
    }

    this.signUpForm = this.formBuilder.group({
      role: ["SERVICE_PROVIDER", [Validators.required]],
      username: [
        "",
        [
          Validators.required,
          Validators.pattern("^(\\d|\\w)+$"),
          Validators.maxLength(50)
        ]
      ],
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.minLength(8), PasswordValidator.strong]
      ],
      confirmPassword: ["", [Validators.required]],
      acceptTerms: [false, [Validators.requiredTrue]]
    });
  }

  onSubmit(signUpForm: FormGroup): void {
    if (
      signUpForm.valid &&
      this.passwordsMatch &&
      this.emailAvailable &&
      this.usernameAvailable
    ) {
      const {
        role,
        username,
        email,
        password,
        confirmPassword
      } = signUpForm.value;

      this.auth
        .register(role, username, email, password, confirmPassword)
        .subscribe(
          response => {
            this.registrationMessage = response.message;
            this.resendEmail = this.email.value;
            this.signupSubmit.fire();
            this.signUpForm.reset();
          },
          error => {
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.errorAlert.fire();
    }
  }

  resendVerification(): void {
    const emailto = this.email.value ? this.email.value : this.resendEmail;

    this.auth.resendVerification(emailto).subscribe(
      response => (this.verificationResendConfirmation = response.message),
      error => {
        this.verificationResendConfirmation =
          "Trouble sending email to this email address...";
        throw error;
      }
    );

    this.resendClicked = true;

    this.wait30Seconds.subscribe(() => {
      this.resendClicked = false;
    });
  }

  get role() {
    return this.signUpForm.get("role");
  }

  get username() {
    return this.signUpForm.get("username");
  }

  get email() {
    return this.signUpForm.get("email");
  }

  get password() {
    return this.signUpForm.get("password");
  }

  get confirmPassword() {
    return this.signUpForm.get("confirmPassword");
  }

  get passwordsMatch() {
    return (
      this.signUpForm.get("password").value ===
      this.signUpForm.get("confirmPassword").value
    );
  }

  get acceptTerms() {
    return this.signUpForm.get("acceptTerms");
  }
}
