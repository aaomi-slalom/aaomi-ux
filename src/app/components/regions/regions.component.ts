import { Component, OnInit, ViewChild } from "@angular/core";
import { RegionService } from "src/app/services/region/region.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { faPlus, faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-regions",
  templateUrl: "./regions.component.html",
  styleUrls: ["./regions.component.scss"]
})
export class RegionsComponent implements OnInit {
  createNewRegionForm: FormGroup;
  editExistingRegionForm: FormGroup;
  faPlus = faPlus;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  existingRegionDtos = [];
  regionId: number;
  editCurrRegionName: String;
  editCurrRegionCounties: any;
  serverMessage: String;
  errorMessage: String;
  regionIdToDelete: number;
  createCountyOptions: [];
  editCountyOptions: [];
  countySelectSettings: {};
  wait1Second = timer(1000);
  @ViewChild("editRegionModal") editRegionModal: SwalComponent;
  @ViewChild("confirmDeleteRegionAlert")
  confirmDeleteRegionAlert: SwalComponent;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private regionService: RegionService,
    private auth: AuthService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.regionService.getExistingRegions().subscribe(existingRegionDtos => {
      this.existingRegionDtos = existingRegionDtos;
    });

    this.createNewRegionForm = this.formBuilder.group({
      regionName: ["", Validators.required],
      countiesSelected: []
    });

    this.editExistingRegionForm = this.formBuilder.group({
      newRegionName: ["", Validators.required],
      newCountyIds: [],
      newCountiesList: []
    });

    this.countySelectSettings = {
      singleSelection: false,
      idField: "countyId",
      textField: "county",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.regionService
      .getAvailableCountyOptionsForNewRegion()
      .subscribe(options => (this.createCountyOptions = options));
  }

  createRegion(): void {
    if (this.createNewRegionForm.valid && !this.regionAlreadyExists) {
      this.regionService
        .createNewRegion(
          this.regionName.value.trim(),
          this.countiesSelected.value
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create new region...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editRegion(event): void {
    this.regionId = event.currentTarget.dataset.regionid;
    this.editCurrRegionName = event.currentTarget.dataset.region;
    this.editCurrRegionCounties = this.existingRegionDtos.find(
      region => region.regionId == this.regionId
    ).counties;
    this.newRegionName.setValue(this.editCurrRegionName);
    this.newCountiesList.setValue(this.editCurrRegionCounties);
    this.regionService
      .getAvailableCountyOptions(this.regionId)
      .subscribe(options => (this.editCountyOptions = options));
    this.editRegionModal.fire();
  }

  editRegionSubmit(): void {
    if (this.editExistingRegionForm.valid && !this.editRegionAlreadyExist) {
      this.regionService
        .updateExistingRegion(
          this.regionId,
          this.newRegionName.value.trim(),
          this.newCountiesList.value
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update region";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  deleteExistingRegion(event): void {
    this.regionIdToDelete = event.currentTarget.dataset.regionid;
    this.confirmDeleteRegionAlert.fire();
  }

  confirmDeleteRegion(): void {
    this.regionService
      .deleteRegion(this.regionIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete region...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancel(): void {
    Swal.close();
  }

  get newCountyIds() {
    return this.editExistingRegionForm.get("newCountyIds");
  }

  get newCountiesList() {
    return this.editExistingRegionForm.get("newCountiesList");
  }

  get newRegionName() {
    return this.editExistingRegionForm.get("newRegionName");
  }

  get regionName() {
    return this.createNewRegionForm.get("regionName");
  }

  get countiesSelected() {
    return this.createNewRegionForm.get("countiesSelected");
  }

  get regionAlreadyExists() {
    return this.existingRegionDtos
      .map(region => region.region)
      .includes(this.regionName.value.trim());
  }

  get editRegionAlreadyExist() {
    return this.existingRegionDtos
      .filter(region => region.region !== this.editCurrRegionName)
      .map(region => region.region)
      .includes(this.newRegionName.value.trim());
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
