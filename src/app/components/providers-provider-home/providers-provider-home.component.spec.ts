import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidersProviderHomeComponent } from './providers-provider-home.component';

describe('ProvidersProviderHomeComponent', () => {
  let component: ProvidersProviderHomeComponent;
  let fixture: ComponentFixture<ProvidersProviderHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidersProviderHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidersProviderHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
