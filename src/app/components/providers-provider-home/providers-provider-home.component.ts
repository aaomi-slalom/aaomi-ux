import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ActivatedRoute } from "@angular/router";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { map, flatMap } from "rxjs/operators";
import { environment as ENV } from "../../../environments/environment";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UrlValidator } from "../profile-create/validators/url.validator";
import Swal from "sweetalert2";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "app-providers-provider-home",
  templateUrl: "./providers-provider-home.component.html",
  styleUrls: ["./providers-provider-home.component.scss"]
})
export class ProvidersProviderHomeComponent implements OnInit {
  editDemographicsForm: FormGroup;
  unapprovedDemographicsForm: FormGroup;
  Swal = Swal;
  profileImg = ENV.EMPTY_PROFILE_IMG_URL;
  imageBuffer: string | ArrayBuffer = ENV.EMPTY_LOGO_IMG_URL;
  modifiedImageBuffer: string | ArrayBuffer = ENV.EMPTY_LOGO_IMG_URL;
  isViewMode: boolean = true;
  hasProviderName: boolean = false;
  serverMessage: string;
  errorMessage: string;
  providerInfoId;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  wait1Second = timer(1000);
  provider = {
    accountDto: {
      email: "",
      firstName: "",
      lastName: ""
    },
    providerInfoDto: {
      providerName: "",
      logo: "",
      website: "",
      description: "",
      facebook: "",
      linkedin: "",
      twitter: "",
      instagram: "",
      youtube: "",
      modifiedProviderName: "",
      modifiedLogo: "",
      modifiedWebsite: "",
      modifiedDescription: "",
      modifiedFacebook: "",
      modifiedLinkedin: "",
      modifiedTwitter: "",
      modifiedInstagram: "",
      modifiedYoutube: "",
      modifyRequested: false
    }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "150",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    sanitize: false,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      ["fontName"],
      [
        "insertImage",
        "insertVideo",
        "insertHorizontalRule",
        "backgroundColor",
        "customClasses",
        "fontSize",
        "toggleEditorMode"
      ]
    ]
  };

  constructor(
    private auth: AuthService,
    private providerService: ProviderService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) { }

  checkUpdateProviderName(): void {
    this.providerService
      .checkUpdateProviderName(this.providerName.value, this.providerInfoId)
      .subscribe(response => {
        this.hasProviderName = response.hasProviderName;
      });
  }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.editDemographicsForm = this.formBuilder.group({
      providerName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      logo: ["", []],
      description: ["", []],
      facebook: ["", [UrlValidator.isValidUrl]],
      linkedin: ["", [UrlValidator.isValidUrl]],
      twitter: ["", [UrlValidator.isValidUrl]],
      instagram: ["", [UrlValidator.isValidUrl]],
      youtube: ["", [UrlValidator.isValidUrl]]
    });

    this.unapprovedDemographicsForm = this.formBuilder.group({
      providerName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      logo: ["", []],
      description: ["", []],
      facebook: ["", [UrlValidator.isValidUrl]],
      linkedin: ["", [UrlValidator.isValidUrl]],
      twitter: ["", [UrlValidator.isValidUrl]],
      instagram: ["", [UrlValidator.isValidUrl]],
      youtube: ["", [UrlValidator.isValidUrl]]
    });

    this.route.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() => this.providerService.getProviderById(this.providerInfoId))
      )
      .subscribe(provider => {
        if (provider.accountDto) {
          if (provider.accountDto.image) {
            this.profileImg = provider.accountDto.image;
          }
        }

        if (provider.providerInfoDto.logo) {
          this.imageBuffer = provider.providerInfoDto.logo;
        }

        if (this.provider.providerInfoDto.modifiedLogo) {
          this.modifiedImageBuffer = this.provider.providerInfoDto.modifiedLogo;
        }

        this.providerName.setValue(provider.providerInfoDto.providerName);
        this.website.setValue(provider.providerInfoDto.website);
        this.description.setValue(provider.providerInfoDto.description);
        this.facebook.setValue(provider.providerInfoDto.facebook);
        this.twitter.setValue(provider.providerInfoDto.twitter);
        this.linkedin.setValue(provider.providerInfoDto.linkedin);
        this.instagram.setValue(provider.providerInfoDto.instagram);
        this.youtube.setValue(provider.providerInfoDto.youtube);

        this.unapprovedDemographicsForm.patchValue({
          providerName: provider.providerInfoDto.modifiedProviderName,
          website: provider.providerInfoDto.modifiedWebsite,
          description: provider.providerInfoDto.modifiedDescription,
          facebook: provider.providerInfoDto.modifiedFacebook,
          twitter: provider.providerInfoDto.modifiedTwitter,
          linkedin: provider.providerInfoDto.modifiedLinkedin,
          instagram: provider.providerInfoDto.modifiedInstagram,
          youtube: provider.providerInfoDto.modifiedYoutube
        });

        this.provider = provider;
      });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  submitEditDemographicsForm(): void {
    if (this.editDemographicsForm.valid) {
      const demographicsValues = { ...this.editDemographicsForm.value };
      demographicsValues.logo = this.imageBuffer;

      if (this.hasAdminEditPerm) {
        this.providerService
          .updateProviderDemographicsById(
            this.providerInfoId,
            demographicsValues
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              this.isViewMode = true;
              Swal.close();
              this.ngOnInit();
            }),
          error => {
            this.errorMessage =
              " Provider Error when trying to update provider demographics...";
            this.errorAlert.fire();
            throw error;
          };
      } else {
        this.providerService
          .navigatorUpdateProviderDemographicsById(
            this.providerInfoId,
            demographicsValues
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              this.isViewMode = true;
              Swal.close();
              this.ngOnInit();
            }),
          error => {
            this.errorMessage =
              "Navigator Error when trying to update provider demographics...";
            this.errorAlert.fire();
            throw error;
          }
          ;
      }
    }
  }

  get providerName() {
    return this.editDemographicsForm.get("providerName");
  }

  get website() {
    return this.editDemographicsForm.get("website");
  }

  get description() {
    return this.editDemographicsForm.get("description");
  }

  get facebook() {
    return this.editDemographicsForm.get("facebook");
  }

  get twitter() {
    return this.editDemographicsForm.get("twitter");
  }

  get linkedin() {
    return this.editDemographicsForm.get("linkedin");
  }

  get instagram() {
    return this.editDemographicsForm.get("instagram");
  }

  get youtube() {
    return this.editDemographicsForm.get("youtube");
  }

  // permissions
  get hasAdminEditPerm() {
    return this.auth.hasPermissions(["PERM_CREATE_OR_MODIFY_PROVIDER_INFO"]);
  }
}
