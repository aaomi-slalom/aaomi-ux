import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { MessageService } from "src/app/services/message/message.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { map, flatMap } from "rxjs/operators";
import { forkJoin, timer } from "rxjs";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { faTrashAlt, faEye } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import * as moment from "moment";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { CommunicationService } from "src/app/services/communication/communication.service";

class ListItem {
  id: String | number;
  text: String | number;
  isDisabled?: boolean;

  public constructor(source: any) {
    if (typeof source === "string" || typeof source === "number") {
      this.id = this.text = source;
      this.isDisabled = false;
    }
    if (typeof source === "object") {
      this.id = source.accountId;
      this.text = source.username;
      this.isDisabled = source.isDisabled;
    }
  }
}

@Component({
  selector: "app-messaging",
  templateUrl: "./messaging.component.html",
  styleUrls: ["./messaging.component.scss"]
})
export class MessagingComponent implements OnInit {
  createMessageForm: FormGroup;
  showMessageForm: FormGroup;
  selectRecipientsForm: FormGroup;
  receivedMessageDtos = [];
  sentMessageDtos = [];
  messageDto;
  accountGroupsSelected;
  messageAccountGroupDtos = [];
  accountGroupAccounts = [];
  accountGroupUsers = [];
  recipientUsers: string;
  accountGroupSelectSettings;
  accountGroupName: string;
  recipientSelectSettings;
  username: string;
  hasMessagingAuthority: boolean;
  showInbox = true;
  showSent: boolean;
  serverMessage: string;
  errorMessage: string;
  cancelMessage: string;
  currSubject: string;
  currMessage: string;
  currSender: string;
  messageId: number;
  messageIdToDelete: number;
  pageSize: number = 5;
  totalElementsSent: number;
  totalPagesSent: number;
  totalElementsInbox: number;
  totalPagesInbox: number;
  faTrashAlt = faTrashAlt;
  faEye = faEye;
  @ViewChild("showMessageModal") showMessageModal: SwalComponent;
  @ViewChild("selectRecipientsModal") selectRecipientsModal: SwalComponent;
  @ViewChild("createMessageInternalUserModal")
  createMessageInternalUserModal: SwalComponent;
  @ViewChild("createMessageExternalUserModal")
  createMessageExternalUserModal: SwalComponent;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("cancelAlert") cancelAlert: SwalComponent;
  @ViewChild("recipientDropdown") recipientDropdown;
  @ViewChild("accountGroupDropdown") accountGroupDropdown;
  @ViewChild("confirmDeleteSentMessageAlert")
  confirmDeleteSentMessageAlert: SwalComponent;
  @ViewChild("confirmDeleteReceivedMessageAlert")
  confirmDeleteReceivedMessageAlert: SwalComponent;
  wait1Second = timer(1000);
  moment = moment;

  constructor(
    private messageService: MessageService,
    private auth: AuthService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.username = this.auth.getUsername();

    this.hasMessagingAuthority = this.auth.hasPermissions([
      "PERM_BULK_MESSAGING"
    ]);

    const accountGroups = this.hasMessagingAuthority
      ? this.messageService.getAllAccountGroups()
      : this.messageService.getAccountGroupsExternalUser();

    this.createMessageForm = this.formBuilder.group({
      subject: ["", [Validators.required]],
      message: ["", [Validators.required]],
      recipients: [[], []]
    });

    this.selectRecipientsForm = this.formBuilder.group({
      accountGroups: [[], []],
      recipientUsernames: [[], []]
    });

    this.accountGroupSelectSettings = {
      singleSelection: false,
      idField: "accountGroupId",
      textField: "accountGroup",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.recipientSelectSettings = {
      singleSelection: false,
      idField: "accountId",
      textField: "username",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    const sentMessages = this.messageService.getAllSentMessagesByUsername(
      this.username,
      0,
      this.pageSize
    );
    const receivedMessages = this.messageService.getAllReceivedMessagesByUsername(
      this.username,
      0,
      this.pageSize
    );

    forkJoin([sentMessages, receivedMessages, accountGroups]).subscribe(
      results => {
        this.sentMessageDtos = results[0].sentMessages;
        this.totalElementsSent = results[0].totalElements;
        this.totalPagesSent = results[0].totalPages;
        this.receivedMessageDtos = results[1].receivedMessages;
        this.totalElementsInbox = results[1].totalElements;
        this.totalPagesInbox = results[1].totalPages;
        this.messageAccountGroupDtos = results[2];
      }
    );
  }

  pageChangedSentMessages(event: PageChangedEvent): void {
    this.messageService
      .getAllSentMessagesByUsername(
        this.username,
        event.page - 1,
        this.pageSize
      )
      .subscribe(response => {
        this.totalPagesSent = response.totalPages;
        this.totalElementsSent = response.totalElements;
        this.sentMessageDtos = response.sentMessages;
      });
  }

  pageChangedReceivedMessages(event: PageChangedEvent): void {
    this.messageService
      .getAllReceivedMessagesByUsername(
        this.username,
        event.page - 1,
        this.pageSize
      )
      .subscribe(response => {
        this.totalPagesInbox = response.totalPages;
        this.totalElementsInbox = response.totalElements;
        this.receivedMessageDtos = response.receivedMessages;
      });
  }

  viewMessage(event) {
    this.messageId = event.currentTarget.dataset.messageid;

    this.messageService
      .viewSpecificMessage(this.messageId)
      .subscribe(messageObj => {
        this.messageDto = messageObj;
        this.currSender = this.messageDto.sender;
        this.currSubject = this.messageDto.subject;
        this.currMessage = this.messageDto.message;
      });
    this.showMessageModal.fire();
  }

  isInternalUser(): void {
    this.hasMessagingAuthority
      ? this.createMessageInternalUserModal.fire()
      : this.createMessageExternalUserModal.fire();
  }

  createMessage(): void {
    const { subject, message, recipients } = this.createMessageForm.value;
    this.messageService
      .saveMessage(this.username, subject, message, recipients)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.accountGroupUsers = [];
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to send message...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  createMessageExternalUser(): void {
    const { subject, message, recipients } = this.createMessageForm.value;
    this.messageService
      .saveMessageExternalUser(subject, message, recipients)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to send message...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  showInboxHideSent(): void {
    this.showInbox = true;
    this.showSent = false;
  }

  showSentHideInbox(): void {
    this.showSent = true;
    this.showInbox = false;
  }

  onAccountGroupSelect(accountGroupSelected): void {
    this.accountGroupAccounts = this.messageAccountGroupDtos
      .filter(
        group => group.accountGroupId === accountGroupSelected.accountGroupId
      )
      .flatMap(accountGroup => accountGroup.recipients);

    this.accountGroupUsers.push(...this.accountGroupAccounts);
    const multiSelectData = this.accountGroupUsers.map(
      agu => new ListItem(agu)
    );
    this.recipientDropdown._data = multiSelectData;
  }

  onSelectAllAccountGroups(): void {
    this.accountGroupAccounts = this.messageAccountGroupDtos.flatMap(
      accountGroup => accountGroup.recipients
    );
    this.accountGroupUsers.push(...this.accountGroupAccounts);
    const multiSelectData = this.accountGroupUsers.map(
      agu => new ListItem(agu)
    );
    this.recipientDropdown._data = multiSelectData;
  }

  cancel(): void {
    this.createMessageForm.reset({
      subject: "",
      message: "",
      recipients: []
    });
    this.accountGroupUsers = [];
    Swal.close();
  }

  get displayUsers() {
    return this.accountGroupUsers[0];
  }

  get sender() {
    return this.createMessageForm.get("sender");
  }

  get subject() {
    return this.createMessageForm.get("subject");
  }

  get message() {
    return this.createMessageForm.get("message");
  }

  get recipients() {
    return this.createMessageForm.get("recipients");
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
