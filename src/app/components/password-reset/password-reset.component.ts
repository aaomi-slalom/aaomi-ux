import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PasswordValidator } from "../registration/validators/password.validator";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { map, flatMap } from "rxjs/operators";
import { timer } from "rxjs";

@Component({
  selector: "app-password-reset",
  templateUrl: "./password-reset.component.html",
  styleUrls: ["./password-reset.component.scss"]
})
export class PasswordResetComponent implements OnInit {
  passwordResetForm: FormGroup;
  accountId: number;
  linkValidMessage: string;
  resetEmail: string;
  token: string;
  serverMessage: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  wait2Seconds = timer(2000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.passwordResetForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.minLength(8), PasswordValidator.strong]
      ],
      confirmPassword: ["", [Validators.required]]
    });

    this.accountId = this.route.snapshot.queryParams["id"];
    this.token = this.route.snapshot.queryParams["token"];

    this.route.queryParams
      .pipe(
        flatMap(params => this.auth.verifyResetLink(params.id, params.token))
      )
      .subscribe(
        response => {
          this.linkValidMessage = response.message;
          this.resetEmail = response.email;
          this.email.setValue(this.resetEmail);
          this.email.disable();
        },
        error => {
          this.linkValidMessage = error.error.message;
          this.resetEmail = null;
          throw error;
        }
      );
  }

  onSubmit(passwordResetForm: FormGroup): void {
    if (passwordResetForm.valid && this.passwordsMatch) {
      const reqBody = {
        ...passwordResetForm.value,
        email: this.email.value,
        accountId: this.accountId,
        passwordResetToken: this.token
      };

      const {
        email,
        password,
        confirmPassword,
        accountId,
        passwordResetToken
      } = reqBody;

      this.auth
        .updatePassword(
          email,
          password,
          confirmPassword,
          accountId,
          passwordResetToken
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait2Seconds)
        )
        .subscribe(
          () => {
            this.router.navigate(["/login"]);
          },
          error => {
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.errorAlert.fire();
    }
  }

  get email() {
    return this.passwordResetForm.get("email");
  }

  get password() {
    return this.passwordResetForm.get("password");
  }

  get confirmPassword() {
    return this.passwordResetForm.get("confirmPassword");
  }

  get passwordsMatch() {
    return (
      this.passwordResetForm.get("password").value ===
      this.passwordResetForm.get("confirmPassword").value
    );
  }
}
