import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PasswordResetComponent } from "./password-reset.component";
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router, Params, UrlSegment, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe("PasswordResetComponent", () => {
  let component: PasswordResetComponent;
  let fixture: ComponentFixture<PasswordResetComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const fakeActivatedRoute = {
    snapshot: {
      params: {
        result: 'some result'
      },
      queryParams: {
        id: '3',
        token: 'some token'
      }
    },
    params: of<Params>(),
    url: of<UrlSegment[]>(),
    queryParams: of<Params>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
        FormBuilder,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute }
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [PasswordResetComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
