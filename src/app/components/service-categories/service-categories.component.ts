import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faEdit,
  faPlus,
  faTrashAlt,
  faInfoCircle
} from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { timer } from "rxjs";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { flatMap, map } from "rxjs/operators";
import Swal from "sweetalert2";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-service-categories",
  templateUrl: "./service-categories.component.html",
  styleUrls: ["./service-categories.component.scss"]
})
export class ServiceCategoriesComponent implements OnInit {
  createServiceCategoryForm: FormGroup;
  editServiceCategoryForm: FormGroup;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  faPlus = faPlus;
  faInfoCircle = faInfoCircle;
  serviceCategories = [];
  serverMessage: string;
  errorMessage: string;
  editPrevCategory: string;
  editPrevDescription: string;
  editCategoryId: number;
  deleteCategoryId: number;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  @ViewChild("editServiceCategoryModal")
  editServiceCategoryModal: SwalComponent;
  @ViewChild("confirmDeleteCategoryAlert")
  confirmDeleteCategoryAlert: SwalComponent;

  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private serviceCategoryService: ServiceCategoryService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.serviceCategoryService
      .getAllServiceCategories()
      .subscribe(serviceCategories => {
        this.serviceCategories = serviceCategories;
      });

    this.createServiceCategoryForm = this.formBuilder.group({
      category: ["", [Validators.required]],
      description: ["", []]
    });

    this.editServiceCategoryForm = this.formBuilder.group({
      editCategory: ["", [Validators.required]],
      editDescription: ["", []]
    });
  }

  editServiceCategory(event): void {
    this.editCategoryId = event.currentTarget.dataset.categoryid;
    this.editPrevCategory = event.currentTarget.dataset.servicecategory;
    this.editPrevDescription = event.currentTarget.dataset.description;
    this.editCategory.setValue(this.editPrevCategory);
    this.editDescription.setValue(this.editPrevDescription);

    this.editServiceCategoryModal.fire();
  }

  editServiceCategorySubmit(): void {
    if (this.editServiceCategoryForm.valid && !this.editCategoryAlreadyExist) {
      this.serviceCategoryService
        .updateServiceCategory(
          this.editCategoryId,
          this.editCategory.value.trim(),
          this.editDescription.value.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update service category...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  createServiceCategory(): void {
    if (this.createServiceCategoryForm.valid && !this.categoryAlreadyExist) {
      this.serviceCategoryService
        .createServiceCategory(
          this.category.value.trim(),
          this.description.value.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to create service category...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  deleteServiceCategory(event): void {
    this.deleteCategoryId = event.currentTarget.dataset.categoryid;
    this.confirmDeleteCategoryAlert.fire();
  }

  confirmDeleteCategory(): void {
    this.serviceCategoryService
      .deleteServiceCategory(this.deleteCategoryId)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete service category...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancel(): void {
    Swal.close();
  }

  get category() {
    return this.createServiceCategoryForm.get("category");
  }

  get description() {
    return this.createServiceCategoryForm.get("description");
  }

  get categoryAlreadyExist() {
    return this.serviceCategories
      .map(category => category.serviceCategory)
      .includes(this.category.value.trim());
  }

  get editCategory() {
    return this.editServiceCategoryForm.get("editCategory");
  }

  get editDescription() {
    return this.editServiceCategoryForm.get("editDescription");
  }

  get editCategoryAlreadyExist() {
    return this.serviceCategories
      .filter(category => category.serviceCategory !== this.editPrevCategory)
      .map(category => category.serviceCategory)
      .includes(this.editCategory.value.trim());
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
