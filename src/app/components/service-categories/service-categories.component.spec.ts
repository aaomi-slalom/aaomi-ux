import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceCategoriesComponent } from './service-categories.component';
import { Router } from "@angular/router";
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ServiceCategoryService } from 'src/app/services/service-category/service-category.service';
import { ReactiveFormsModule } from "@angular/forms"

describe('ServiceCategoriesComponent', () => {
  let component: ServiceCategoriesComponent;
  let fixture: ComponentFixture<ServiceCategoriesComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceCategoriesComponent],
      providers: [ServiceCategoryService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
