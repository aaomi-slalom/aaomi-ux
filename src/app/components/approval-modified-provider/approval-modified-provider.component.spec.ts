import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalModifiedProviderComponent } from './approval-modified-provider.component';

describe('ApprovalModifiedProviderComponent', () => {
  let component: ApprovalModifiedProviderComponent;
  let fixture: ComponentFixture<ApprovalModifiedProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalModifiedProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalModifiedProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
