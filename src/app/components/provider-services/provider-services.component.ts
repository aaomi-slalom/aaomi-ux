import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faEdit,
  faPlus,
  faInfoCircle,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { timer, forkJoin } from "rxjs";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import Swal from "sweetalert2";
import { map, flatMap } from "rxjs/operators";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-provider-services",
  templateUrl: "./provider-services.component.html",
  styleUrls: ["./provider-services.component.scss"]
})
export class ProviderServicesComponent implements OnInit {
  createServiceForm: FormGroup;
  editServiceForm: FormGroup;
  faPlus = faPlus;
  faEdit = faEdit;
  faInfoCircle = faInfoCircle;
  faTrashAlt = faTrashAlt;
  serverMessage: string;
  errorMessage: string;
  editPrevCategory: string;
  editPrevService: string;
  editPrevDescription: string;
  editPrevInternal: boolean;
  editServiceId: number;
  serviceIdToDelete: number;
  categoryServicesDtos = [];
  serviceCategories = [];
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("editServiceModal") editServiceModal: SwalComponent;
  @ViewChild("confirmDeleteServiceAlert")
  confirmDeleteServiceAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    forkJoin([
      this.providerServiceService.getAllServices(),
      this.serviceCategoryService.getAllServiceCategories()
    ]).subscribe(result => {
      this.categoryServicesDtos = result[0];
      this.serviceCategories = result[1];
    });

    this.editServiceForm = this.formBuilder.group({
      editCategory: ["", [Validators.required]],
      editService: ["", [Validators.required]],
      editDescription: ["", []],
      editInternal: [false, []]
    });

    this.createServiceForm = this.formBuilder.group({
      category: ["", [Validators.required]],
      service: ["", [Validators.required]],
      description: ["", []],
      internal: [false, []]
    });
  }

  editServiceFire(event): void {
    this.editServiceId = event.currentTarget.dataset.serviceid;
    this.editPrevCategory = event.currentTarget.dataset.servicecategory;
    this.editPrevService = event.currentTarget.dataset.servicename;
    this.editPrevDescription = event.currentTarget.dataset.description;
    this.editPrevInternal = event.currentTarget.dataset.internal === "true";

    this.editInternal.setValue(this.editPrevInternal);
    this.editCategory.setValue(this.editPrevCategory);
    this.editService.setValue(this.editPrevService);
    this.editDescription.setValue(this.editPrevDescription);

    this.editServiceModal.fire();
  }

  editServiceSubmit(): void {
    if (this.editServiceForm.valid) {
      const {
        editCategory,
        editService,
        editDescription,
        editInternal
      } = this.editServiceForm.value;

      this.providerServiceService
        .updateService(
          this.editServiceId,
          editCategory.trim(),
          editService.trim(),
          editDescription.trim(),
          editInternal
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update service...";
            if (error.error.message) {
              this.errorMessage = error.error.message;
            }

            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  createServiceSubmit(): void {
    if (this.createServiceForm.valid) {
      const {
        category,
        service,
        description,
        internal
      } = this.createServiceForm.value;
      this.providerServiceService
        .createService(
          category.trim(),
          service.trim(),
          description.trim(),
          internal
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create service...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  deleteService(event): void {
    this.serviceIdToDelete = event.currentTarget.dataset.serviceid;

    this.confirmDeleteServiceAlert.fire();
  }

  cancel(): void {
    Swal.close();
  }

  confirmDeleteService(): void {
    this.providerServiceService
      .deleteService(this.serviceIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete group...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get category() {
    return this.createServiceForm.get("category");
  }

  get service() {
    return this.createServiceForm.get("service");
  }

  get serviceAlreadyExist() {
    const services = this.categoryServicesDtos.flatMap(
      categoryServices => categoryServices.serviceDtos
    );

    return services
      .map(service => service.service)
      .includes(this.service.value.trim());
  }

  get editCategory() {
    return this.editServiceForm.get("editCategory");
  }

  get editService() {
    return this.editServiceForm.get("editService");
  }

  get editDescription() {
    return this.editServiceForm.get("editDescription");
  }

  get editInternal() {
    return this.editServiceForm.get("editInternal");
  }

  get editServiceAlreadyExist() {
    const services = this.categoryServicesDtos.flatMap(
      categoryServices => categoryServices.serviceDtos
    );

    return services
      .filter(service => service.service !== this.editPrevService)
      .map(service => service.service)
      .includes(this.editService.value.trim());
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
