import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faPlus,
  faEye,
  faTrashAlt,
  faSearch
} from "@fortawesome/free-solid-svg-icons";
import { Router } from "@angular/router";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { environment as ENV } from "../../../environments/environment";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";
import * as moment from "moment";
import { DomSanitizer } from "@angular/platform-browser";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-providers",
  templateUrl: "./providers.component.html",
  styleUrls: ["./providers.component.scss"]
})
export class ProvidersComponent implements OnInit {
  searchProvidersForm: FormGroup;
  faEye = faEye;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  faSearch = faSearch;
  serverMessage: string;
  errorMessage: string;
  totalPages: number;
  totalElements: number;
  pageSize: number = 5;
  providers = [];
  providerIdToDelete;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  wait1Second = timer(1000);
  pn = this.usPhoneNumberService.formatPhoneNumber;
  moment = moment;

  reportRequested: boolean = false;
  reportFetched: boolean = false;
  reportName: string;
  reportData = {
    reportFile: ""
  };

  constructor(
    private auth: AuthService,
    private providerService: ProviderService,
    private usPhoneNumberService: UsPhoneNumberService,
    private communicationService: CommunicationService,
    private router: Router,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer
  ) {}

  sanitize(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();
    const providerSearchParam: string = localStorage.getItem('searchParam') || '';
    this.providerService
      .getAllProvidersByProviderName(providerSearchParam, 0, this.pageSize)
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.providers = response.providerInfoDtos;
      });

    this.searchProvidersForm = this.formBuilder.group({
      providerName: [providerSearchParam, []]
    });
    localStorage.setItem('searchParam', '');
  }

  createProvider(): void {
    this.router.navigate(["/admin-navigator/provider/create"]);
  }

  pageChanged(event: PageChangedEvent): void {
    const providerNameSearch: string = this.searchProvidersForm.get(
      "providerName"
    ).value;

    this.providerService
      .getAllProvidersByProviderName(
        providerNameSearch,
        event.page - 1,
        this.pageSize
      )
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.providers = response.providerInfoDtos;
      });
  }

  searchProvidersByName(searchProvidersForm: FormGroup): void {
    const providerName = searchProvidersForm.value.providerName.toUpperCase();
    localStorage.setItem('searchParam', providerName);

    this.providerService
      .getAllProvidersByProviderName(providerName, 0, this.pageSize)
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.providers = response.providerInfoDtos;
      });
  }

  openProviderView(event) {
    this.router.navigate([
      "/admin/provider/",
      event.currentTarget.dataset.providerid
    ]);
  }

  deleteProvider(event): void {
    this.providerIdToDelete = event.currentTarget.dataset.providerid;

    this.confirmDeleteAlert.fire();
  }

  confirmDelete(): void {
    this.providerService
      .deleteProviderById(this.providerIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete provider...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancelDelete(): void {
    Swal.close();
  }

  generateProviderReport(): void {
    this.reportFetched = false;
    this.reportRequested = true;

    this.providerService.getGrantReport().subscribe(reportData => {
      this.reportData = reportData;
      this.reportFetched = true;
      this.reportRequested = false;
      this.reportName =
        "AAOMI_Provider_Count_Breakdown_" +
        moment().format("MM-DD-YYYYTHH-mm-ss") +
        ".xlsx";
    });
  }

  get providersDisplay() {
    if (this.providers) {
      return this.providers.map(provider => {
        if (!provider.logo) {
          provider.logo = ENV.EMPTY_LOGO_IMG_URL;
        }

        return { ...provider };
      });
    }
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
