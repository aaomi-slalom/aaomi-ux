import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProvidersComponent } from './providers.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { ReactiveFormsModule } from "@angular/forms";

describe('ProvidersComponent', () => {
  let component: ProvidersComponent;
  let fixture: ComponentFixture<ProvidersComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProvidersComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [ProviderService, { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
