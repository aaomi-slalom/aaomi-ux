import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faSearch,
  faEye,
  faTrashAlt,
  faPlus,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UsersService } from "src/app/services/users/users.service";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { PasswordValidator } from "../registration/validators/password.validator";
import { Router } from "@angular/router";
import { environment as ENV } from "../../../environments/environment";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"]
})
export class UsersComponent implements OnInit {
  searchUsersForm: FormGroup;
  createUserForm: FormGroup;
  totalPages: number;
  totalElements: number;
  pageSize: number = 5;
  usernameAvailable: boolean = true;
  emailAvailable: boolean = true;
  accountEnabled: boolean = false;
  usernameDelete: string;
  serverMessage: string;
  errorMessage: string;
  faSearch = faSearch;
  faEye = faEye;
  faTrashAlt = faTrashAlt;
  faPlus = faPlus;
  faExclamationCircle = faExclamationCircle;
  users;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private usersService: UsersService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  checkUsername(): void {
    this.auth.checkUsername(this.username.value).subscribe(response => {
      this.usernameAvailable = response.usernameAvailable;
    });
  }

  checkEmailAndEnabled(): void {
    this.auth.checkEmailAndEnabled(this.email.value).subscribe(response => {
      this.emailAvailable = response.emailAvailable;
      this.accountEnabled = response.accountEnabled;
    });
  }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();
    const userNameSearchParam: string = localStorage.getItem('userName') || '';
    const accountGroupSearchParam: string = localStorage.getItem('accountGroup') || '';
    this.usersService
      .getUsersByUsernameAndAccountGroup(userNameSearchParam, accountGroupSearchParam, 0, this.pageSize)
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.users = response.userDtos;
      });

    this.searchUsersForm = this.formBuilder.group({
      username: [userNameSearchParam, []],
      accountGroup: [accountGroupSearchParam, []]
    });
    localStorage.setItem('userName', '');
    localStorage.setItem('accountGroup', '');

    this.createUserForm = this.formBuilder.group({
      username: [
        "",
        [
          Validators.required,
          Validators.pattern("^(\\d|\\w)+$"),
          Validators.maxLength(50)
        ]
      ],
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.minLength(8), PasswordValidator.strong]
      ],
      confirmPassword: ["", [Validators.required]]
    });
  }

  searchUsers(searchUsersForm: FormGroup): void {
    const usernameSearch: string = searchUsersForm.get('username').value;
    const accountGroupSearch: string = searchUsersForm.get('accountGroup')
      .value;
    localStorage.setItem('userName', usernameSearch);
    localStorage.setItem('accountGroup', accountGroupSearch);

    this.usersService
      .getUsersByUsernameAndAccountGroup(
        usernameSearch,
        accountGroupSearch,
        0,
        this.pageSize
      )
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.users = response.userDtos;
      });
  }

  pageChanged(event: PageChangedEvent): void {
    const usernameSearch: string = this.searchUsersForm.get("username").value;
    const accountGroupSearch: string = this.searchUsersForm.get("accountGroup")
      .value;

    this.usersService
      .getUsersByUsernameAndAccountGroup(
        usernameSearch,
        accountGroupSearch,
        event.page - 1,
        this.pageSize
      )
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.users = response.userDtos;
      });
  }

  openUserView(event): void {
    const username = event.currentTarget.dataset.username;

    this.router.navigate([`/admin/users/user/${username}`]);
  }

  deleteUser(event): void {
    this.usernameDelete = event.currentTarget.dataset.username;

    this.confirmDeleteAlert.fire();
  }

  confirmDelete(): void {
    this.usersService
      .deleteUser(this.usernameDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete user...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancelDelete(): void {
    Swal.close();
  }

  onSubmit(createUserForm: FormGroup): void {
    if (
      createUserForm.valid &&
      this.passwordsMatch &&
      this.emailAvailable &&
      this.usernameAvailable
    ) {
      const {
        username,
        email,
        password,
        confirmPassword
      } = createUserForm.value;

      this.usersService
        .createUser(username, email, password, confirmPassword)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
            this.createUserForm.reset();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create user...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  cancelCreate(): void {
    Swal.close();
  }

  get usersDisplay() {
    if (this.users) {
      return this.users.map(user => {
        let groupsDisplay = [];
        let defaultGroupsDisplay = [];

        if (user.groups.length > 0) {
          groupsDisplay = user.groups.map(group =>
            this.formTrimService.processNamesToDisplay(group, 6)
          );
        }

        if (user.defaultGroups.length > 0) {
          defaultGroupsDisplay = user.defaultGroups.map(group =>
            this.formTrimService.processNamesToDisplay(group, 6)
          );
        }

        if (!user.image) {
          user.image = ENV.EMPTY_PROFILE_IMG_URL;
        }

        return { ...user, groupsDisplay, defaultGroupsDisplay };
      });
    }
  }

  get username() {
    return this.createUserForm.get("username");
  }

  get email() {
    return this.createUserForm.get("email");
  }

  get password() {
    return this.createUserForm.get("password");
  }

  get confirmPassword() {
    return this.createUserForm.get("confirmPassword");
  }

  get passwordsMatch() {
    return (
      this.createUserForm.get("password").value ===
      this.createUserForm.get("confirmPassword").value
    );
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
