
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  returnUrl: string;
  loading = false;

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,  
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.auth.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
    this.loginForm = this.formBuilder.group({
      usernameOrEmail: "",
      password: "",
      rememberMe: false
    });
  }

  onSubmit(loginForm: FormGroup): void {
    if (this.returnUrl === undefined ){
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    const rememberMe = loginForm.value.rememberMe;
    this.loading = true;
    this.auth
      .login(
        loginForm.value.usernameOrEmail.trim(),
        loginForm.value.password.trim()
      )
      .subscribe(
        response => {
          if (rememberMe) {
            localStorage.setItem("token", response.jwt);
          } else {
            sessionStorage.setItem("token", response.jwt);
          }
          if (
            (this.getGroups() == "GROUP_INTERNAL") || (this.getGroups() == "GROUP_NAVIGATOR") || (this.getGroups() == "GROUP_ADMIN")) {
            var re = /user/gi;
            var newUrl = this.returnUrl.replace(re, "admin");
            this.router.navigateByUrl(newUrl);}
           else if ((this.getGroups() == "GROUP_SERVICE_PROVIDER") || (this.getGroups() == "GROUP_FAMILY"))  {
             var profileUrl = "profile/show"
             this.router.navigateByUrl(profileUrl);
           }
           else {
            this.router.navigateByUrl(this.returnUrl);
          }
        },
        error => {
          if (
            error.error.message.includes("Bad credentials") ||
            error.error.message.includes("null")
          ) {
            this.errorMessage = "Incorrect username or password";
          } else if (error.error.message.includes("User is disabled")) {
            this.errorMessage =
              "You may not have finished registering your account, please register again or contact Administrator";
          } else if (error.error.message.includes("User account is locked")) {
            this.errorMessage =
              "Account disabled, please contact Administrator";
          } else {
            this.errorMessage = "Error when trying to login...";
          }

          this.errorAlert.fire();
          this.loading = false;
          return;
        }
      );
  }
  getGroups(): any {
    const helper = new JwtHelperService();
    const token = sessionStorage.getItem("token")
    const decodedToken = helper.decodeToken(token);
    const group = decodedToken.groups[0];
    return group;
  }
}
