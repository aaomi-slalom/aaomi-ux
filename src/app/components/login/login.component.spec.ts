import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LoginComponent } from "./login.component";
import { Observable, of, throwError } from "rxjs";
import { AuthResponseDto } from "src/app/dtos/AuthResponseDto";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { Router } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
import { By } from "@angular/platform-browser";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { DebugElement } from "@angular/core";
import Swal from "sweetalert2";
import { SpyNgModuleFactoryLoader } from "@angular/router/testing";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let injectedAuthService: AuthService;
  let injectedRouter: Router;
  let formDebugElement: DebugElement;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate")
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [AuthService, { provide: Router, useValue: mockRouter }],
      imports: [
        ReactiveFormsModule,
        SweetAlert2Module.forRoot(),
        HttpClientTestingModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    injectedAuthService = TestBed.inject(AuthService);
    injectedRouter = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
    formDebugElement = fixture.debugElement.query(By.css("form"));
  });

  // spy on local storage
  beforeEach(() => {
    let mockLocalStore = {};

    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in mockLocalStore ? mockLocalStore[key] : null;
      },
      setItem: (key: string, value: string) => {
        mockLocalStore[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete mockLocalStore[key];
      },
      clear: () => {
        mockLocalStore = {};
      }
    };

    spyOn(localStorage, "getItem").and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, "setItem").and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, "removeItem").and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, "clear").and.callFake(mockLocalStorage.clear);
  });

  // spy on session storage
  beforeEach(() => {
    let mockSessionStore = {};

    const mockSessionStorage = {
      getItem: (key: string): string => {
        return key in mockSessionStore ? mockSessionStore[key] : null;
      },
      setItem: (key: string, value: string) => {
        mockSessionStore[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete mockSessionStore[key];
      },
      clear: () => {
        mockSessionStore = {};
      }
    };

    spyOn(sessionStorage, "getItem").and.callFake(mockSessionStorage.getItem);
    spyOn(sessionStorage, "setItem").and.callFake(mockSessionStorage.setItem);
    spyOn(sessionStorage, "removeItem").and.callFake(
      mockSessionStorage.removeItem
    );
    spyOn(sessionStorage, "clear").and.callFake(mockSessionStorage.clear);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("template should bind to Form Group", () => {
    // arrange
    const usernameInputElement: HTMLInputElement = fixture.debugElement.query(
      By.css("#usernameOrEmail")
    ).nativeElement;
    const passwordInputElement: HTMLInputElement = fixture.debugElement.query(
      By.css("#password")
    ).nativeElement;
    const rememberMeInputElement: HTMLInputElement = fixture.debugElement.query(
      By.css("#rememberMe")
    ).nativeElement;

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");
    component.loginForm.controls.rememberMe.setValue(true);

    // act
    fixture.detectChanges();

    // assert
    expect(usernameInputElement.value).toEqual("username");
    expect(passwordInputElement.value).toEqual("password");
    expect(rememberMeInputElement.checked).toBeTrue();
  });

  it("should navigate home if successful auth", () => {
    // arrange
    const authServiceSpy = spyOn(injectedAuthService, "login").and.returnValue(
      of<AuthResponseDto>({
        jwt: "token"
      })
    );

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");

    // act
    formDebugElement.triggerEventHandler("ngSubmit", null);

    // assert
    expect(injectedRouter.navigate).toHaveBeenCalledWith([""]);
  });

  it("should show error alert on auth service error", () => {
    // arrange
    const authServiceSpy = spyOn(injectedAuthService, "login").and.returnValue(
      throwError("some error")
    );

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");

    const swalSpy = spyOn(component.errorAlert, "fire").and.callThrough();

    // act
    formDebugElement.triggerEventHandler("ngSubmit", null);
    fixture.detectChanges();

    // assert
    expect(swalSpy).toHaveBeenCalledTimes(1);
  });

  it("sets token in local storage if remember me is checked and auth succeeds", () => {
    const authServiceSpy = spyOn(injectedAuthService, "login").and.returnValue(
      of<AuthResponseDto>({
        jwt: "some token"
      })
    );

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");
    component.loginForm.controls.rememberMe.setValue(true);

    // act
    formDebugElement.triggerEventHandler("ngSubmit", null);

    // assert
    expect(localStorage.getItem("token")).toEqual("some token");
  });

  it("sets token in session storage if remember me is not checked and auth succeeds", () => {
    // arrange
    const authServiceSpy = spyOn(injectedAuthService, "login").and.returnValue(
      of<AuthResponseDto>({
        jwt: "some token"
      })
    );

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");
    component.loginForm.controls.rememberMe.setValue(false);

    // act
    formDebugElement.triggerEventHandler("ngSubmit", null);

    // assert
    expect(sessionStorage.getItem("token")).toEqual("some token");
  });

  it("error alert is hidden by default", () => {
    // arrange
    const swalContainer = fixture.debugElement.query(
      By.css(".swal2-container")
    );

    // act

    // assert
    expect(swalContainer).toBeFalsy();
  });

  it("form provides correct inputs to auth service", () => {
    // arrange
    const authServiceSpy = spyOn(injectedAuthService, "login").and.returnValue(
      of<AuthResponseDto>({
        jwt: "token"
      })
    );

    component.loginForm.controls.usernameOrEmail.setValue("username");
    component.loginForm.controls.password.setValue("password");

    // act
    formDebugElement.triggerEventHandler("ngSubmit", null);

    // assert
    expect(authServiceSpy).toHaveBeenCalledWith("username", "password");
  });
});
