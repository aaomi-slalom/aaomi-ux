import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderNoteService } from "src/app/services/provider-note/provider-note.service";
import { ActivatedRoute } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { forkJoin, timer } from "rxjs";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import * as moment from "moment";
import Swal from "sweetalert2";
import swal from 'sweetalert2';
import {
  faSearch,
  faTimes,
  faTrashAlt,
  faEdit
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-provider-notes",
  templateUrl: "./provider-notes.component.html",
  styleUrls: ["./provider-notes.component.scss"]
})

export class ProviderNotesComponent implements OnInit {
  filterNotesForm: FormGroup;
  createNoteForm: FormGroup;
  editNoteForm: FormGroup;
  noteSources = [];
  filteredNotes = [];
  notes = [];
  noteOptions = [];
  serverMessage: string;
  errorMessage: string;
  faSearch = faSearch;
  faTimes = faTimes;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  providerInfoId;
  noteIdToDelete;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  @ViewChild("editNoteModal") editNoteModal: SwalComponent;
  moment = moment;
  waitOneSecond = timer(1000);
  noteTitleToEdit: any;
  noteBodyToEdit;
  noteIdToEdit;

  constructor(
    private auth: AuthService,
    private providerNoteService: ProviderNoteService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.filterNotesForm = this.formBuilder.group({
      source: ["", [Validators.required]],
      note: ["", [Validators.required]],
      title: ["", [Validators.required]]
    });

    this.createNoteForm = this.formBuilder.group({
      note: ["", [Validators.required]],
      title: ["", [Validators.required]],
      noteLinkIdType: ["", [Validators.required]]
    });

    this.editNoteForm = this.formBuilder.group({
      noteId: [""],
      title: ["", [Validators.required]],
      note: ["", [Validators.required]],
    });

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() => {
          const notes = this.providerNoteService.getAllNotesByProviderInfoId(
            this.providerInfoId
          );
          const noteOptions = this.providerNoteService.getAllNoteOptionsByProviderInfoId(
            this.providerInfoId
          );
          return forkJoin([notes, noteOptions]);
        })
      )
      .subscribe(results => {
        this.notes = results[0];
        this.noteOptions = results[1];
        this.noteSources = [];

        const sourcesSeen = new Set();
        this.notes.forEach(note => {
          if (!sourcesSeen.has(note.source)) {
            this.noteSources.push({ type: note.type, source: note.source });
          }
          sourcesSeen.add(note.source);
        });
        this.filteredNotes = this.notes;
      });
  }

  createNote(): void {
    if (this.createNoteForm.valid) {
      const { noteLinkIdType, title, note } = this.createNoteForm.value;
      const [noteType, noteLinkId] = noteLinkIdType.split("^%$%^");

      this.providerNoteService
        .createNote(this.providerInfoId, noteLinkId, noteType, note, title)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.waitOneSecond)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create note...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  filterNotes(): void {
    if (this.filterNotesForm.valid) {
      const { source } = this.filterNotesForm.value;
      this.filteredNotes = this.notes.filter(note => note.source === source);
    }
  }

  cancelFilter(): void {
    this.filteredNotes = this.notes;
    this.filterNotesForm.reset();
  }

  editNote(event): void {
    this.noteIdToEdit = event.currentTarget.dataset.noteid;
    let noteToEdit;
    for (let i = 0; i < this.filteredNotes.length; i++) {
      if (this.filteredNotes[i].noteId == this.noteIdToEdit) {
        noteToEdit = this.filteredNotes[i]
      }
    }
    this.editNoteForm.setValue({
      noteId: noteToEdit.noteId,
      title: noteToEdit.title,
      note: noteToEdit.note,

    });
    this.editNoteModal.fire();
  }

  revisedNote() {
    if (this.editNoteForm.valid) {
      const { noteId, title, note } = this.editNoteForm.value;
      this.providerNoteService
        .editNote(
          noteId,
          title,
          note
        )
        .subscribe(
          () => {
            swal.fire({
              text: 'Note saved successfully',
              timer: 1000,
              showConfirmButton: false
            })
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to edit note...";
            this.errorAlert.fire()
          }
        );

    }
  }

  cancelEdit(): void {
    Swal.close();
  }

  deleteNote(event): void {
    this.noteIdToDelete = event.currentTarget.dataset.noteid;
    this.confirmDeleteAlert.fire();
  }

  cancelDelete(): void {
    Swal.close();
  }

  confirmDelete(): void {
    this.providerNoteService
      .deleteNoteByNoteId(this.noteIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.waitOneSecond)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete note...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get note() {
    return this.createNoteForm.get("note");
  }

  get title() {
    return this.createNoteForm.get("title");
  }

  get noteId() {
    return this.createNoteForm.get("noteId");
  }

  get noteLinkIdType() {
    return this.createNoteForm.get("noteLinkIdType");
  }

  get f() { return this.editNoteForm.controls; }

}
