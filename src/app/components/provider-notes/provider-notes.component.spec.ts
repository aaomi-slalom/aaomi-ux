import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderNotesComponent } from './provider-notes.component';

describe('ProviderNotesComponent', () => {
  let component: ProviderNotesComponent;
  let fixture: ComponentFixture<ProviderNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
