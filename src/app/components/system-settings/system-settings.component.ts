import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SystemSettingsService } from "src/app/services/system-settings/system-settings.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { map, flatMap } from "rxjs/operators";
import { timer } from "rxjs";
import Swal from "sweetalert2";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { environment as ENV } from "../../../environments/environment";

@Component({
  selector: "app-system-settings",
  templateUrl: "./system-settings.component.html",
  styleUrls: ["./system-settings.component.scss"]
})
export class SystemSettingsComponent implements OnInit {
  editBackgroundImageForm: FormGroup;
  editSearchSupportNoteForm: FormGroup;
  editTechnicalSupportNoteForm: FormGroup;
  editDisclaimerForm: FormGroup;
  serverMessage: String;
  errorMessage: String;
  imageBuffer: string | ArrayBuffer;
  previousBgImage = ENV.DEFAULT_BG_IMG;

  @ViewChild("editBackgroundImageModal")
  editBackgroundImageModal: SwalComponent;

  @ViewChild("editSearchSupportNoteModal")
  editSearchSupportNoteModal: SwalComponent;

  @ViewChild("editTechnicalSupportNoteModal")
  editTechnicalSupportNoteModal: SwalComponent;

  @ViewChild("editDisclaimerModal") editDisclaimerModal: SwalComponent;

  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  wait1Second = timer(1000);

  settingsPageRoutesProviders = [
    { name: "Service Categories", link: "/admin/service/categories" },
    { name: "Services", link: "/admin/services" },
    { name: "Service Delivery", link: "/admin/service/deliveries" },
    { name: "Insurance", link: "/admin/insurances" },
    { name: "Age Groups", link: "/admin/agegroups" }
  ];
  settingsPageRoutesUserManagement = [
    { name: "Users", link: "/admin/users/view" },
    { name: "Groups", link: "/admin/groups/view" },
    { name: "Permissions", link: "/admin/permissions/view" },
    { name: "Roles", link: "/admin/roles/view" }
  ];

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "0",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    sanitize: false,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      ["fontName"],
      [
        "insertImage",
        "insertVideo",
        "insertHorizontalRule",
        "backgroundColor",
        "customClasses",
        "fontSize",
        "toggleEditorMode"
      ]
    ]
  };

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private systemSettingsService: SystemSettingsService,
    private communicationService: CommunicationService
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.editBackgroundImageForm = this.formBuilder.group({
      editBackgroundImage: ["", [Validators.required]]
    });

    this.editSearchSupportNoteForm = this.formBuilder.group({
      editSearchSupportNote: ["", [Validators.required]]
    });

    this.editTechnicalSupportNoteForm = this.formBuilder.group({
      editTechnicalSupportNote: ["", [Validators.required]]
    });

    this.editDisclaimerForm = this.formBuilder.group({
      editDisclaimer: ["", [Validators.required]]
    });

    this.systemSettingsService
      .getCurrentSystemSettings()
      .subscribe(systemSettings => {
        if (systemSettings.backgroundImage) {
          this.imageBuffer = systemSettings.backgroundImage;
          this.editBackgroundImage.setValue(systemSettings.backgroundImage);
          this.communicationService.setData(
            "appBackgroundImage",
            systemSettings.backgroundImage
          );
        } else {
          this.imageBuffer = null;

          this.communicationService.setData(
            "appBackgroundImage",
            ENV.DEFAULT_BG_IMG
          );
        }

        this.editSearchSupportNote.setValue(systemSettings.searchSupportNote);
        this.editTechnicalSupportNote.setValue(
          systemSettings.technicalSupportNote
        );
        this.editDisclaimer.setValue(systemSettings.disclaimer);

        this.communicationService.setData(
          "appSearchSupportNote",
          systemSettings.searchSupportNote
        );
        this.communicationService.setData(
          "appTechnicalSupportNote",
          systemSettings.technicalSupportNote
        );
        this.communicationService.setData(
          "appDisclaimer",
          systemSettings.disclaimer
        );
      });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  editBackgroundImageSubmit(): void {
    if (this.editBackgroundImageForm.valid) {
      this.systemSettingsService
        .updateBackgroundImage(this.imageBuffer)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update background image...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editSearchSupportNoteSubmit(): void {
    if (this.editSearchSupportNoteForm.valid) {
      this.systemSettingsService
        .updateSearchSupportNote(this.editSearchSupportNote.value.trim())
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update search support note...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editTechnicalSupportNoteSubmit(): void {
    if (this.editTechnicalSupportNoteForm.valid) {
      this.systemSettingsService
        .updateTechnicalSupportNote(this.editTechnicalSupportNote.value.trim())
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update technical support note...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editDisclaimerSubmit(): void {
    if (this.editDisclaimerForm.valid) {
      this.systemSettingsService
        .updateDisclaimer(this.editDisclaimer.value.trim())
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update disclaimer...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  get editBackgroundImage() {
    return this.editBackgroundImageForm.get("editBackgroundImage");
  }

  get editSearchSupportNote() {
    return this.editSearchSupportNoteForm.get("editSearchSupportNote");
  }

  get editTechnicalSupportNote() {
    return this.editTechnicalSupportNoteForm.get("editTechnicalSupportNote");
  }

  get editDisclaimer() {
    return this.editDisclaimerForm.get("editDisclaimer");
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
