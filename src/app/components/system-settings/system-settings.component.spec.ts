import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SystemSettingsComponent } from './system-settings.component';
import { Router } from "@angular/router";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { SystemSettingsService } from "src/app/services/system-settings/system-settings.service";
import { ReactiveFormsModule } from '@angular/forms'


describe('SystemSettingsComponent', () => {
  let component: SystemSettingsComponent;
  let fixture: ComponentFixture<SystemSettingsComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SystemSettingsComponent],
      providers: [SystemSettingsService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
