import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProviderWaitTimesComponent } from './provider-wait-times.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Params, Router, UrlSegment } from "@angular/router";
import { FormBuilder, ReactiveFormsModule } from "@angular/forms"
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderWaitTimeService } from "src/app/services/provider-wait-time/provider-wait-time.service";
import { ActivatedRoute } from "@angular/router"
import { of } from 'rxjs';

describe('ProviderWaitTimesComponent', () => {
  let component: ProviderWaitTimesComponent;
  let fixture: ComponentFixture<ProviderWaitTimesComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  const fakeActivatedRoute = {
    snapshot: {
      params: {
        result: 'some result'
      },
      queryParams: {
        id: '3',
        token: 'some token'
      }
    },
    params: of<Params>(),
    queryParams: of<Params>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderWaitTimesComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [AuthService, { provide: Router, useValue: mockRouter },
        FormBuilder,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderWaitTimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
