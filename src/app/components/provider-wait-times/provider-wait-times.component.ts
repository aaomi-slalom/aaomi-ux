import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { ProviderWaitTimeService } from "src/app/services/provider-wait-time/provider-wait-time.service";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  faSearch,
  faTimes,
  faTrashAlt,
  faEdit
} from "@fortawesome/free-solid-svg-icons";
import { timer, forkJoin } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import * as moment from "moment";

@Component({
  selector: "app-provider-wait-times",
  templateUrl: "./provider-wait-times.component.html",
  styleUrls: ["./provider-wait-times.component.scss"]
})
export class ProviderWaitTimesComponent implements OnInit {
  filterWaitTimesForm: FormGroup;
  createWaitTimeForm: FormGroup;
  editWaitTimeForm: FormGroup;
  waitTimeSources = [];
  waitTimeDtos = [];
  waitTimeOptionDtos = [];
  filteredWaitTimes = [];
  providerInfoId;
  waitTimeId;
  waitTimeIdToDelete;
  currTitle;
  currWaitTime;
  faSearch = faSearch;
  faTimes = faTimes;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  errorMessage: string;
  serverMessage: string;
  @ViewChild("editWaitTimeModal") editWaitTimeModal: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("confirmDeleteWaitTimeAlert")
  confirmDeleteWaitTimeAlert: SwalComponent;
  moment = moment;
  wait1Second = timer(1000);

  constructor(
    private auth: AuthService,
    public readonly swalTargets: SwalPortalTargets,
    private waitTimeService: ProviderWaitTimeService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.createWaitTimeForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      waitTime: ["", [Validators.required]],
      waitTimeLinkIdType: ["", [Validators.required]]
    });

    this.editWaitTimeForm = this.formBuilder.group({
      updatedTitle: ["", []],
      updatedWaitTime: ["", [Validators.required]]
    });

    this.filterWaitTimesForm = this.formBuilder.group({
      source: ["", [Validators.required]]
    });

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() => {
          const waitTimes = this.waitTimeService.getAllWaitTimesByProviderInfoId(
            this.providerInfoId
          );
          const waitTimeOptions = this.waitTimeService.getAllWaitTimeOptionsByProviderInfoId(
            this.providerInfoId
          );

          return forkJoin([waitTimes, waitTimeOptions]);
        })
      )
      .subscribe(results => {
        this.waitTimeDtos = results[0];
        this.waitTimeOptionDtos = results[1];
        this.waitTimeSources = [];

        const sourcesSeen = new Set();
        this.waitTimeDtos.forEach(waitTime => {
          if (!sourcesSeen.has(waitTime.source)) {
            this.waitTimeSources.push({
              type: waitTime.waitTimeType,
              source: waitTime.source
            });
          }
          sourcesSeen.add(waitTime.source);
        });

        this.filteredWaitTimes = this.waitTimeDtos;
      });
  }

  createWaitTime(): void {
    if (this.createWaitTimeForm.valid) {
      const {
        title,
        waitTime,
        waitTimeLinkIdType
      } = this.createWaitTimeForm.value;

      const [waitTimeType, waitTimeLinkId] = waitTimeLinkIdType.split("^%$%^");

      this.waitTimeService
        .createNewWaitTime(
          this.providerInfoId,
          waitTimeLinkId,
          waitTimeType,
          title,
          waitTime
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create wait time...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  public editWaitTime(event): void {
    this.waitTimeId = event.currentTarget.dataset.waittimeid;
    this.currTitle = event.currentTarget.dataset.title;
    this.currWaitTime = event.currentTarget.dataset.waittime;

    this.updatedTitle.setValue(this.currTitle);
    this.updatedWaitTime.setValue(this.currWaitTime);
    this.editWaitTimeModal.fire();
  }

  public editWaitTimeSubmit(): void {
    if (this.editWaitTimeForm.valid) {
      this.waitTimeService
        .updateExistingWaitTime(
          this.waitTimeId,
          this.updatedTitle.value.trim(),
          this.updatedWaitTime.value.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update wait time...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  public deleteWaitTime(event): void {
    this.waitTimeIdToDelete = event.currentTarget.dataset.waittimeid;

    this.confirmDeleteWaitTimeAlert.fire();
  }

  public deleteWaitTimeConfirm(): void {
    this.waitTimeService
      .deleteExistingWaitTime(this.waitTimeIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete wait time...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  filterWaitTimes(): void {
    if (this.filterWaitTimesForm.valid) {
      const { source } = this.filterWaitTimesForm.value;
      this.filteredWaitTimes = this.waitTimeDtos.filter(
        waitTime => waitTime.source === source
      );
    }
  }

  cancelFilter(): void {
    this.filteredWaitTimes = this.waitTimeDtos;
    this.filterWaitTimesForm.reset();
  }

  cancel(): void {
    Swal.close();
  }

  get title() {
    return this.createWaitTimeForm.get("title");
  }

  get waitTime() {
    return this.createWaitTimeForm.get("waitTime");
  }

  get waitTimeLinkIdType() {
    return this.createWaitTimeForm.get("waitTimeLinkIdType");
  }

  get updatedTitle() {
    return this.editWaitTimeForm.get("updatedTitle");
  }

  get updatedWaitTime() {
    return this.editWaitTimeForm.get("updatedWaitTime");
  }
}
