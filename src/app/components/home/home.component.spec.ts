import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HomeComponent } from "./home.component";
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProfileShowService } from 'src/app/services/profile/profile-show/profile-show.service';
import { CommunicationService } from 'src/app/services/communication/communication.service';
import { of } from 'rxjs';
import { SecuredHtmlPipe } from "src/app/pipes/secured-html/secured-html.pipe"

describe("HomeComponent", () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  let injectedAuthService: AuthService;
  let injectedProfileShowService: ProfileShowService;
  let injectedCommunicationService: CommunicationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        ProfileShowService,
        CommunicationService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [HomeComponent, SecuredHtmlPipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    injectedAuthService = TestBed.inject(AuthService);
    injectedProfileShowService = TestBed.inject(ProfileShowService);
    injectedCommunicationService = TestBed.inject(CommunicationService);
    spyOn(injectedAuthService, 'getRole').and.returnValue('role1234_role5678');
    spyOn(injectedAuthService, 'hasRoles').and.returnValue(true);
    spyOn(injectedProfileShowService, 'getProfile').and.returnValue(of<any>());

    spyOn(injectedCommunicationService, 'getData').and.returnValue({ onProfileChild: 'some value' });
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
