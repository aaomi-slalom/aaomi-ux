import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProfileShowService } from "src/app/services/profile/profile-show/profile-show.service";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { environment as ENV } from "../../../environments/environment";
import { ProviderService } from "src/app/services/provider/provider/provider.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  isProvider: boolean;
  hasProviderProfile: boolean;
  isFamily: boolean;
  hasProfile: boolean;
  firstName: string;
  lastName: string;
  accountRole: string;
  imageBuffer: string;
  onProfileChild: string;

  constructor(
    private auth: AuthService,
    private profileShowService: ProfileShowService,
    private communicationService: CommunicationService,
    private formTrimService: FormTrimService,
    private providerService: ProviderService,
    private cdr: ChangeDetectorRef
  ) {
    this.communicationService.changeEmitted$.subscribe(() => {
      this.loadUserProfile();

      this.onProfileChild = this.communicationService.getData()[
        "onProfileChild"
      ];

      this.cdr.detectChanges();
    });
  }

  ngOnInit(): void {
    this.isFamily = this.auth.hasRoles(["ROLE_FAMILY"]);
    this.isProvider =
      this.auth.hasRoles(["ROLE_SERVICE_PROVIDER"]) ||
      this.auth.hasGroups(["GROUP_SERVICE_PROVIDER"]);
    this.onProfileChild = this.communicationService.getData()["onProfileChild"];

    this.providerService.checkHasProviderProfile().subscribe(response => {
      this.hasProviderProfile = response.hasProviderProfile;
    });

    this.loadAccountRole();
    this.loadUserProfile();
  }

  loadAccountRole(): void {
    if (this.auth.getRole()) {
      this.accountRole = this.formTrimService.processNamesToDisplay(
        this.auth.getRole(),
        5
      );
    }
  }

  loadUserProfile(): void {
    this.profileShowService.getProfile().subscribe(
      response => {
        this.hasProfile = response != null;

        if (!response) {
          this.firstName = "New";
          this.lastName = "User";
          this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;
        } else {
          if (response.image) {
            this.imageBuffer = response.image;
          } else {
            this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;
          }

          this.firstName = response.firstName;
          this.lastName = response.lastName;
        }
      },
      error => {
        this.auth.logout();
        throw error;
      }
    );
  }

  get technicalSupportNote() {
    return this.communicationService.getData()["appTechnicalSupportNote"];
  }
}
