import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderInfoCreateComponent } from './provider-info-create.component';

describe('ProviderInfoCreateComponent', () => {
  let component: ProviderInfoCreateComponent;
  let fixture: ComponentFixture<ProviderInfoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderInfoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderInfoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
