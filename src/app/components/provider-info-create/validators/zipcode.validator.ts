import { FormControl } from "@angular/forms";
import { postcodeValidator } from "postcode-validator";

export interface ValidationResult {
  [key: string]: boolean;
}

export class ZipcodeValidator {
  public static zipcode(control: FormControl): ValidationResult {
    let isUSZip = postcodeValidator(control.value, "US");

    const valid = isUSZip || !control.value;

    if (!valid) {
      return { zipcode: true };
    }

    return null;
  }
}
