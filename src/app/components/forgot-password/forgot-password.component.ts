import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  forgotpasswordForm: FormGroup;
  serverMessage: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.forgotpasswordForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }

  onSubmit(forgotpasswordForm: FormGroup): void {
    if (forgotpasswordForm.valid) {
      const email = forgotpasswordForm.value.email;

      this.auth.sendResetRequest(email).subscribe(
        response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        },
        error => {
          this.errorAlert.fire();
          throw error;
        }
      );
    } else {
      this.errorAlert.fire();
    }
  }

  get email() {
    return this.forgotpasswordForm.get("email");
  }
}
