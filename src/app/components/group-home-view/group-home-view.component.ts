import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-group-home-view",
  templateUrl: "./group-home-view.component.html",
  styleUrls: ["./group-home-view.component.scss"]
})
export class GroupHomeViewComponent implements OnInit {
  accountGroup: string;
  accountGroupDisplay: string;

  constructor(
    private auth: AuthService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.route.url.subscribe(url => {
      this.accountGroup = url[2].path;

      this.accountGroupDisplay = this.formTrimService.processNamesToDisplay(
        this.accountGroup,
        6
      );
    });
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
