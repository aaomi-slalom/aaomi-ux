import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupHomeViewComponent } from './group-home-view.component';

describe('GroupHomeViewComponent', () => {
  let component: GroupHomeViewComponent;
  let fixture: ComponentFixture<GroupHomeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupHomeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupHomeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
