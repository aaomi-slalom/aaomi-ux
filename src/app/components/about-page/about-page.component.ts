import { Component, OnInit } from "@angular/core";
import { AboutService } from "src/app/services/about/about.service";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-about-page",
  templateUrl: "./about-page.component.html",
  styleUrls: ["./about-page.component.scss"]
})
export class AboutPageComponent implements OnInit {
  aboutPageDto = {
    uxVersion: "",
    coreVersion: ""
  };

  constructor(
    private aboutService: AboutService,
    private auth: AuthService,
    private communicationService: CommunicationService
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.aboutService.getVersionInformation().subscribe(aboutPage => {
      this.aboutPageDto = aboutPage;
    });
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
