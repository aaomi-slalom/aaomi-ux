import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { RoleService } from "src/app/services/role/role.service";
import { faEdit } from "@fortawesome/free-regular-svg-icons";
import { GroupService } from "src/app/services/group/group.service";
import { group } from "@angular/animations";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import Swal from "sweetalert2";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-user-roles",
  templateUrl: "./user-roles.component.html",
  styleUrls: ["./user-roles.component.scss"]
})
export class UserRolesComponent implements OnInit {
  roles;
  groupNames;
  faEdit = faEdit;
  defaultRoleSelected: string;
  defaultGroupSelected: string;
  serverMessage: string;
  @ViewChild("changeGroupModal") private changeGroupModal: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private roleService: RoleService,
    private groupService: GroupService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.roleService.getAllRoles().subscribe(rolesRaw => {
      const roles = rolesRaw.map(role => {
        const roleNameDisplay = this.formTrimService.processNamesToDisplay(
          role.role,
          5
        );
        const groupNameDisplay = this.formTrimService.processNamesToDisplay(
          role.defaultGroup,
          6
        );
        return { ...role, roleNameDisplay, groupNameDisplay };
      });

      this.roles = roles;
    });

    this.groupService.getAllGroupNames().subscribe(groupNamesRaw => {
      const groupNames = groupNamesRaw.map(groupName => {
        const groupNameDisplay = this.formTrimService.processNamesToDisplay(
          groupName,
          6
        );
        return { groupName, groupNameDisplay };
      });

      this.groupNames = groupNames;
    });
  }

  editDefaultGroup(roleName: string, defaultGroupName: string) {
    this.defaultRoleSelected = roleName;
    this.defaultGroupSelected = defaultGroupName;
    this.changeGroupModal.fire();
  }

  onSubmit() {
    this.roleService
      .updateDefaultGroupOnRole(
        this.defaultRoleSelected,
        this.defaultGroupSelected
      )
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
