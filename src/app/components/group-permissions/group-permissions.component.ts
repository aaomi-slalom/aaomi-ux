import { Component, OnInit, ViewChild } from "@angular/core";
import { GroupService } from "src/app/services/group/group.service";
import { PermissionService } from "src/app/services/permission/permission.service";
import { ActivatedRoute } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { error } from "protractor";

@Component({
  selector: "app-group-permissions",
  templateUrl: "./group-permissions.component.html",
  styleUrls: ["./group-permissions.component.scss"]
})
export class GroupPermissionsComponent implements OnInit {
  accountGroup: string;
  adminGroup: string;
  errorMessage: string;
  groupPermissions: string[];
  allPermissions;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  constructor(
    private permissionService: PermissionService,
    private groupService: GroupService,
    private formTrimService: FormTrimService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.parent.url
      .pipe(
        map(url => (this.accountGroup = url[2].path)),
        flatMap(() => this.groupService.getGroupPermissions(this.accountGroup)),
        map(groupPermissions => (this.groupPermissions = groupPermissions)),
        flatMap(() => this.permissionService.getAllPermissions())
      )
      .subscribe(allPermissionsRaw => {
        const allPermissions = allPermissionsRaw.map(permission => {
          const permissionNameDisplay = this.formTrimService.processNamesToDisplay(
            permission.permission,
            5
          );

          const checked: boolean = this.groupPermissions.includes(
            permission.permission
          );

          return { ...permission, permissionNameDisplay, checked };
        });

        this.allPermissions = allPermissions;
      });

    this.groupService.getAppAdminGroup().subscribe(response => {
      this.adminGroup = response.message;
    });
  }

  updateGroupPermission(event): void {
    const accountGroup = this.accountGroup;
    const permission = event.currentTarget.dataset.permission;

    if (event.currentTarget.checked) {
      this.groupService
        .addPermissionToGroup(accountGroup, permission)
        .subscribe(
          () => {},
          error => {
            (this.errorMessage =
              "Error when trying to add permission to group..."),
              this.errorAlert.fire();
          }
        );
    } else {
      this.groupService
        .deletePermissionFromGroup(accountGroup, permission)
        .subscribe(
          () => {},
          error => {
            this.errorMessage =
              "Error when trying to delete permission from group...";
            this.errorAlert.fire();
          }
        );
    }
  }
}
