import { Component, OnInit } from "@angular/core";
import { ProfileShowService } from "src/app/services/profile/profile-show/profile-show.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { environment as ENV } from "../../../environments/environment";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";

@Component({
  selector: "app-profile-show",
  templateUrl: "./profile-show.component.html",
  styleUrls: ["./profile-show.component.scss"]
})
export class ProfileShowComponent implements OnInit {
  imageBuffer: string;
  firstName: string;
  lastName: string;
  contactNumber: string = "";
  website: string;
  bio: string;
  pn = this.usPhoneNumberService.formatPhoneNumber;

  constructor(
    private profileShowService: ProfileShowService,
    private communicationService: CommunicationService,
    private usPhoneNumberService: UsPhoneNumberService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.profileShowService.getProfile().subscribe(response => {
      if (response) {
        this.communicationService.setData("hasProfile", true);

        if (response.image) {
          this.imageBuffer = response.image;
        } else {
          this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;
        }

        this.firstName = response.firstName;
        this.lastName = response.lastName;
        this.contactNumber = response.contactNumber;
        this.website = response.website;
        this.bio = response.bio;
      }
    });

    this.route.url.subscribe(urlVal => {
      this.communicationService.setData("onProfileChild", urlVal[1].path);
      this.communicationService.emitChange();
    });
  }

  edit(): void {
    this.router.navigate(["/profile/edit"]);
  }
}
