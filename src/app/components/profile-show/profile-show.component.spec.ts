import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ProfileShowComponent } from "./profile-show.component";
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router, ActivatedRoute, Params, UrlSegment } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

describe("ProfileShowComponent", () => {
  let component: ProfileShowComponent;
  let fixture: ComponentFixture<ProfileShowComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const fakeActivatedRoute = {
    snapshot: {
      params: {
        result: 'some result'
      }
    },
    params: of<Params>(),
    url: of<UrlSegment[]>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
        FormBuilder,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute }
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [ProfileShowComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
