import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicHolderComponent } from './public-holder.component';

describe('PublicHolderComponent', () => {
  let component: PublicHolderComponent;
  let fixture: ComponentFixture<PublicHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
