import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faEdit,
  faPlus,
  faEye,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { SwalPortalTargets, SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { UrlValidator } from "../profile-create/validators/url.validator";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-insurances",
  templateUrl: "./insurances.component.html",
  styleUrls: ["./insurances.component.scss"]
})
export class InsurancesComponent implements OnInit {
  createInsuranceForm: FormGroup;
  editInsuranceForm: FormGroup;
  faEye = faEye;
  faEdit = faEdit;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  insuranceDtos = [];
  errorMessage: string;
  serverMessage: string;
  editPrevInsuranceName: string;
  editPrevWebsite: string;
  editPrevContactNumber: string;
  currInsuranceName: string;
  currInsuranceWebsite: string;
  currInsuranceContactNumber: string;
  editInsuranceId: number;
  insuranceIdToDelete: number;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("editInsuranceModal") editInsuranceModal: SwalComponent;
  @ViewChild("showInsuranceModal") showInsuranceModal: SwalComponent;
  @ViewChild("confirmDeleteInsuranceAlert")
  confirmDeleteInsuranceAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private insuranceService: InsuranceService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.insuranceService.getAllInsurances().subscribe(insuranceDtos => {
      this.insuranceDtos = insuranceDtos;
    });

    this.createInsuranceForm = this.formBuilder.group({
      insuranceName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      contactNumber: ["", [Validators.pattern("^\\+?\\d{0,15}")]]
    });

    this.editInsuranceForm = this.formBuilder.group({
      editInsuranceName: ["", [Validators.required]],
      editWebsite: ["", [UrlValidator.isValidUrl]],
      editContactNumber: ["", [Validators.pattern("^\\+?\\d{0,15}")]]
    });
  }

  createInsurance(): void {
    if (this.createInsuranceForm.valid && !this.insuranceAlreadyExist) {
      const {
        insuranceName,
        website,
        contactNumber
      } = this.createInsuranceForm.value;
      this.insuranceService
        .createInsurance(
          insuranceName.trim(),
          website.trim(),
          contactNumber.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create insurance...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editInsurance(event): void {
    this.editInsuranceId = event.currentTarget.dataset.insuranceid;
    this.editPrevInsuranceName = event.currentTarget.dataset.insurancename;
    this.editPrevWebsite = event.currentTarget.dataset.website;
    this.editPrevContactNumber = event.currentTarget.dataset.contactnumber;

    this.editInsuranceName.setValue(this.editPrevInsuranceName);
    this.editWebsite.setValue(this.editPrevWebsite);
    this.editContactNumber.setValue(this.editPrevContactNumber);
    this.editInsuranceModal.fire();
  }

  editInsuranceSubmit(): void {
    if (this.editInsuranceForm.valid && !this.editInsuranceAlreadyExist) {
      this.insuranceService
        .updateInsurance(
          this.editInsuranceId,
          this.editInsuranceName.value.trim(),
          this.editWebsite.value.trim(),
          this.editContactNumber.value.trim()
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update insurance...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  showInsuranceDetails(event): void {
    this.currInsuranceName = event.currentTarget.dataset.insurancename;
    this.currInsuranceWebsite = event.currentTarget.dataset.website;
    this.currInsuranceContactNumber = event.currentTarget.dataset.contactnumber;

    this.showInsuranceModal.fire();
  }

  close(): void {
    Swal.close();
  }

  cancel(): void {
    Swal.close();
  }

  deleteInsurance(event): void {
    this.insuranceIdToDelete = event.currentTarget.dataset.insuranceid;
    this.confirmDeleteInsuranceAlert.fire();
  }

  confirmDeleteInsurance(): void {
    this.insuranceService
      .deleteInsurance(this.insuranceIdToDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete insurance...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get insuranceName() {
    return this.createInsuranceForm.get("insuranceName");
  }

  get contactNumber() {
    return this.createInsuranceForm.get("contactNumber");
  }

  get website() {
    return this.createInsuranceForm.get("website");
  }

  get insuranceAlreadyExist() {
    return this.insuranceDtos
      .map(insurance => insurance.insuranceName)
      .includes(this.insuranceName.value.trim());
  }

  get editInsuranceName() {
    return this.editInsuranceForm.get("editInsuranceName");
  }

  get editContactNumber() {
    return this.editInsuranceForm.get("editContactNumber");
  }

  get editWebsite() {
    return this.editInsuranceForm.get("editWebsite");
  }

  get editInsuranceAlreadyExist() {
    return this.insuranceDtos
      .filter(
        insurance => insurance.insuranceName !== this.editPrevInsuranceName
      )
      .map(insurance => insurance.insuranceName)
      .includes(this.editInsuranceName.value.trim());
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
