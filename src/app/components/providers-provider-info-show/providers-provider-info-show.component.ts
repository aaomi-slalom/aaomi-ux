import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import {
  faPlus,
  faEye,
  faTrashAlt,
  faEdit,
  faSave,
  faTimes,
  faClone
} from "@fortawesome/free-solid-svg-icons";
import { LocationService } from "src/app/services/location/location.service";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer, forkJoin } from "rxjs";
import Swal from "sweetalert2";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ZipcodeValidator, ValidationResult } from '../provider-info-create/validators/zipcode.validator';
import scus from "state-counties-us";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";
import { ContactTypeService } from "src/app/services/contact-type/contact-type.service";
import { DeliveryService } from "src/app/services/delivery/delivery.service";
import { CartesianService } from "src/app/services/cartesian/cartesian.service";
import { MiCountyService } from "src/app/services/mi-county/mi-county.service";
import { v4 as uuidv4 } from "uuid";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";

@Component({
  selector: "app-providers-provider-info-show",
  templateUrl: "./providers-provider-info-show.component.html",
  styleUrls: ["./providers-provider-info-show.component.scss"]
})
export class ProvidersProviderInfoShowComponent implements OnInit {
  providerInfoId;
  showLocationId;
  showServiceAgeGroupId;
  deleteLocationId;
  deleteEditEmailId;
  deleteEditContactNumberId;
  deleteEditCountyValue;
  deleteEditServiceIdentifier;
  editLocationNameValue;
  editAddressId;
  editAddressForm: FormGroup;
  editEmailForm: FormGroup;
  editContactNumberForm: FormGroup;
  editServiceCountyForm: FormGroup;
  editServiceForm: FormGroup;
  cloneServiceForm: FormGroup;
  cloneServiceId;
  locationNameEditMode: boolean = false;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  faSave = faSave;
  faTimes = faTimes;
  faPlus = faPlus;
  faEye = faEye;
  faClone = faClone;
  errorMessage: string;
  serverMessage: string;
  editAddressType: string;
  stateOptions = [];
  provider = {
    providerInfoDto: {
      createApproved: true
    },
    providerLocationDtos: [],
    serviceAgeGroupOptionDtos: [],
  };

  unapprovedCounties = [];
  michiganCounties: string[] = [];
  serviceOptions = [];
  serviceCategoryOptions = [];
  serviceDeliveryOptions = [];
  insuranceOptions = [];
  ageGroupOptions = [];
  contactTypeOptions = [];
  serviceOptionsUnderCategories = [];
  deliverySelectSettings = {};
  insuranceSelectSettings = {};
  ageGroupSelectSettings = {};
  countySelectSettings = {};


  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  @ViewChild("confirmDeleteLocationAlert")
  confirmDeleteLocationAlert: SwalComponent;
  @ViewChild("confirmDeleteEditEmailAlert")
  confirmDeleteEditEmailAlert: SwalComponent;
  @ViewChild("confirmDeleteEditContactNumberAlert")
  confirmDeleteEditContactNumberAlert: SwalComponent;
  @ViewChild("confirmDeleteEditCountyAlert")
  confirmDeleteEditCountyAlert: SwalComponent;
  @ViewChild("confirmDeleteEditServiceAlert")
  confirmDeleteEditServiceAlert: SwalComponent;

  @ViewChild("editAddressModal") editAddressModal: SwalComponent;
  @ViewChild("editServiceCountyModal") editServiceCountyModal: SwalComponent;
  @ViewChild("unapprovedUpdatedServiceCountyModal")
  unapprovedUpdatedServiceCountyModal: SwalComponent;
  @ViewChild("cloneServiceModal") cloneServiceModal: SwalComponent;

  wait1Second = timer(1000);
  pn = this.usPhoneNumberService.formatPhoneNumber;
  serviceIdentifiertoEdit;

  constructor(
    private auth: AuthService,
    private providerService: ProviderService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private insuranceService: InsuranceService,
    private ageGroupService: AgeGroupService,
    private contactTypeService: ContactTypeService,
    private deliveryService: DeliveryService,
    private cartesianService: CartesianService,
    private miCountyService: MiCountyService,
    private locationService: LocationService,
    private usPhoneNumberService: UsPhoneNumberService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() => this.providerService.getProviderById(this.providerInfoId))
      )
      .subscribe(provider => {
        this.provider = provider;
      });

    this.editAddressForm = this.formBuilder.group({
      editStreet: ["", [Validators.required]],
      editCity: ["", [Validators.required]],
      editState: ["MI", [Validators.required]],
      editZip: ["", [Validators.required, ZipcodeValidator.zipcode]]
    });

    this.editEmailForm = this.formBuilder.group({
      editEmail: ["", [Validators.required, Validators.email]],
      editContactType: ["", [Validators.required]]
    });

    this.editContactNumberForm = this.formBuilder.group({
      editContactNumber: [
        "",
        [Validators.required, Validators.pattern("^\\+?\\d{0,15}")]
      ],
      editContactType: ["", [Validators.required]]
    });

    this.editServiceCountyForm = this.formBuilder.group({
      editCounties: ["", Validators.required]
    });

    //this form actually adds a new service
    this.editServiceForm = this.formBuilder.group({
      editService: ["", [Validators.required]],
      editServiceDisplay: ["", []],
      editServiceCategory: ["", []],
      editInternalOnly: [false, []],
      editServiceDeliveries: [[], [Validators.required]],
      editInsurances: [[], [Validators.required]],
      editAgeGroups: [[], [Validators.required]]
    });

    this.cloneServiceForm = this.formBuilder.group({
      cloneService: ["", [Validators.required]],
      cloneServiceDisplay: ["", []],
      cloneServiceCategory: ["", []],
      cloneInternalOnly: [false, []],
      cloneServiceDeliveries: [[], [Validators.required]],
      cloneInsurances: [[], [Validators.required]],
      cloneAgeGroups: [[], [Validators.required]]
    });

    forkJoin([
      this.providerServiceService.getAllServiceOptions(),
      this.insuranceService.getAllInsuranceOptions(),
      this.ageGroupService.getAllAgeGroupOptions(),
      this.contactTypeService.getAllContactTypeOptions(),
      this.serviceCategoryService.getAllServiceCategoryOptions(),
      this.providerServiceService.getAllServices(),
      this.deliveryService.getAllDeliveryOptions(),
      this.miCountyService.getAllMICountyOptions()
    ]).subscribe(results => {
      this.serviceOptions = results[0];
      this.insuranceOptions = results[1];
      this.ageGroupOptions = results[2];
      this.contactTypeOptions = results[3];
      this.serviceCategoryOptions = results[4];
      this.serviceOptionsUnderCategories = results[5];
      this.serviceDeliveryOptions = results[6];
      this.michiganCounties = results[7];

      this.deliverySelectSettings = {
        singleSelection: false,
        idField: "serviceDeliveryId",
        textField: "delivery",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.insuranceSelectSettings = {
        singleSelection: false,
        idField: "insuranceId",
        textField: "insuranceName",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.ageGroupSelectSettings = {
        singleSelection: false,
        idField: "serviceAgeGroupId",
        textField: "ageGroupDisplay",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.countySelectSettings = {
        singleSelection: false,
        idField: "countyId",
        textField: "county",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    });

    this.stateOptions = scus.getStates();
  }


  openLocationView(event) {
    this.showLocationId = event.currentTarget.dataset.locationid;

    this.locationNameEditMode = false;
    this.editLocationNameValue = null;
  }

  cancel(): void {
    Swal.close();
  }

  deleteLocation(event) {
    this.deleteLocationId = event.currentTarget.dataset.locationid;

    this.confirmDeleteLocationAlert.fire();
  }

  deleteEditEmail(event) {
    this.deleteEditEmailId = event.currentTarget.dataset.editemailid;

    this.confirmDeleteEditEmailAlert.fire();
  }

  deleteEditContactNumber(event) {
    this.deleteEditContactNumberId = event.currentTarget.dataset.editnumberid;

    this.confirmDeleteEditContactNumberAlert.fire();
  }

  deleteEditCounty(event) {
    this.deleteEditCountyValue = event.currentTarget.dataset.editcounty;

    this.confirmDeleteEditCountyAlert.fire();
  }

  deleteEditService(event) {
    this.deleteEditServiceIdentifier =
      event.currentTarget.dataset.editidentifier;

    this.confirmDeleteEditServiceAlert.fire();
  }

  confirmDeleteLocation(): void {
    if (this.hasAdminEditPerm) {
      this.locationService
        .deleteLocation(this.deleteLocationId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete location...";
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.locationService
        .navigatorDeleteLocation(this.deleteLocationId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete location...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  confirmDeleteEditEmail(): void {
    if (this.hasAdminEditPerm) {
      this.locationService
        .deleteEmail(this.deleteEditEmailId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete email...";
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.locationService
        .navigatorDeleteEmail(this.deleteEditEmailId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete email...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  confirmDeleteEditContactNumber(): void {
    if (this.hasAdminEditPerm) {
      this.locationService
        .deleteContactNumber(this.deleteEditContactNumberId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete contact number...";
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.locationService
        .navigatorDeleteContactNumber(this.deleteEditContactNumberId)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete contact number...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  confirmDeleteEditService(): void {
    if (this.hasAdminEditPerm) {
      this.locationService
        .deleteService(this.deleteEditServiceIdentifier)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete service...";
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.locationService
        .navigatorDeleteService(this.deleteEditServiceIdentifier)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to delete service...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editLocationName(event): void {
    this.locationNameEditMode = true;
    this.editLocationNameValue = event.currentTarget.dataset.locationname;
  }

  cancelEditLocationName(): void {
    this.locationNameEditMode = false;
  }

  saveEditLocationName(event): void {
    if (this.hasAdminEditPerm) {
      this.locationService
        .updateLocationName(
          event.currentTarget.dataset.locationid,
          this.editLocationNameValue
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.locationNameEditMode = false;
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update location name...";
            this.errorAlert.fire();
            throw error;
          }
        );
    } else {
      this.locationService
        .navigatorUpdateLocationName(
          event.currentTarget.dataset.locationid,
          this.editLocationNameValue
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.locationNameEditMode = false;
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update location name...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editLocationAddress(event): void {
    this.editAddressId = event.currentTarget.dataset.addressid;
    this.editAddressType = event.currentTarget.dataset.addresstype;
    this.editCity.setValue(event.currentTarget.dataset.city);
    this.editStreet.setValue(event.currentTarget.dataset.street);
    this.editState.setValue(event.currentTarget.dataset.state);
    this.editZip.setValue(event.currentTarget.dataset.zip);

    this.editAddressModal.fire();
  }

  editAddressSubmit(): void {
    if (this.editAddressForm.valid) {
      const {
        editCity,
        editStreet,
        editState,
        editZip
      } = this.editAddressForm.value;

      if (this.hasAdminEditPerm) {
        this.locationService
          .updateLocationAddress(
            this.editAddressId,
            editStreet,
            editCity,
            editState,
            editZip
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = `Error when trying to update ${this.editAddressType} address...`;
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorUpdateLocationAddress(
            this.editAddressId,
            editStreet,
            editCity,
            editState,
            editZip
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = `Error when trying to update ${this.editAddressType} address...`;
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }

  submitEditEmail(): void {
    if (this.editEmailForm.valid) {
      const { editEmail, editContactType } = this.editEmailForm.value;

      if (this.hasAdminEditPerm) {
        this.locationService
          .addEmail(this.showLocationId, editEmail, editContactType)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add email...";
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorAddEmail(this.showLocationId, editEmail, editContactType)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add email...";
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }

  submitEditContactNumber(): void {
    if (this.editContactNumberForm.valid) {
      const {
        editContactNumber,
        editContactType
      } = this.editContactNumberForm.value;

      if (this.hasAdminEditPerm) {
        this.locationService
          .addContactNumber(
            this.showLocationId,
            editContactNumber,
            editContactType
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add contact number...";
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorAddContactNumber(
            this.showLocationId,
            editContactNumber,
            editContactType
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add contact number...";
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }
  editServiceCountiesFire(): void {
    const editLocation = this.provider.providerLocationDtos.find(
      l => l.locationId === parseInt(this.showLocationId)
    );

    this.editCounties.setValue(editLocation.serviceCounties);

    this.editServiceCountyModal.fire();
  }

  submitEditServiceCounty(): void {
    if (this.editServiceCountyForm.valid) {
      const countyIds = this.editCounties.value.map(ec => ec.countyId);

      if (this.hasAdminEditPerm) {
        this.locationService
          .updateCounties(this.showLocationId, countyIds)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to update counties...";
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorUpdateCounties(this.showLocationId, countyIds)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to update counties...";
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }

  cloneServiceFire(event): void {

    const _services = this.provider.providerLocationDtos.find(
      l => l.locationId === parseInt(this.showLocationId)
    ).locationServiceDtos;

    const serviceToClone = _services.filter(s => s.identifier == event.currentTarget.dataset.editidentifier)[0];
    const cloneRaw = serviceToClone;

    let deliveriesForForm = [];
    const serviceDeliveryMap = new Map(this.serviceDeliveryOptions.map(i => [i.delivery, i]));
    const cloneServiceDeliveryToMap = cloneRaw.deliveries;
    deliveriesForForm = cloneServiceDeliveryToMap.map((_, i) => {
      return serviceDeliveryMap.get(cloneServiceDeliveryToMap[i])
    })

    let insurancesForForm = [];
    const insuranceMap = new Map(this.insuranceOptions.map(i => [i.insuranceName, i]));
    const cloneServiceInsuranceToMap = cloneRaw.insurances;
    insurancesForForm = cloneServiceInsuranceToMap.map((_, i) => {
      return insuranceMap.get(cloneServiceInsuranceToMap[i])
    })

    let ageGroupsForForm = [];
    const ageGroupMap = new Map(this.ageGroupOptions.map(i => [i.ageEnd, i]));
    const cloneServiceAgeToMap = cloneRaw.locationAgeGroupDtos

    ageGroupsForForm = cloneServiceAgeToMap.map(function (_, i) {
      return ageGroupMap.get(cloneServiceAgeToMap[i].ageEnd)
    })

    this.cloneService.setValue(serviceToClone.serviceName);
    this.cloneInternalOnly.setValue(serviceToClone.internal);
    this.cloneServiceDeliveries.setValue(deliveriesForForm);
    this.cloneInsurances.setValue(insurancesForForm);
    this.cloneAgeGroups.setValue(ageGroupsForForm);


    this.cloneServiceModal.fire();

  };

  submitCloneService(): void {
    if (this.cloneServiceForm.valid) {
      const cloneSubmit = this.cloneServiceForm.value;

      let serviceNameSubmit = cloneSubmit.cloneService;
      let serviceObject = this.serviceOptions.find(o => o.service === serviceNameSubmit);
      let serviceIdToSubmit = serviceObject.serviceId;

      const clone = {
        serviceId: serviceIdToSubmit,
        internal: cloneSubmit.cloneInternalOnly,
        serviceDeliveryIds: [],
        insuranceIds: [],
        ageGroupIds: [],
        identifier: uuidv4()
      };

      clone.serviceDeliveryIds = cloneSubmit.cloneServiceDeliveries.map(
        serviceDelivery => serviceDelivery.serviceDeliveryId
      );

      clone.insuranceIds = cloneSubmit.cloneInsurances.map(
        insurance => insurance.insuranceId
      );

      clone.ageGroupIds = cloneSubmit.cloneAgeGroups.map(
        ageGroup => ageGroup.serviceAgeGroupId
      );

      const processedClones = this.cartesianService
        .cartesian(
          [clone.internal],
          [...clone.serviceDeliveryIds],
          [...clone.insuranceIds],
          [...clone.ageGroupIds],
          [clone.identifier],
          [clone.serviceId]
        )
        .map(e => {
          return {
            internal: e[0],
            serviceDeliveryId: e[1],
            insuranceId: e[2],
            ageGroupId: e[3],
            identifier: e[4],
            serviceId: e[5]
          };
        });

      if (this.hasAdminEditPerm) {
        this.locationService
          .addServiceToLocation(this.showLocationId, processedClones)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to clone service...";
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorAddServiceToLocation(
            this.showLocationId,
            processedClones
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to clone service...";
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }

  //this form is tied to the add service button and adds a new service - rather than editing 
  submitEditService(): void {
    if (this.editServiceForm.valid) {
      const locationSrvcLinkRaw = this.editServiceForm.value;
      const locationSrvcLink = {
        serviceId: parseInt(locationSrvcLinkRaw.editService),
        internal: locationSrvcLinkRaw.editInternalOnly,
        serviceDeliveryIds: [],
        insuranceIds: [],
        ageGroupIds: [],
        identifier: uuidv4()
      };

      locationSrvcLink.serviceDeliveryIds = locationSrvcLinkRaw.editServiceDeliveries.map(
        serviceDelivery => serviceDelivery.serviceDeliveryId
      );
      locationSrvcLink.insuranceIds = locationSrvcLinkRaw.editInsurances.map(
        insurance => insurance.insuranceId
      );
      locationSrvcLink.ageGroupIds = locationSrvcLinkRaw.editAgeGroups.map(
        ageGroup => ageGroup.serviceAgeGroupId
      );

      const processedLocationSrvcLinks = this.cartesianService
        .cartesian(
          [locationSrvcLink.serviceId],
          [locationSrvcLink.internal],
          [...locationSrvcLink.serviceDeliveryIds],
          [...locationSrvcLink.insuranceIds],
          [...locationSrvcLink.ageGroupIds],
          [locationSrvcLink.identifier]
        )
        .map(e => {
          return {
            serviceId: e[0],
            internal: e[1],
            serviceDeliveryId: e[2],
            insuranceId: e[3],
            ageGroupId: e[4],
            identifier: e[5]
          };
        });

      if (this.hasAdminEditPerm) {
        this.locationService
          .addServiceToLocation(this.showLocationId, processedLocationSrvcLinks)
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add service...";
              this.errorAlert.fire();
              throw error;
            }
          );
      } else {
        this.locationService
          .navigatorAddServiceToLocation(
            this.showLocationId,
            processedLocationSrvcLinks
          )
          .pipe(
            map(response => {
              this.serverMessage = response.message;
              this.infoAlert.fire();
            }),
            flatMap(() => this.wait1Second)
          )
          .subscribe(
            () => {
              Swal.close();
              this.ngOnInit();
            },
            error => {
              this.errorMessage = "Error when trying to add service...";
              this.errorAlert.fire();
              throw error;
            }
          );
      }
    }
  }

  addLocation(): void {
    this.router.navigate(["../addlocation"], { relativeTo: this.route });
  }

  unapprovedCountiesViewFire(): void {
    const currLocation = this.provider.providerLocationDtos.find(
      l => l.locationId === parseInt(this.showLocationId)
    );

    this.unapprovedCounties = currLocation.modifiedCounties;

    this.unapprovedUpdatedServiceCountyModal.fire();
  }

  publishLocation(locationId): void {
    this.locationService
      .updateLocationInternalStatus(locationId, false)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to publish location...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  unpublishLocation(locationId): void {
    this.locationService
      .updateLocationInternalStatus(locationId, true)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to make location internal...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  publishService(identifier): void {
    this.locationService
      .updateGroupedServicesInternalStatus(identifier, false)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to publish service...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  unpublishService(identifier): void {
    this.locationService
      .updateGroupedServicesInternalStatus(identifier, true)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to make service internal...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get editStreet() {
    return this.editAddressForm.get("editStreet");
  }

  get editCity() {
    return this.editAddressForm.get("editCity");
  }

  get editState() {
    return this.editAddressForm.get("editState");
  }

  get editZip() {
    return this.editAddressForm.get("editZip");
  }

  get editEmail() {
    return this.editEmailForm.get("editEmail");
  }

  get editContactNumber() {
    return this.editContactNumberForm.get("editContactNumber");
  }

  get editCounties() {
    return this.editServiceCountyForm.get("editCounties");
  }

  get hasAdminEditPerm() {
    return this.auth.hasPermissions(["PERM_CREATE_OR_MODIFY_PROVIDER_INFO"]);
  }

  get cloneService() {
    return this.cloneServiceForm.get("cloneService");
  }

  get cloneAgeGroups() {
    return this.cloneServiceForm.get("cloneAgeGroups");
  }

  get cloneServiceCategory() {
    return this.cloneServiceForm.get("cloneServiceCategory");
  }

  get cloneInternalOnly() {
    return this.cloneServiceForm.get("cloneInternalOnly");
  }

  get cloneServiceDeliveries() {
    return this.cloneServiceForm.get("cloneServiceDeliveries");
  }

  get cloneInsurances() {
    return this.cloneServiceForm.get("cloneInsurances");
  }

}
