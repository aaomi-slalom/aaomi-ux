import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidersProviderInfoShowComponent } from './providers-provider-info-show.component';

describe('ProvidersProviderInfoShowComponent', () => {
  let component: ProvidersProviderInfoShowComponent;
  let fixture: ComponentFixture<ProvidersProviderInfoShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidersProviderInfoShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidersProviderInfoShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
