import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";
import { DeliveryService } from "src/app/services/delivery/delivery.service";
import { MiCountyService } from "src/app/services/mi-county/mi-county.service";
import { Router, ActivatedRoute } from "@angular/router";
import { forkJoin } from "rxjs";
import urlBase64 from "btrz-base64-encoder";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { map, flatMap, finalize } from "rxjs/operators";
import { QueryService } from "src/app/services/query/query.service";
import { RegionService } from "src/app/services/region/region.service";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";
import * as printJS from "print-js";
import * as moment from "moment";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-public-query",
  templateUrl: "./public-query.component.html",
  styleUrls: ["./public-query.component.scss"]
})
export class PublicQueryComponent implements OnInit {
  searchProviderForm: FormGroup;
  totalPages: number;
  totalElements: number;
  pageSize: number = 5;
  providerInfos = [];
  providerReportRecords = [];
  serviceOptions = [];
  serviceCategoryOptions = [];
  serviceDeliveryOptions = [];
  insuranceOptions = [];
  ageGroupOptions = [];
  countyOptions = [];
  regionOptions = [];
  categorySelectSettings = {};
  serviceSelectSettings = {};
  deliverySelectSettings = {};
  insuranceSelectSettings = {};
  ageGroupSelectSettings = {};
  countySelectSettings = {};
  regionSelectSettings = {};
  faEye = faEye;
  pn = this.usPhoneNumberService.formatPhoneNumber;
  moment = moment;
  loading: boolean;

  contactReportRequested: boolean = false;
  contactReportFetched: boolean = false;
  contactReportName: string;
  contactReportData = {
    reportFile: ""
  };

  standaloneReportRequested: boolean = false;
  standaloneReportFetched: boolean = false;
  standaloneReportName: string;
  standaloneReportData = {
    reportFile: ""
  };

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private insuranceService: InsuranceService,
    private ageGroupService: AgeGroupService,
    private deliveryService: DeliveryService,
    private miCountyService: MiCountyService,
    private communicationService: CommunicationService,
    private regionService: RegionService,
    private queryService: QueryService,
    private usPhoneNumberService: UsPhoneNumberService,
    private providerService: ProviderService,
    private router: Router,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.searchProviderForm = this.formBuilder.group({
      providerName: ["", []],
      keyword: ["", []],
      serviceCategories: [[], []],
      services: [[], []],
      serviceDeliveries: [[], []],
      insurances: [[], []],
      ageGroups: [[], []],
      counties: [[], []],
      regions: [[], []]
    });

    const services = this.providerServiceService.getAllPublicServiceOptions();
    const serviceCategories = this.serviceCategoryService.getAllServiceCategoryOptions();
    const serviceDeliveries = this.deliveryService.getAllDeliveryOptions();
    const insurances = this.insuranceService.getAllInsuranceOptions();
    const ageGroups = this.ageGroupService.getAllAgeGroupOptions();
    const miCounties = this.miCountyService.getAllMICountyOptions();
    const regions = this.regionService.getAllRegionOptions();

    this.categorySelectSettings = {
      singleSelection: false,
      idField: "serviceCategoryId",
      textField: "serviceCategory",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.serviceSelectSettings = {
      singleSelection: false,
      idField: "serviceId",
      textField: "service",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.deliverySelectSettings = {
      singleSelection: false,
      idField: "serviceDeliveryId",
      textField: "delivery",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.insuranceSelectSettings = {
      singleSelection: false,
      idField: "insuranceId",
      textField: "insuranceName",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.ageGroupSelectSettings = {
      singleSelection: false,
      idField: "serviceAgeGroupId",
      textField: "ageGroupDisplay",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.countySelectSettings = {
      singleSelection: false,
      idField: "countyId",
      textField: "county",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.regionSelectSettings = {
      singleSelection: false,
      idField: "regionId",
      textField: "region",
      selectAllText: "Select All",
      unSelectAllText: "Unselect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    forkJoin([
      services,
      serviceCategories,
      serviceDeliveries,
      insurances,
      ageGroups,
      miCounties,
      regions
    ])
      .pipe(
        map(results => {
          this.serviceOptions = results[0];
          this.serviceCategoryOptions = results[1];
          this.serviceDeliveryOptions = results[2];
          this.insuranceOptions = results[3];
          this.ageGroupOptions = results[4];
          this.countyOptions = results[5];
          this.regionOptions = results[6];

          if (this.communicationService.hasData("landingSearchForm")) {
            const landingSearchFormData = this.communicationService.getData()[
              "landingSearchForm"
            ].value;
            this.searchProviderForm.patchValue(landingSearchFormData);

            const searchDataURLStr = urlBase64.encode(this.searchData);
          }
        }),
        flatMap(() => this.route.url)
      )
      .subscribe(url => {
        const urlRedirectSearchData = url[1].parameters.data;
        if (urlRedirectSearchData) {
          const redirectSearchData = urlBase64.decode(urlRedirectSearchData);

          this.providerNameControl.setValue(redirectSearchData.providerName);
          this.keywordControl.setValue(redirectSearchData.keyword);

          this.ageGroupsControl.setValue(
            redirectSearchData.ageGroupIds.map(ageGroupId => {
              return this.ageGroupOptions.find(
                ageGroupOption =>
                  ageGroupOption.serviceAgeGroupId === ageGroupId
              );
            })
          );

          this.countiesControl.setValue(
            redirectSearchData.countyIds.map(countyId => {
              return this.countyOptions.find(
                countyOption => countyOption.countyId === countyId
              );
            })
          );

          this.regionsControl.setValue(
            redirectSearchData.regionIds.map(regionId => {
              return this.regionOptions.find(
                regionOption => regionOption.regionId === regionId
              );
            })
          );

          this.servicesControl.setValue(
            redirectSearchData.serviceIds.map(serviceId => {
              return this.serviceOptions.find(
                serviceOption => serviceOption.serviceId === serviceId
              );
            })
          );

          this.insurancesControl.setValue(
            redirectSearchData.insuranceIds.map(insuranceId => {
              return this.insuranceOptions.find(
                insuranceOption => insuranceOption.insuranceId === insuranceId
              );
            })
          );

          this.serviceCategoriesControl.setValue(
            redirectSearchData.categoryIds.map(categoryId => {
              return this.serviceCategoryOptions.find(
                serviceCategoryOption =>
                  serviceCategoryOption.serviceCategoryId === categoryId
              );
            })
          );

          this.serviceDeliveriesControl.setValue(
            redirectSearchData.deliveryIds.map(deliveryId => {
              return this.serviceDeliveryOptions.find(
                serviceDeliveryOption =>
                  serviceDeliveryOption.serviceDeliveryId === deliveryId
              );
            })
          );
        }

        if (this.hasInternalAccess) {
          this.internalQuery(0);
        } else {
          this.publicQuery(0);
        }
      });
  }

  submitSearchProvider(): void {
    const searchDataURLStr = urlBase64.encode(this.searchData);
    this.router.navigate(["/providers/search", { data: searchDataURLStr }])
  }

  internalQuery(currentPage): void {
  this.loading=true
    this.queryService
      .internalQueryProviderInfosByConditions({
        currentPage: currentPage,
        pageSize: this.pageSize,
        ...this.searchData
       })

      .subscribe(response => {
        this.loading=false
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.providerInfos = response.publicProviderInfoDtos;
      });
  }

  printReport(): void {
    printJS({
      printable: "customized-resource-list",
      type: "html",
      documentTitle:
        "Autism Alliance of Michigan - MiNavigator Program - Customized Resource List"
    });
  }

  addToReport(providerInfo): void {
    this.providerReportRecords.push(providerInfo);
  }

  removeFromReport(suitableLocationId): void {
    const copy = this.providerReportRecords.slice();
    this.providerReportRecords = copy.filter(
      record => record.suitableLocationId !== suitableLocationId
    );
  }

  reported(suitableLocationId): boolean {
    return (
      this.providerReportRecords.findIndex(
        record => record.suitableLocationId === suitableLocationId
      ) !== -1
    );
  }

  publicQuery(cp): void {
    this.loading = true;
    this.queryService
      .queryProviderInfosByConditions({
        currentPage: cp,
        pageSize: this.pageSize,
        ...this.searchData,
        guestUserEmail: localStorage.getItem('guestUserEmail')
      })
      .subscribe(response => {
        this.loading = false;
        this.totalPages = response.totalPages;
        this.totalElements = response.totalElements;
        this.providerInfos = response.publicProviderInfoDtos;
      },
        err => {
          this.loading = false;
        });
  }

  pageChanged(event: PageChangedEvent): void {
    if (this.hasInternalAccess) {
      this.internalQuery(event.page - 1);
    } else {
      this.publicQuery(event.page - 1);
    }
  }

  viewSpecificLocation(event): void {
    const locationId = event.currentTarget.dataset.locationid;
    this.router.navigate([`query/provider/location/${locationId}`]);
  }

  sanitize(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  generateContactReport(): void {
    this.contactReportFetched = false;
    this.contactReportRequested = true;

    this.providerService
      .getContactReport({
        currentPage: 0,
        pageSize: this.pageSize,
        ...this.searchData
      })
      .subscribe(contactReportData => {
        this.contactReportData = contactReportData;
        this.contactReportFetched = true;
        this.contactReportRequested = false;
        this.contactReportName =
          "AAOMI_Provider_Contacts_" +
          moment().format("MM-DD-YYYYTHH-mm-ss") +
          ".xlsx";
      });
  }

  generateStandaloneReport(): void {
    this.standaloneReportFetched = false;
    this.standaloneReportRequested = true;

    this.providerService
      .getStandaloneReport({
        currentPage: 0,
        pageSize: this.pageSize,
        ...this.searchData
      })
      .subscribe(standaloneReportData => {
        this.standaloneReportData = standaloneReportData;
        this.standaloneReportFetched = true;
        this.standaloneReportRequested = false;
        this.standaloneReportName =
          "AAOMI_Standalone_Providers_" +
          moment().format("MM-DD-YYYYTHH-mm-ss") +
          ".xlsx";
      });
  }

  get searchData() {
    return {
      providerName: this.providerName,
      keyword: this.keyword,
      ageGroupIds: this.ageGroupIds,
      categoryIds: this.categoryIds,
      serviceIds: this.serviceIds,
      insuranceIds: this.insuranceIds,
      countyIds: this.countyIds,
      regionIds: this.regionIds,
      deliveryIds: this.deliveryIds
    };
  }

  get providerName() {
    return this.searchProviderForm.get("providerName").value;
  }

  get providerNameControl() {
    return this.searchProviderForm.get("providerName");
  }

  get keyword() {
    return this.searchProviderForm.get("keyword").value;
  }

  get keywordControl() {
    return this.searchProviderForm.get("keyword");
  }

  get ageGroupIds() {
    return this.searchProviderForm
      .get("ageGroups")
      .value.map(ageGroup => ageGroup.serviceAgeGroupId);
  }

  get ageGroupsControl() {
    return this.searchProviderForm.get("ageGroups");
  }

  get categoryIds() {
    return this.searchProviderForm
      .get("serviceCategories")
      .value.map(serviceCategory => serviceCategory.serviceCategoryId);
  }

  get serviceCategoriesControl() {
    return this.searchProviderForm.get("serviceCategories");
  }

  get serviceIds() {
    return this.searchProviderForm
      .get("services")
      .value.map(service => service.serviceId);
  }

  get servicesControl() {
    return this.searchProviderForm.get("services");
  }

  get insuranceIds() {
    return this.searchProviderForm
      .get("insurances")
      .value.map(insurance => insurance.insuranceId);
  }

  get insurancesControl() {
    return this.searchProviderForm.get("insurances");
  }

  get countyIds() {
    return this.searchProviderForm
      .get("counties")
      .value.map(county => county.countyId);
  }

  get countiesControl() {
    return this.searchProviderForm.get("counties");
  }

  get deliveryIds() {
    return this.searchProviderForm
      .get("serviceDeliveries")
      .value.map(serviceDelivery => serviceDelivery.serviceDeliveryId);
  }

  get serviceDeliveriesControl() {
    return this.searchProviderForm.get("serviceDeliveries");
  }

  get regionIds() {
    return this.searchProviderForm
      .get("regions")
      .value.map(region => region.regionId);
  }

  get regionsControl() {
    return this.searchProviderForm.get("regions");
  }

  get hasInternalAccess(): boolean {
    if (!this.auth.isAuthenticated()) return false;

    return this.auth.hasPermissions(["PERM_VIEW_PROVIDER_INFO"]);
  }

  get disclaimer() {
    return this.communicationService.getData()["appDisclaimer"];
  }

  get searchSupportNote() {
    return this.communicationService.getData()["appSearchSupportNote"];
  }
}
