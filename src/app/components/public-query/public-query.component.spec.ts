import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicQueryComponent } from './public-query.component';

describe('PublicQueryComponent', () => {
  let component: PublicQueryComponent;
  let fixture: ComponentFixture<PublicQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
