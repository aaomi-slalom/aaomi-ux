import { FormControl } from "@angular/forms";
import isUrl from "is-url";

export interface ValidationResult {
  [key: string]: boolean;
}

export class UrlValidator {
  public static isValidUrl(control: FormControl): ValidationResult {
    let valid = isUrl(control.value) || !control.value;

    if (!valid) {
      return { isValidUrl: true };
    }

    return null;
  }
}
