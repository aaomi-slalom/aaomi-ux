import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { UrlValidator } from "./validators/url.validator";
import { ProfileCreateService } from "src/app/services/profile/profile-create/profile-create.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { Router } from "@angular/router";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { map, flatMap } from "rxjs/operators";
import { timer } from "rxjs";
import { environment as ENV } from "../../../environments/environment";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";

@Component({
  selector: "app-profile-create",
  templateUrl: "./profile-create.component.html",
  styleUrls: ["./profile-create.component.scss"]
})
export class ProfileCreateComponent implements OnInit {
  createProfileForm: FormGroup;
  imageBuffer: string | ArrayBuffer;
  serverMessage: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  waitHalfSecond = timer(500);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private formTrim: FormTrimService,
    private profileCreateService: ProfileCreateService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder,

    private router: Router
  ) {}

  ngOnInit(): void {
    this.imageBuffer = ENV.EMPTY_PROFILE_IMG_URL;

    this.createProfileForm = this.formBuilder.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      contactNumber: [
        "",
        [Validators.required, Validators.pattern("^\\+?\\d{0,15}")]
      ],
      website: ["", [UrlValidator.isValidUrl]],
      image: ["", []],
      bio: ""
    });

    if (this.communicationService.getData()["hasProfile"]) {
      this.router.navigate(["/profile/show"]);
    }
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  onSubmit(createProfileForm: FormGroup): void {
    const createProfileFormValue = this.formTrim.trimForm(
      createProfileForm.value
    );

    let {
      firstName,
      lastName,
      contactNumber,
      website,
      image,
      bio
    } = createProfileFormValue;

    image = this.imageBuffer;

    this.profileCreateService
      .createUserProfile(
        firstName,
        lastName,
        contactNumber,
        website,
        image,
        bio
      )
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.waitHalfSecond)
      )
      .subscribe(
        () => {
          this.communicationService.emitChange();
          this.router.navigate(["/profile/show"]);
        },
        error => {
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get firstName() {
    return this.createProfileForm.get("firstName");
  }

  get lastName() {
    return this.createProfileForm.get("lastName");
  }

  get contactNumber() {
    return this.createProfileForm.get("contactNumber");
  }

  get bio() {
    return this.createProfileForm.get("bio");
  }

  get website() {
    return this.createProfileForm.get("website");
  }

  get image() {
    return this.createProfileForm.get("image");
  }
}
