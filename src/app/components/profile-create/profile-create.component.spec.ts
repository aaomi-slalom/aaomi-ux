import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ProfileCreateComponent } from "./profile-create.component";
import { FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("ProfileCreateComponent", () => {
  let component: ProfileCreateComponent;
  let fixture: ComponentFixture<ProfileCreateComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
        FormBuilder
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [ProfileCreateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
