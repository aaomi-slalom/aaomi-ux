import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth/auth.service';

import { QueryLocationShowComponent } from './query-location-show.component';

describe('QueryLocationShowComponent', () => {
  let component: QueryLocationShowComponent;
  let fixture: ComponentFixture<QueryLocationShowComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QueryLocationShowComponent],
      providers: [AuthService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryLocationShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
