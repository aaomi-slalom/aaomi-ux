import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import { QueryService } from "src/app/services/query/query.service";
import { environment as ENV } from "../../../environments/environment";
import Swal from "sweetalert2";
import { Location } from "@angular/common";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-query-location-show",
  templateUrl: "./query-location-show.component.html",
  styleUrls: ["./query-location-show.component.scss"]
})
export class QueryLocationShowComponent implements OnInit {
  locationId;
  location = {
    providerInfoId: null,
    providerName: "",
    locationName: "",
    logo: ENV.EMPTY_LOGO_IMG_URL,
    website: "",
    facebook: "",
    twitter: "",
    linkedin: "",
    instagram: "",
    youtube: "",
    description: "",
    address: "",
    internalMailingAddress: "",
    emails: [],
    internalAdminEmails: [],
    internalMktEmails: [],
    additionalEmails: [],
    contactNumbers: [],
    faxNumbers: [],
    internalAdminContactNumbers: [],
    internalMktContactNumbers: [],
    locationServiceDtos: [],
    otherLocations: [],
    files: []
  };
  pn = this.usPhoneNumberService.formatPhoneNumber;

  constructor(
    private auth: AuthService,
    private queryService: QueryService,
    private usPhoneNumberService: UsPhoneNumberService,
    private communicationService: CommunicationService,
    private route: ActivatedRoute,
    private router: Router,
    private currLocation: Location
  ) {}

  ngOnInit(): void {
    this.route.url
      .pipe(
        map(url => (this.locationId = url[3].path)),
        flatMap(() => {
          if (this.hasInternalPermission) {
            return this.queryService.getInternalLocationByLocationId(
              this.locationId
            );
          }

          return this.queryService.getPublicLocationByLocationId(
            this.locationId
          );
        })
      )
      .subscribe(location => {
        const img = location.logo ? location.logo : ENV.EMPTY_LOGO_IMG_URL;
        this.location = location;
        this.location.logo = img;
      });
  }

  viewOtherLocation(event): void {
    const locationId = event.currentTarget.dataset.locationid;
    this.router.navigate([`/query/provider/location/${locationId}`]);
    Swal.close();
  }

  goBack(): void {
    this.currLocation.back();
  }

  viewProviderNotes(event): void {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`admin/provider/${providerInfoId}/notes`]);
  }

  viewProviderWaitTimes(event): void {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`admin/provider/${providerInfoId}/waittimes`]);
  }

  viewProviderAccounts(event): void {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`admin/provider/${providerInfoId}/accounts`]);
  }

  viewProvider(event): void {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`admin/provider/${providerInfoId}`]);
  }

  viewProviderFiles(event): void {
    const providerInfoId = event.currentTarget.dataset.providerinfoid;

    this.router.navigate([`public/provider/${providerInfoId}/files`]);
  }

  get hasInternalPermission(): boolean {
    if (!this.auth.isAuthenticated()) return false;

    return this.auth.hasPermissions(["PERM_VIEW_PROVIDER_INFO"]);
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
