import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ProfileEditComponent } from "./profile-edit.component";
import { AuthService } from 'src/app/services/auth/auth/auth.service';
import { Router, Params, UrlSegment, ActivatedRoute } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

describe("ProfileEditComponent", () => {
  let component: ProfileEditComponent;
  let fixture: ComponentFixture<ProfileEditComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const fakeActivatedRoute = {
    snapshot: {
      params: {
        result: 'some result'
      }
    },
    params: of<Params>(),
    url: of<UrlSegment[]>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
        FormBuilder,
        { provide: ActivatedRoute, useValue: fakeActivatedRoute }
      ],
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [ProfileEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
