import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { UrlValidator } from "../profile-create/validators/url.validator";
import { ProfileShowService } from "src/app/services/profile/profile-show/profile-show.service";
import { ProfileEditService } from "src/app/services/profile/profile-edit/profile-edit.service";
import { timer } from "rxjs";
import { map, flatMap } from "rxjs/operators";
import { environment as ENV } from "../../../environments/environment";

@Component({
  selector: "app-profile-edit",
  templateUrl: "./profile-edit.component.html",
  styleUrls: ["./profile-edit.component.scss"]
})
export class ProfileEditComponent implements OnInit {
  imageBuffer: string | ArrayBuffer;
  serverMessage: string;
  @ViewChild("errorAlert") private errorAlert: SwalComponent;
  @ViewChild("infoAlert") private infoAlert: SwalComponent;
  waitHalfSecond = timer(500);

  editProfileForm: FormGroup = this.formBuilder.group({
    firstName: ["", [Validators.required]],
    lastName: ["", [Validators.required]],
    contactNumber: [
      "",
      [Validators.required, Validators.pattern("^\\+?\\d{0,15}")]
    ],
    website: ["", [UrlValidator.isValidUrl]],
    image: ["", []],
    bio: ["", []]
  });

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private formTrim: FormTrimService,
    private communicationService: CommunicationService,
    private profileShowService: ProfileShowService,
    private profileEditService: ProfileEditService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.profileShowService.getProfile().subscribe(response => {
      if (response) {
        const userProfile = response;
        this.imageBuffer = userProfile.image
          ? userProfile.image
          : ENV.EMPTY_PROFILE_IMG_URL;

        this.editProfileForm.setValue({
          firstName: userProfile.firstName,
          lastName: userProfile.lastName,
          contactNumber: userProfile.contactNumber,
          website: userProfile.website ? userProfile.website : "",
          image: "",
          bio: userProfile.bio ? userProfile.bio : ""
        });
      }
    });

    this.route.url.subscribe(urlVal => {
      this.communicationService.setData("onProfileChild", urlVal[1].path);
      this.communicationService.emitChange();
    });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  cancel(): void {
    this.router.navigate(["/profile/show"]);
  }

  onSubmit(editProfileForm: FormGroup): void {
    const editProfileFormValue = this.formTrim.trimForm(editProfileForm.value);

    let {
      firstName,
      lastName,
      contactNumber,
      website,
      image,
      bio
    } = editProfileFormValue;

    image = this.imageBuffer;

    console.log(website);

    this.profileEditService
      .editUserProfile(firstName, lastName, contactNumber, website, image, bio)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.waitHalfSecond)
      )
      .subscribe(
        () => {
          this.communicationService.emitChange();
          this.router.navigate(["/profile/show"]);
        },
        error => {
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  get firstName() {
    return this.editProfileForm.get("firstName");
  }

  get lastName() {
    return this.editProfileForm.get("lastName");
  }

  get contactNumber() {
    return this.editProfileForm.get("contactNumber");
  }

  get bio() {
    return this.editProfileForm.get("bio");
  }

  get website() {
    return this.editProfileForm.get("website");
  }

  get image() {
    return this.editProfileForm.get("image");
  }
}
