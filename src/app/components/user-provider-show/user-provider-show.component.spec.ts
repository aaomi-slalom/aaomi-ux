import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { UserProviderShowComponent } from "./user-provider-show.component";

describe("UserProviderShowComponent", () => {
  let component: UserProviderShowComponent;
  let fixture: ComponentFixture<UserProviderShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserProviderShowComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProviderShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
