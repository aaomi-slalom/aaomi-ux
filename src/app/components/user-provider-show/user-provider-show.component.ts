import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderService } from "src/app/services/provider/provider/provider.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, flatMap } from "rxjs/operators";
import {
  faPlus,
  faEye,
  faTrashAlt,
  faEdit,
  faSave,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { LocationService } from "src/app/services/location/location.service";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { timer, forkJoin } from "rxjs";
import Swal from "sweetalert2";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ZipcodeValidator } from "../provider-info-create/validators/zipcode.validator";
import scus from "state-counties-us";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";
import { ContactTypeService } from "src/app/services/contact-type/contact-type.service";
import { DeliveryService } from "src/app/services/delivery/delivery.service";
import { CartesianService } from "src/app/services/cartesian/cartesian.service";
import { MiCountyService } from "src/app/services/mi-county/mi-county.service";
import { environment as ENV } from "../../../environments/environment";
import { v4 as uuidv4 } from "uuid";
import { UrlValidator } from "../profile-create/validators/url.validator";
import { UsPhoneNumberService } from "src/app/services/us-phone-number/us-phone-number.service";
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "app-user-provider-show",
  templateUrl: "./user-provider-show.component.html",
  styleUrls: ["./user-provider-show.component.scss"]
})
export class UserProviderShowComponent implements OnInit {
  editDemographicsForm: FormGroup;
  unapprovedDemographicsForm: FormGroup;
  Swal = Swal;
  profileImg = ENV.EMPTY_PROFILE_IMG_URL;
  imageBuffer: string | ArrayBuffer = ENV.EMPTY_LOGO_IMG_URL;
  modifiedImageBuffer: string | ArrayBuffer = ENV.EMPTY_LOGO_IMG_URL;
  isViewMode: boolean = true;
  hasProviderName: boolean = false;

  providerInfoId;
  showLocationId;
  deleteLocationId;
  deleteEditEmailId;
  deleteEditContactNumberId;
  deleteEditCountyValue;
  deleteEditServiceIdentifier;
  editLocationNameValue;
  editAddressId;
  editAddressForm: FormGroup;
  editEmailForm: FormGroup;
  editContactNumberForm: FormGroup;
  editServiceCountyForm: FormGroup;
  editServiceForm: FormGroup;
  locationNameEditMode: boolean = false;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  faSave = faSave;
  faTimes = faTimes;
  faPlus = faPlus;
  faEye = faEye;
  errorMessage: string;
  serverMessage: string;
  editAddressType: string;
  stateOptions = [];
  provider = {
    providerLocationDtos: [],
    providerInfoDto: {
      providerName: "",
      logo: "",
      website: "",
      description: "",
      facebook: "",
      linkedin: "",
      twitter: "",
      instagram: "",
      youtube: "",
      modifiedProviderName: "",
      modifiedLogo: "",
      modifiedWebsite: "",
      modifiedDescription: "",
      modifiedFacebook: "",
      modifiedLinkedin: "",
      modifiedTwitter: "",
      modifiedInstagram: "",
      modifiedYoutube: "",
      modifyRequested: false,
      createApproved: true
    }
  };

  unapprovedCounties = [];
  michiganCounties: string[] = [];
  serviceOptions = [];
  serviceCategoryOptions = [];
  serviceDeliveryOptions = [];
  insuranceOptions = [];
  ageGroupOptions = [];
  contactTypeOptions = [];
  serviceOptionsUnderCategories = [];
  deliverySelectSettings = {};
  insuranceSelectSettings = {};
  ageGroupSelectSettings = {};
  countySelectSettings = {};

  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  @ViewChild("confirmDeleteLocationAlert")
  confirmDeleteLocationAlert: SwalComponent;
  @ViewChild("confirmDeleteEditEmailAlert")
  confirmDeleteEditEmailAlert: SwalComponent;
  @ViewChild("confirmDeleteEditContactNumberAlert")
  confirmDeleteEditContactNumberAlert: SwalComponent;
  @ViewChild("confirmDeleteEditCountyAlert")
  confirmDeleteEditCountyAlert: SwalComponent;
  @ViewChild("confirmDeleteEditServiceAlert")
  confirmDeleteEditServiceAlert: SwalComponent;

  @ViewChild("editAddressModal") editAddressModal: SwalComponent;
  @ViewChild("editServiceCountyModal") editServiceCountyModal: SwalComponent;
  @ViewChild("unapprovedUpdatedServiceCountyModal")
  unapprovedUpdatedServiceCountyModal: SwalComponent;
  @ViewChild("viewOrEditDemographicsModal") viewOrEditDemographicsModal: SwalComponent;
  @ViewChild("unapprovedDemographicsModal") unapprovedDemographicsModal: SwalComponent;

  wait1Second = timer(1000);
  pn = this.usPhoneNumberService.formatPhoneNumber;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "150",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    sanitize: false,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      ["fontName"],
      [
        "insertImage",
        "insertVideo",
        "insertHorizontalRule",
        "backgroundColor",
        "customClasses",
        "fontSize",
        "toggleEditorMode"
      ]
    ]
  };

  constructor(
    private auth: AuthService,
    private providerService: ProviderService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private insuranceService: InsuranceService,
    private ageGroupService: AgeGroupService,
    private contactTypeService: ContactTypeService,
    private deliveryService: DeliveryService,
    private cartesianService: CartesianService,
    private miCountyService: MiCountyService,
    private locationService: LocationService,
    private usPhoneNumberService: UsPhoneNumberService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  checkUpdateProviderName(): void {
    this.providerService
      .checkUpdateProviderName(this.providerName.value, this.providerInfoId)
      .subscribe(response => {
        this.hasProviderName = response.hasProviderName;
      });
  }

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() =>
          this.providerService.getUserProviderById(this.providerInfoId)
        )
      )
      .subscribe(provider => {
        this.provider = provider;
      });

    this.editDemographicsForm = this.formBuilder.group({
      providerName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      logo: ["", []],
      description: ["", []],
      facebook: ["", [UrlValidator.isValidUrl]],
      linkedin: ["", [UrlValidator.isValidUrl]],
      twitter: ["", [UrlValidator.isValidUrl]],
      instagram: ["", [UrlValidator.isValidUrl]],
      youtube: ["", [UrlValidator.isValidUrl]]
    });

    this.unapprovedDemographicsForm = this.formBuilder.group({
      providerName: ["", [Validators.required]],
      website: ["", [UrlValidator.isValidUrl]],
      logo: ["", []],
      description: ["", []],
      facebook: ["", [UrlValidator.isValidUrl]],
      linkedin: ["", [UrlValidator.isValidUrl]],
      twitter: ["", [UrlValidator.isValidUrl]],
      instagram: ["", [UrlValidator.isValidUrl]],
      youtube: ["", [UrlValidator.isValidUrl]]
    });

    this.editAddressForm = this.formBuilder.group({
      editStreet: ["", [Validators.required]],
      editCity: ["", [Validators.required]],
      editState: ["MI", [Validators.required]],
      editZip: ["", [Validators.required, ZipcodeValidator.zipcode]]
    });

    this.editEmailForm = this.formBuilder.group({
      editEmail: ["", [Validators.required, Validators.email]],
      editContactType: ["", [Validators.required]]
    });

    this.editContactNumberForm = this.formBuilder.group({
      editContactNumber: [
        "",
        [Validators.required, Validators.pattern("^\\+?\\d{0,15}")]
      ],
      editContactType: ["", [Validators.required]]
    });

    this.editServiceCountyForm = this.formBuilder.group({
      editCounties: ["", Validators.required]
    });

    this.editServiceForm = this.formBuilder.group({
      editService: ["", [Validators.required]],
      editServiceDisplay: ["", []],
      editServiceCategory: ["", []],
      editInternalOnly: [false, []],
      editServiceDeliveries: [[], [Validators.required]],
      editInsurances: [[], [Validators.required]],
      editAgeGroups: [[], [Validators.required]]
    });

    forkJoin([
      this.providerServiceService.getAllPublicServiceOptions(),
      this.insuranceService.getAllInsuranceOptions(),
      this.ageGroupService.getAllAgeGroupOptions(),
      this.contactTypeService.getAllContactTypeOptions(),
      this.serviceCategoryService.getAllServiceCategoryOptions(),
      this.providerServiceService.getAllNonInternalServices(),
      this.deliveryService.getAllDeliveryOptions(),
      this.miCountyService.getAllMICountyOptions()
    ]).subscribe(results => {
      this.serviceOptions = results[0];
      this.insuranceOptions = results[1];
      this.ageGroupOptions = results[2];
      this.contactTypeOptions = results[3];
      this.serviceCategoryOptions = results[4];
      this.serviceOptionsUnderCategories = results[5];
      this.serviceDeliveryOptions = results[6];
      this.michiganCounties = results[7];

      if (this.provider.providerInfoDto.logo) {
        this.imageBuffer = this.provider.providerInfoDto.logo;
      }

      if (this.provider.providerInfoDto.modifiedLogo) {
        this.modifiedImageBuffer = this.provider.providerInfoDto.modifiedLogo;
      }

      this.deliverySelectSettings = {
        singleSelection: false,
        idField: "serviceDeliveryId",
        textField: "delivery",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.insuranceSelectSettings = {
        singleSelection: false,
        idField: "insuranceId",
        textField: "insuranceName",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.ageGroupSelectSettings = {
        singleSelection: false,
        idField: "serviceAgeGroupId",
        textField: "ageGroupDisplay",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.countySelectSettings = {
        singleSelection: false,
        idField: "countyId",
        textField: "county",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    });

    this.stateOptions = scus.getStates();
  }

  viewOrEditDemographics(): void {

    if (this.provider.providerInfoDto.logo) {
      this.imageBuffer = this.provider.providerInfoDto.logo;
    }

    this.providerName.setValue(this.provider.providerInfoDto.providerName);
    this.website.setValue(this.provider.providerInfoDto.website);
    this.description.setValue(this.provider.providerInfoDto.description);
    this.facebook.setValue(this.provider.providerInfoDto.facebook);
    this.twitter.setValue(this.provider.providerInfoDto.twitter);
    this.linkedin.setValue(this.provider.providerInfoDto.linkedin);
    this.instagram.setValue(this.provider.providerInfoDto.instagram);
    this.youtube.setValue(this.provider.providerInfoDto.youtube);

    this.viewOrEditDemographicsModal.fire();
  }

  unapprovedDemographics(): void {

    if (this.provider.providerInfoDto.modifiedLogo) {
      this.modifiedImageBuffer = this.provider.providerInfoDto.modifiedLogo;
    }

    this.unapprovedDemographicsForm.patchValue({
      providerName: this.provider.providerInfoDto.modifiedProviderName,
      website: this.provider.providerInfoDto.modifiedWebsite,
      description: this.provider.providerInfoDto.modifiedDescription,
      facebook: this.provider.providerInfoDto.modifiedFacebook,
      twitter: this.provider.providerInfoDto.modifiedTwitter,
      linkedin: this.provider.providerInfoDto.modifiedLinkedin,
      instagram: this.provider.providerInfoDto.modifiedInstagram,
      youtube: this.provider.providerInfoDto.modifiedYoutube
    });

    this.unapprovedDemographicsModal.fire();

  }

  submitEditDemographicsForm(): void {
    if (this.editDemographicsForm.valid) {
      const demographicsValues = { ...this.editDemographicsForm.value };
      demographicsValues.logo = this.imageBuffer;
      this.providerService
        .userUpdateProviderDemographicsById(
          this.providerInfoId,
          demographicsValues
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            this.isViewMode = true;
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage =
              "Error when trying to update provider demographics...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }


  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = event => {
        this.imageBuffer = event.target.result;
      };
    }
  }

  openLocationView(event) {
    this.showLocationId = event.currentTarget.dataset.locationid;

    // location name
    this.locationNameEditMode = false;
    this.editLocationNameValue = null;
  }

  cancel(): void {
    Swal.close();
  }

  deleteLocation(event) {
    this.deleteLocationId = event.currentTarget.dataset.locationid;

    this.confirmDeleteLocationAlert.fire();
  }

  deleteEditEmail(event) {
    this.deleteEditEmailId = event.currentTarget.dataset.editemailid;

    this.confirmDeleteEditEmailAlert.fire();
  }

  deleteEditContactNumber(event) {
    this.deleteEditContactNumberId = event.currentTarget.dataset.editnumberid;

    this.confirmDeleteEditContactNumberAlert.fire();
  }

  deleteEditCounty(event) {
    this.deleteEditCountyValue = event.currentTarget.dataset.editcounty;

    this.confirmDeleteEditCountyAlert.fire();
  }

  deleteEditService(event) {
    this.deleteEditServiceIdentifier =
      event.currentTarget.dataset.editidentifier;

    this.confirmDeleteEditServiceAlert.fire();
  }

  confirmDeleteLocation(): void {
    this.locationService
      .userDeleteLocation(this.deleteLocationId)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete location...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  confirmDeleteEditEmail(): void {
    this.locationService
      .userDeleteEmail(this.deleteEditEmailId)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete email...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  confirmDeleteEditContactNumber(): void {
    this.locationService
      .userDeleteContactNumber(this.deleteEditContactNumberId)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete contact number...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  confirmDeleteEditService(): void {
    this.locationService
      .userDeleteService(this.deleteEditServiceIdentifier)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete service...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  editLocationName(event): void {
    this.locationNameEditMode = true;
    this.editLocationNameValue = event.currentTarget.dataset.locationname;
  }

  cancelEditLocationName(): void {
    this.locationNameEditMode = false;
  }

  saveEditLocationName(event): void {
    this.locationService
      .userUpdateLocationName(
        event.currentTarget.dataset.locationid,
        this.editLocationNameValue
      )
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.locationNameEditMode = false;
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to update location name...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  editLocationAddress(event): void {
    this.editAddressId = event.currentTarget.dataset.addressid;
    this.editAddressType = event.currentTarget.dataset.addresstype;
    this.editCity.setValue(event.currentTarget.dataset.city);
    this.editStreet.setValue(event.currentTarget.dataset.street);
    this.editState.setValue(event.currentTarget.dataset.state);
    this.editZip.setValue(event.currentTarget.dataset.zip);

    this.editAddressModal.fire();
  }

  editAddressSubmit(): void {
    if (this.editAddressForm.valid) {
      const {
        editCity,
        editStreet,
        editState,
        editZip
      } = this.editAddressForm.value;

      this.locationService
        .userUpdateLocationAddress(
          this.editAddressId,
          editStreet,
          editCity,
          editState,
          editZip
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = `Error when trying to update ${this.editAddressType} address...`;
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  submitEditEmail(): void {
    if (this.editEmailForm.valid) {
      const { editEmail, editContactType } = this.editEmailForm.value;

      this.locationService
        .userAddEmail(this.showLocationId, editEmail, editContactType)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to add email...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  submitEditContactNumber(): void {
    if (this.editContactNumberForm.valid) {
      const {
        editContactNumber,
        editContactType
      } = this.editContactNumberForm.value;

      this.locationService
        .userAddContactNumber(
          this.showLocationId,
          editContactNumber,
          editContactType
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to add contact number...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  editServiceCountiesFire(): void {
    const editLocation = this.provider.providerLocationDtos.find(
      l => l.locationId === parseInt(this.showLocationId)
    );

    this.editCounties.setValue(editLocation.serviceCounties);

    this.editServiceCountyModal.fire();
  }

  unapprovedCountiesViewFire(): void {
    const currLocation = this.provider.providerLocationDtos.find(
      l => l.locationId === parseInt(this.showLocationId)
    );

    this.unapprovedCounties = currLocation.modifiedCounties;

    this.unapprovedUpdatedServiceCountyModal.fire();
  }

  submitEditServiceCounty(): void {
    if (this.editServiceCountyForm.valid) {
      const countyIds = this.editCounties.value.map(ec => ec.countyId);

      this.locationService
        .userUpdateCounties(this.showLocationId, countyIds)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to update counties...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  submitEditService(): void {
    if (this.editServiceForm.valid) {
      const locationSrvcLinkRaw = this.editServiceForm.value;
      const locationSrvcLink = {
        serviceId: parseInt(locationSrvcLinkRaw.editService),
        internal: locationSrvcLinkRaw.editInternalOnly,
        serviceDeliveryIds: [],
        insuranceIds: [],
        ageGroupIds: [],
        identifier: uuidv4()
      };

      locationSrvcLink.serviceDeliveryIds = locationSrvcLinkRaw.editServiceDeliveries.map(
        serviceDelivery => serviceDelivery.serviceDeliveryId
      );
      locationSrvcLink.insuranceIds = locationSrvcLinkRaw.editInsurances.map(
        insurance => insurance.insuranceId
      );
      locationSrvcLink.ageGroupIds = locationSrvcLinkRaw.editAgeGroups.map(
        ageGroup => ageGroup.serviceAgeGroupId
      );

      const processedLocationSrvcLinks = this.cartesianService
        .cartesian(
          [locationSrvcLink.serviceId],
          [locationSrvcLink.internal],
          [...locationSrvcLink.serviceDeliveryIds],
          [...locationSrvcLink.insuranceIds],
          [...locationSrvcLink.ageGroupIds],
          [locationSrvcLink.identifier]
        )
        .map(e => {
          return {
            serviceId: e[0],
            internal: e[1],
            serviceDeliveryId: e[2],
            insuranceId: e[3],
            ageGroupId: e[4],
            identifier: e[5]
          };
        });

      this.locationService
        .userAddServiceToLocation(
          this.showLocationId,
          processedLocationSrvcLinks
        )
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to add service...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  addLocation(): void {
    this.router.navigate(["../addlocation"], { relativeTo: this.route });
  }

  get editStreet() {
    return this.editAddressForm.get("editStreet");
  }

  get editCity() {
    return this.editAddressForm.get("editCity");
  }

  get editState() {
    return this.editAddressForm.get("editState");
  }

  get editZip() {
    return this.editAddressForm.get("editZip");
  }

  // edit email
  get editEmail() {
    return this.editEmailForm.get("editEmail");
  }

  // edit contact number
  get editContactNumber() {
    return this.editContactNumberForm.get("editContactNumber");
  }

  // edit counties
  get editCounties() {
    return this.editServiceCountyForm.get("editCounties");
  }

  // demographics
  get providerName() {
    return this.editDemographicsForm.get("providerName");
  }

  get website() {
    return this.editDemographicsForm.get("website");
  }

  get description() {
    return this.editDemographicsForm.get("description");
  }

  get facebook() {
    return this.editDemographicsForm.get("facebook");
  }

  get twitter() {
    return this.editDemographicsForm.get("twitter");
  }

  get linkedin() {
    return this.editDemographicsForm.get("linkedin");
  }

  get instagram() {
    return this.editDemographicsForm.get("instagram");
  }

  get youtube() {
    return this.editDemographicsForm.get("youtube");
  }
}
