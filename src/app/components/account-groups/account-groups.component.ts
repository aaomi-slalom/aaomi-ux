import { Component, OnInit, ViewChild } from "@angular/core";
import {
  faSearch,
  faEye,
  faTrashAlt,
  faPlus
} from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { GroupService } from "src/app/services/group/group.service";
import { FormTrimService } from "src/app/services/form-trim/form-trim.service";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { timer } from "rxjs";
import { SwalComponent, SwalPortalTargets } from "@sweetalert2/ngx-sweetalert2";
import { map, flatMap } from "rxjs/operators";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { CommunicationService } from "src/app/services/communication/communication.service";

@Component({
  selector: "app-account-groups",
  templateUrl: "./account-groups.component.html",
  styleUrls: ["./account-groups.component.scss"]
})
export class AccountGroupsComponent implements OnInit {
  createGroupForm: FormGroup;
  searchGroupForm: FormGroup;
  faSearch = faSearch;
  faEye = faEye;
  faTrashAlt = faTrashAlt;
  faPlus = faPlus;
  initialGroups = new Array<any>();
  groups = new Array<any>();
  returnedGroups: Array<any>;
  serverMessage: string;
  errorMessage: string;
  accountGroupDelete: string;
  @ViewChild("infoAlert") infoAlert: SwalComponent;
  @ViewChild("errorAlert") errorAlert: SwalComponent;
  @ViewChild("confirmDeleteAlert") confirmDeleteAlert: SwalComponent;
  wait1Second = timer(1000);

  constructor(
    public readonly swalTargets: SwalPortalTargets,
    private auth: AuthService,
    private groupService: GroupService,
    private formTrimService: FormTrimService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.groupService.getAllGroups().subscribe(groupsRaw => {
      const groups = groupsRaw.map(group => {
        const groupNameDisplay = this.formTrimService.processNamesToDisplay(
          group.accountGroup,
          6
        );

        return { ...group, groupNameDisplay };
      });

      this.initialGroups = groups;
      this.groups = groups;
      this.returnedGroups = this.groups.slice(0, 5);
    });

    this.createGroupForm = this.formBuilder.group({
      accountGroup: ["", [Validators.required]]
    });

    this.searchGroupForm = this.formBuilder.group({
      groupName: ["", []]
    });
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedGroups = this.groups.slice(startItem, endItem);
  }

  searchGroupsByName(searchGroupForm: FormGroup): void {
    const groupName = searchGroupForm.value.groupName.toUpperCase();

    this.groups = this.initialGroups.filter(group =>
      group.accountGroup.includes(groupName)
    );

    this.returnedGroups = this.groups.slice(0, 5);
  }

  openGroupView(event): void {
    this.router.navigate(["/admin/group/", event.currentTarget.dataset.group]);
  }

  deleteGroup(event): void {
    this.accountGroupDelete = event.currentTarget.dataset.group;

    this.confirmDeleteAlert.fire();
  }

  confirmDelete(): void {
    this.groupService
      .deleteGroup(this.accountGroupDelete)
      .pipe(
        map(response => {
          this.serverMessage = response.message;
          this.infoAlert.fire();
        }),
        flatMap(() => this.wait1Second)
      )
      .subscribe(
        () => {
          Swal.close();
          this.ngOnInit();
        },
        error => {
          this.errorMessage = "Error when trying to delete group...";
          this.errorAlert.fire();
          throw error;
        }
      );
  }

  cancelDeleteGroup(): void {
    Swal.close();
  }

  onSubmit(): void {
    const accountGroupProcessed = this.accountGroupProcessed;

    if (this.createGroupForm.valid && !this.groupAlreadyExist) {
      this.groupService
        .createGroup(accountGroupProcessed)
        .pipe(
          map(response => {
            this.serverMessage = response.message;
            this.infoAlert.fire();
          }),
          flatMap(() => this.wait1Second)
        )
        .subscribe(
          () => {
            Swal.close();
            this.ngOnInit();
          },
          error => {
            this.errorMessage = "Error when trying to create group...";
            this.errorAlert.fire();
            throw error;
          }
        );
    }
  }

  get accountGroup() {
    return this.createGroupForm.get("accountGroup");
  }

  get accountGroupProcessed() {
    return (
      "GROUP_" +
      this.accountGroup.value.trim().replace(/\s+/g, "_").toUpperCase()
    );
  }

  get groupAlreadyExist() {
    return this.groups
      .map(group => group.accountGroup)
      .includes(this.accountGroupProcessed);
  }

  get appBgImage() {
    return this.communicationService.getData()["appBackgroundImage"];
  }
}
