import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountGroupsComponent } from './account-groups.component';
import { GroupService } from "src/app/services/group/group.service";
import { Router } from "@angular/router";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from "@angular/forms"


describe('AccountGroupsComponent', () => {
  let component: AccountGroupsComponent;
  let fixture: ComponentFixture<AccountGroupsComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountGroupsComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [GroupService, { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
