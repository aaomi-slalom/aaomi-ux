import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { LandingComponent } from "./landing.component";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { Router } from "@angular/router";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ReactiveFormsModule } from "@angular/forms";
import { SecuredHtmlPipe } from "src/app/pipes/secured-html/secured-html.pipe";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("LandingComponent", () => {
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, { provide: Router, useValue: mockRouter }],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      declarations: [LandingComponent, SecuredHtmlPipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });


});
