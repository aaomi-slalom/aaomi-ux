import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { Router } from "@angular/router";
import { ProviderServiceService } from "src/app/services/provider-service/provider-service.service";
import { ServiceCategoryService } from "src/app/services/service-category/service-category.service";
import { InsuranceService } from "src/app/services/insurance/insurance.service";
import { AgeGroupService } from "src/app/services/age-group/age-group.service";
import { DeliveryService } from "src/app/services/delivery/delivery.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { forkJoin, timer } from "rxjs";
import { MiCountyService } from "src/app/services/mi-county/mi-county.service";
import { CommunicationService } from "src/app/services/communication/communication.service";
import { RegionService } from "src/app/services/region/region.service";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { UsersService } from "src/app/services/users/users.service";
import { flatMap, map } from "rxjs/operators"

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"]
})
export class LandingComponent implements OnInit {
  searchProviderForm: FormGroup;
  serviceOptions = [];
  serviceCategoryOptions = [];
  serviceDeliveryOptions = [];
  insuranceOptions = [];
  ageGroupOptions = [];
  countyOptions = [];
  regionOptions = [];
  categorySelectSettings = {};
  serviceSelectSettings = {};
  deliverySelectSettings = {};
  insuranceSelectSettings = {};
  ageGroupSelectSettings = {};
  countySelectSettings = {};
  regionSelectSettings = {};
  @ViewChild("guestSearchModal") guestSearchModal: SwalComponent;
  serverResponse: any;
  wait1Second = timer(1000);
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private providerServiceService: ProviderServiceService,
    private serviceCategoryService: ServiceCategoryService,
    private insuranceService: InsuranceService,
    private ageGroupService: AgeGroupService,
    private deliveryService: DeliveryService,
    private miCountyService: MiCountyService,
    private regionService: RegionService,
    private communicationService: CommunicationService,
    private router: Router,
    private usersService: UsersService


  ) { }

  ngOnInit(): void {
    if (this.auth.isAuthenticated()) {
      this.router.navigate([""]);
    }

    this.searchProviderForm = this.formBuilder.group({
      providerName: ["", []],
      keyword: ["", []],
      serviceCategories: [[], []],
      services: [[], []],
      serviceDeliveries: [[], []],
      insurances: [[], []],
      ageGroups: [[], []],
      counties: [[], []],
      regions: [[], []]
    });

    const services = this.providerServiceService.getAllPublicServiceOptions();
    const serviceCategories = this.serviceCategoryService.getAllServiceCategoryOptions();
    const serviceDeliveries = this.deliveryService.getAllDeliveryOptions();
    const insurances = this.insuranceService.getAllInsuranceOptions();
    const ageGroups = this.ageGroupService.getAllAgeGroupOptions();
    const miCounties = this.miCountyService.getAllMICountyOptions();
    const regions = this.regionService.getAllRegionOptions();

    forkJoin([
      services,
      serviceCategories,
      serviceDeliveries,
      insurances,
      ageGroups,
      miCounties,
      regions
    ]).subscribe(results => {
      this.serviceOptions = results[0];
      this.serviceCategoryOptions = results[1];
      this.serviceDeliveryOptions = results[2];
      this.insuranceOptions = results[3];
      this.ageGroupOptions = results[4];
      this.countyOptions = results[5];
      this.regionOptions = results[6];

      this.categorySelectSettings = {
        singleSelection: false,
        idField: "serviceCategoryId",
        textField: "serviceCategory",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.serviceSelectSettings = {
        singleSelection: false,
        idField: "serviceId",
        textField: "service",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.deliverySelectSettings = {
        singleSelection: false,
        idField: "serviceDeliveryId",
        textField: "delivery",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.insuranceSelectSettings = {
        singleSelection: false,
        idField: "insuranceId",
        textField: "insuranceName",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.ageGroupSelectSettings = {
        singleSelection: false,
        idField: "serviceAgeGroupId",
        textField: "ageGroupDisplay",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.countySelectSettings = {
        singleSelection: false,
        idField: "countyId",
        textField: "county",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };

      this.regionSelectSettings = {
        singleSelection: false,
        idField: "regionId",
        textField: "region",
        selectAllText: "Select All",
        unSelectAllText: "Unselect All",
        itemsShowLimit: 3,
        allowSearchFilter: true
      };
    });
  }

  guestSearchForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    hasANavigator: [false],
    subscribedToNewsletter: [false],
    acceptTerms: [false, Validators.requiredTrue]
  });


  get f() { return this.guestSearchForm.controls; }

  guestSearch(event): void {
    if (!this.isReturningUser()) {
      this.guestSearchModal.fire()
    }
    else {
      this.submitSearchProvider()
    }
  }

  isReturningUser() {
    var guestUserExpiresAt = localStorage.getItem("guestUserExpiresAt")
    if (guestUserExpiresAt == null) {
      return false
    }
    if (Date.now() < parseInt(guestUserExpiresAt)) {
      return true
    } else {
      localStorage.removeItem("guestUserExpiresAt")
      localStorage.removeItem("guestUserEmail")
      return false
    }
  }

  rememberUser(email: string): void {
    var oneHourInMs = 60 * 60 * 1000;
    var guestUserExpiresAt = Date.now() + oneHourInMs * 24;
    localStorage.setItem("guestUserExpiresAt", guestUserExpiresAt.toString())
    localStorage.setItem("guestUserEmail", email);
  }


  guestSearchSubmit() {
    this.submitted = true;
    if (this.guestSearchForm.valid) {
      const { email, hasANavigator, subscribedToNewsletter} = this.guestSearchForm.value;
      this.usersService
        .saveGuestUser(email, hasANavigator, subscribedToNewsletter)
        .pipe(
          map(response => {
            this.serverResponse = response;
          }),
          flatMap(() => {
            this.rememberUser(email);
            return this.wait1Second;
          })
        )
        .subscribe(
          () => {
            this.communicationService.emitChange();
          }
        )

      this.submitSearchProvider()
    }
  }


  submitSearchProvider(): void {
    this.communicationService.setData(
      "landingSearchForm",
      this.searchProviderForm
    );
    this.router.navigate(["/providers/search"]);
  }


  get technicalSupportNote() {
    return this.communicationService.getData()["appTechnicalSupportNote"];
  }

  get email() {
    return this.guestSearchForm.get("email");
  }

  get hasANavigator() {
    return this.guestSearchForm.get("hasANavigator");
  }

  get subscribedToNewsletter() {
    return this.guestSearchForm.get("subscribedToNewsletter");
  }
  get acceptTerms() {
    return this.guestSearchForm.get("acceptTerms");
  }
}
