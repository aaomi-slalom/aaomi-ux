import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth/auth.service";
import { ProviderAuditService } from "src/app/services/provider-audit/provider-audit.service";
import { ActivatedRoute } from "@angular/router";
import * as moment from "moment";
import { map, flatMap } from "rxjs/operators";

@Component({
  selector: "app-provider-audit",
  templateUrl: "./provider-audit.component.html",
  styleUrls: ["./provider-audit.component.scss"]
})
export class ProviderAuditComponent implements OnInit {
  audits = [];
  providerInfoId;
  moment = moment;

  constructor(
    private auth: AuthService,
    private providerAuditService: ProviderAuditService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.auth.validateCredentialsOnServer();

    this.route.parent.url
      .pipe(
        map(url => (this.providerInfoId = url[2].path)),
        flatMap(() =>
          this.providerAuditService.getAllAuditLogsByProviderInfoId(
            this.providerInfoId
          )
        )
      )
      .subscribe(audits => {
        this.audits = audits;
      });
  }
}
