import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAuditComponent } from './provider-audit.component';

describe('ProviderAuditComponent', () => {
  let component: ProviderAuditComponent;
  let fixture: ComponentFixture<ProviderAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
