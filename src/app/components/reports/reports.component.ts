import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ReportsService } from 'src/app/services/reports/reports.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { map } from "rxjs/operators";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import swal from 'sweetalert2';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})

export class ReportsComponent implements OnInit {
  activityReportForm: FormGroup;
  control: FormControl;
  data = [];
  errorMessage: string;
  @ViewChild("errorAlert") errorAlert: SwalComponent;

  constructor(
    private builder: FormBuilder,
    private reportService: ReportsService) { }

  ngOnInit(): void {
    this.activityReportForm = this.builder.group({
      fromDate: ['', Validators.required],
      toDate: ['', [Validators.required]],
      fileName: ['User_Activity_Report', Validators.required]
     } , {validator: this.checkDates})

  }
  get fileName() {
    return this.activityReportForm.get('fileName');
  }

  checkDates(group: FormGroup) {
    if(group.controls.toDate.value < group.controls.fromDate.value) {
      return { notValid:true }
    }
    return null;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  get fromDate() {
    return this.activityReportForm.get('fromDate');
  }

  get toDate() {
    return this.activityReportForm.get('toDate');
  }

  getUsers(): void {
    if (this.activityReportForm.valid) {
      const { fromDate, toDate } = this.activityReportForm.value;
      var formattedFromDate = this.formatDate(fromDate);
      var formattedToDate = this.formatDate(toDate);
      this.reportService
        .getGuestUsers(formattedFromDate, formattedToDate)
        .subscribe(
          (response) => {
            if(response.length <= 0){ 
              swal.fire({
                icon: 'info',
                text: 'There is no Guest User activity available for the dates entered',
                confirmButtonText: 'OK',
                confirmButtonColor: '#28a745'})
            } else {
              this.reportService.exportToCsv(response, this.activityReportForm.value.fileName,
              ['email', 'createdAt', 'updatedAt', 'hasANavigator', 'subscribedToNewsletter']);
          }
        },
          error => {
            this.errorMessage = "Error when trying to create report...";
            this.errorAlert.fire();
          });
    }
  }

  getUserActivity(): void {
    if (this.activityReportForm.valid) {
      const { fromDate, toDate } = this.activityReportForm.value;
      var formattedFromDate = this.formatDate(fromDate);
      var formattedToDate = this.formatDate(toDate);
      this.reportService
        .getUserActivity(formattedFromDate, formattedToDate)
        .pipe(
          map((response) => {
            if (response.length<= 0){
              swal.fire({
                icon: 'info',
                text: 'There is no Guest User activity available for the dates entered',
                confirmButtonText: 'OK',
                confirmButtonColor: '#28a745'})
          } else {
            this.data = response.map(q => Object.assign({ "createdAt": q.createdAt }, { "userEmail": q.email }, { "hasANavigator": q.hasANavigator }, q.query));
            return this.data;
          }}
          ))
        .subscribe(
          () => {
            this.reportService.exportToCsv(this.data, this.activityReportForm.value.fileName,
              ['createdAt', 'userEmail', 'hasANavigator', 'keyword', 'countyList', 'regionList', 'serviceList', 'ageGroupList', 'categoryList', 'deliveryList', 'insuranceList', 'providerName']);
          },
          error => {
            this.errorMessage = "Error when trying to create report...";
            this.errorAlert.fire();
          });
    }
  }
}
