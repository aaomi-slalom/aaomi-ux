import { Component, OnInit } from "@angular/core";
import { AuthService } from "./services/auth/auth/auth.service";
import { faSearch, faRss } from "@fortawesome/free-solid-svg-icons";
import { faFacebookF, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { faUser } from "@fortawesome/free-regular-svg-icons";
import { BsDropdownConfig } from "ngx-bootstrap/dropdown";
import { CommunicationService } from "./services/communication/communication.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SystemSettingsService } from "./services/system-settings/system-settings.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers: [
    {
      provide: BsDropdownConfig,
      useValue: { isAnimated: true, autoClose: true }
    }
  ]
})
export class AppComponent implements OnInit {
  searchProviderForm: FormGroup;
  collapsed: boolean = true;
  faSearch = faSearch;
  faUser = faUser;
  faRss = faRss;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  searchKeyword: string;
  adminChoicesGroups = [
    [
      { name: "Providers", link: "/navigator-admin/providers" },
      // { name: "Families", link: "#" },
      { name: "Approvals", link: "/approvals" },
      { name: "Reports", link: "/reports" }
    ],
    [
      { name: "Service Categories", link: "/admin/service/categories" },
      { name: "Services", link: "/admin/services" },
      { name: "Insurance", link: "/admin/insurances" },
      { name: "Age Groups", link: "/admin/agegroups" }
    ],
    [
      { name: "Users", link: "/admin/users/view" },
      { name: "Groups", link: "/admin/groups/view" },
      { name: "System Permissions", link: "/admin/permissions/view" },
      { name: "Roles", link: "/admin/roles/view" }
    ],
    [
      { name: "Regions", link: "/regions" },
      { name: "Messaging", link: "/user/messages" },
      { name: "Settings", link: "/admin/settings" },
      { name: "About", link: "/admin/about" }
    ]
  ];
  navigatorChoices = [
    { name: "Providers", link: "/navigator-admin/providers" },
    // { name: "Families", link: "#" },
    { name: "Messaging", link: "/user/messages" },
    { name: "Regions", link: "/regions" },
    { name: "About", link: "/admin/about" },
    { name: "Reports", link: "/reports" }
  ];
  internalChoices = [
    { name: "Messaging", link: "/user/messages" },
    { name: "About", link: "/admin/about" },
    { name: "Reports", link: "/reports" }
  ];

  constructor(
    private auth: AuthService,
    private systemSettingsService: SystemSettingsService,
    private communicationService: CommunicationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.systemSettingsService
      .getCurrentSystemSettings()
      .subscribe(systemSettings => {
        this.communicationService.setData(
          "appBackgroundImage",
          systemSettings.backgroundImage
        );
        this.communicationService.setData(
          "appSearchSupportNote",
          systemSettings.searchSupportNote
        );
        this.communicationService.setData(
          "appTechnicalSupportNote",
          systemSettings.technicalSupportNote
        );
        this.communicationService.setData(
          "appDisclaimer",
          systemSettings.disclaimer
        );
      });

    this.searchProviderForm = this.formBuilder.group({
      providerName: ["", []],
      keyword: ["", []],
      serviceCategories: [[], []],
      services: [[], []],
      serviceDeliveries: [[], []],
      insurances: [[], []],
      ageGroups: [[], []],
      counties: [[], []],
      regions: [[], []]
    });
  }

  logout(): void {
    this.auth.logout();
  }

  searchProvidersByKeyword(): void {
    this.keyword.setValue(this.searchKeyword);

    this.communicationService.setData(
      "landingSearchForm",
      this.searchProviderForm
    );

    this.router.navigate(["/providers/search"]);
  }

  get isLoggedIn(): boolean {
    return this.auth.isAuthenticated();
  }

  get hasAdminGroup(): boolean {
    return this.auth.hasGroups(["GROUP_ADMIN"]);
  }

  get hasNavigatorGroup(): boolean {
    return this.auth.hasGroups(["GROUP_NAVIGATOR"]);
  }

  get hasInternalGroup(): boolean {
    return this.auth.hasGroups(["GROUP_INTERNAL"]);
  }

  get keyword() {
    return this.searchProviderForm.get("keyword");
  }

  get disclaimer() {
    return this.communicationService.getData()["appDisclaimer"];
  }
}
