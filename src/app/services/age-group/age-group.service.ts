import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AgeGroupService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllAgeGroups(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/agegroups`, {
      headers: reqHeader
    });
  }

  public getAllAgeGroupOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/agegroup/options`
    );
  }

  public createAgeGroup(ageStart: number, ageEnd: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/agegroup`,
      { ageStart, ageEnd },
      {
        headers: reqHeader
      }
    );
  }

  public updateAgeGroup(
    ageGroupId: number,
    ageStart: number,
    ageEnd: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/agegroup/${ageGroupId}`,
      { ageStart, ageEnd },
      {
        headers: reqHeader
      }
    );
  }

  public deleteAgeGroup(ageGroupId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/agegroup/${ageGroupId}`,
      {
        headers: reqHeader
      }
    );
  }
}
