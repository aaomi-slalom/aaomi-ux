import { TestBed } from '@angular/core/testing';
import { AgeGroupService } from './age-group.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('AgeGroupService', () => {
  let service: AgeGroupService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AgeGroupService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(AgeGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
