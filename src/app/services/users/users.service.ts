import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class UsersService {
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getUsersByUsernameAndAccountGroup(
    usernameSearch: string,
    accountGroupSearch: string,
    currentPage: number,
    pageSize: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/view`,
      { usernameSearch, accountGroupSearch, currentPage, pageSize },
      { headers: reqHeader }
    );
  }

  public deleteUser(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/delete/${username}`,
      { headers: reqHeader }
    );
  }

  public createUser(
    username: string,
    email: string,
    password: string,
    confirmPassword: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/create`,
      { username, email, password, confirmPassword },
      { headers: reqHeader }
    );
  }

  public viewSpecificUser(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/user/${username}`,
      { headers: reqHeader }
    );
  }

  public disableUser(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/user/${username}/disable`,
      { headers: reqHeader }
    );
  }

  public enableUser(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/user/${username}/enable`,
      { headers: reqHeader }
    );
  }

  public changeUserPassword(
    username: string,
    newPassword: string,
    confirmNewPassword: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/password/change`,
      { username, newPassword, confirmNewPassword },
      { headers: reqHeader }
    );
  }

  public changeUserEmail(username: string, email: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/user/email/update`,
      { username, email },
      { headers: reqHeader }
    );
  }

  public changeUserUsername(
    username: string,
    newUsername: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/users/user/username/update`,
      { username, newUsername },
      { headers: reqHeader }
    );
  }

  public deleteGroupFromUser(
    username: string,
    accountGroup: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/${username}/group/${accountGroup}/delete`,
      { headers: reqHeader }
    );
  }

  public deletePermissionFromUser(
    username: string,
    permission: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/${username}/permission/${permission}/delete`,
      { headers: reqHeader }
    );
  }

  public getAvailableGroupsToAdd(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/${username}/groups/notin/view`,
      { headers: reqHeader }
    );
  }

  public getAvailablePermissionsToAdd(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/${username}/permissions/notin/view`,
      { headers: reqHeader }
    );
  }

  public addGroupToUser(
    username: string,
    accountGroup: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/group/add`,
      { username, accountGroup },
      { headers: reqHeader }
    );
  }

  public addPermissionToUser(
    username: string,
    permission: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/user/permission/add`,
      { username, permission },
      { headers: reqHeader }
    );
  }

  public saveGuestUser(
    email: string,
    hasANavigator: boolean,
    subscribedToNewsletter: boolean
  ): Observable<any> {
    const reqHeader = this.auth.createHeaderWithoutToken();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/guest-users`,
      { email, hasANavigator, subscribedToNewsletter },
      { headers: reqHeader }
    );
  }
}
