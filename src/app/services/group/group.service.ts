import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class GroupService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAppAdminGroup(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/admingroup`, {
      headers: reqHeader
    });
  }

  public getAllGroups(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/groups/view`, {
      headers: reqHeader
    });
  }

  public getAllGroupNames(): Observable<string[]> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/groups/names/view`,
      {
        headers: reqHeader
      }
    );
  }

  public getGroupUsers(
    accountGroup: string,
    currentPage: number,
    pageSize: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/users/view`,
      {
        accountGroup,
        currentPage,
        pageSize
      },
      {
        headers: reqHeader
      }
    );
  }

  public getGroupPermissions(accountGroup: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/permissions/view/${accountGroup}`,
      {
        headers: reqHeader
      }
    );
  }

  public addPermissionToGroup(accountGroup: string, permission: string) {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/permission/add`,
      { accountGroup, permission },
      { headers: reqHeader }
    );
  }

  public deletePermissionFromGroup(accountGroup: string, permission: string) {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/permission/delete/${accountGroup}/${permission}`,
      {
        headers: reqHeader
      }
    );
  }

  public createGroup(accountGroup: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/create`,
      { accountGroup },
      { headers: reqHeader }
    );
  }

  public deleteGroup(accountGroup: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/delete/${accountGroup}`,
      { headers: reqHeader }
    );
  }

  public deleteGroupUser(
    accountGroup: string,
    username: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/group/user/delete/${accountGroup}/${username}`,
      { headers: reqHeader }
    );
  }
}
