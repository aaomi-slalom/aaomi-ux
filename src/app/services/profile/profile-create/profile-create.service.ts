import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../../environments/environment";
import { AuthService } from "../../auth/auth/auth.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProfileCreateService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public createUserProfile(
    firstName: string,
    lastName: string,
    contactNumber: string,
    website: string,
    image: string,
    bio: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/userprofile/create`,
      { firstName, lastName, contactNumber, website, image, bio },
      { headers: reqHeader }
    );
  }
}
