import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ProfileCreateService } from "./profile-create.service";
import { AuthService } from '../../auth/auth/auth.service';
import { Router } from '@angular/router';

describe("ProfileCreateService", () => {
  let service: ProfileCreateService;
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ProfileCreateService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
