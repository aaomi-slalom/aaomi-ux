import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/auth/auth.service";
import { environment as ENV } from "../../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProfileShowService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getProfile(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/userprofile/show`, {
      headers: reqHeader
    });
  }
}
