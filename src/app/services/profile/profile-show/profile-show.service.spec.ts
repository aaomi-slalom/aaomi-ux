import { TestBed } from '@angular/core/testing';

import { ProfileShowService } from './profile-show.service';
import { AuthService } from '../../auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProfileShowService', () => {
  let service: ProfileShowService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ProfileShowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
