import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/auth/auth.service";
import { environment as ENV } from "../../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProfileEditService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public editUserProfile(
    firstName: string,
    lastName: string,
    contactNumber: string,
    website: string,
    image: string,
    bio: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/userprofile/edit`,
      { firstName, lastName, contactNumber, website, image, bio },
      { headers: reqHeader }
    );
  }
}
