import { TestBed, async } from "@angular/core/testing";

import { ProfileEditService } from "./profile-edit.service";
import { AuthService } from '../../auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("ProfileEditService", () => {
  let service: ProfileEditService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ProfileEditService);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
