import { TestBed, async } from "@angular/core/testing";

import { ProfileGuardService } from "./profile-guard.service";
import { AuthService } from '../../auth/auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("ProfileGuardService", () => {
  let service: ProfileGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ProfileGuardService);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
