import { Injectable, OnInit } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { ProfileShowService } from "../profile-show/profile-show.service";

@Injectable({
  providedIn: "root"
})
export class ProfileGuardService implements CanActivate {
  constructor(
    private profileShowService: ProfileShowService,
    private router: Router
  ) {}

  canActivate(): boolean {
    this.profileShowService.getProfile().subscribe(response => {
      if (!response) {
        this.router.navigate(["profile/create"]);
        return false;
      }
    });

    return true;
  }
}
