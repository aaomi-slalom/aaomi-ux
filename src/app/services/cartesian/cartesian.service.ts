import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CartesianService {
  constructor() {}

  cartesian(...args) {
    let r = [];
    let max = args.length - 1;

    const helper = (arr, i) => {
      for (let j = 0, l = args[i].length; j < l; j++) {
        let a = arr.slice();
        a.push(args[i][j]);
        if (i == max) r.push(a);
        else helper(a, i + 1);
      }
    };

    helper([], 0);
    return r;
  }
}
