import { TestBed } from '@angular/core/testing';

import { CartesianService } from './cartesian.service';

describe('CartesianService', () => {
  let service: CartesianService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CartesianService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
