import { TestBed } from '@angular/core/testing';
import { ProviderWaitTimeService } from './provider-wait-time.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ProviderWaitTimeService', () => {
  let service: ProviderWaitTimeService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderWaitTimeService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderWaitTimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
