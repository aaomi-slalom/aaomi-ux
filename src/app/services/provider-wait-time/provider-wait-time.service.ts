import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ProviderWaitTimeService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllWaitTimesByProviderInfoId(
    providerInfoId: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/waittimes`,
      { headers: reqHeader }
    );
  }

  public getAllWaitTimeOptionsByProviderInfoId(
    providerInfoId: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/waittime/options`,
      { headers: reqHeader }
    );
  }

  public createNewWaitTime(
    providerInfoId: number,
    waitTimeLinkId: string,
    waitTimeType: string,
    title: string,
    waitTime: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/waittime`,
      { providerInfoId, waitTimeLinkId, waitTimeType, title, waitTime },
      { headers: reqHeader }
    );
  }

  public updateExistingWaitTime(
    waitTimeId: number,
    title: string,
    waitTime: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/waittimes/${waitTimeId}`,
      { title, waitTime },
      { headers: reqHeader }
    );
  }

  public deleteExistingWaitTime(waitTimeId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/waittime/${waitTimeId}`,
      { headers: reqHeader }
    );
  }
}
