import { TestBed } from '@angular/core/testing';
import { ProviderAccountService } from './provider-account.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ProviderAccountService', () => {
  let service: ProviderAccountService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderAccountService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderAccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
