import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProviderAccountService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllAccountsByProviderInfoId(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}/accounts`,
      { headers: reqHeader }
    );
  }

  public linkProviderAccount(providerInfoId, username): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}/account/${username}`,
      {
        headers: reqHeader
      }
    );
  }

  public unlinkProviderAccount(providerInfoId, username): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}/account/${username}`,
      {
        headers: reqHeader
      }
    );
  }
}
