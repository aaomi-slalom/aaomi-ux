import { Injectable, ViewChild } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class SystemSettingsService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getCurrentSystemSettings(): Observable<any> {
    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/systemsettings`);
  }

  public updateBackgroundImage(updateString): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/systemsettings/backgroundimage`,
      { updateString },
      { headers: reqHeader }
    );
  }

  public updateSearchSupportNote(updateString: String): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/systemsettings/searchsupportnote`,
      { updateString },
      { headers: reqHeader }
    );
  }

  public updateTechnicalSupportNote(updateString: String): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/systemsettings/technicalsupportnote`,
      { updateString },
      { headers: reqHeader }
    );
  }

  public updateDisclaimer(updateString: String): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/systemsettings/disclaimer`,
      { updateString },
      { headers: reqHeader }
    );
  }
}
