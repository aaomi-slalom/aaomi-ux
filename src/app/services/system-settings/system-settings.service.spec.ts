import { TestBed } from '@angular/core/testing';
import { SystemSettingsService } from './system-settings.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('SystemSettingsService', () => {
  let service: SystemSettingsService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SystemSettingsService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(SystemSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
