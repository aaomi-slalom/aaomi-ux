import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class InsuranceService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllInsurances(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/insurances`, {
      headers: reqHeader
    });
  }

  public getAllInsuranceOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/insurance/options`
    );
  }

  public createInsurance(
    insuranceName: string,
    website: string,
    contactNumber: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/insurance`,
      { insuranceName, website, contactNumber },
      {
        headers: reqHeader
      }
    );
  }

  public updateInsurance(
    insuranceId: number,
    insuranceName: string,
    website: string,
    contactNumber: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/insurance/${insuranceId}`,
      { insuranceName, website, contactNumber },
      {
        headers: reqHeader
      }
    );
  }

  public deleteInsurance(insuranceId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/insurance/${insuranceId}`,
      {
        headers: reqHeader
      }
    );
  }
}
