import { TestBed } from '@angular/core/testing';
import { MiCountyService } from './mi-county.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('MiCountyService', () => {
  let service: MiCountyService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MiCountyService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(MiCountyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
