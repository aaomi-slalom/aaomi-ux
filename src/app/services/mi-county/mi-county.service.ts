import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MiCountyService {
  constructor(private http: HttpClient) {}

  public getAllMICountyOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/micounty/options`
    );
  }
}
