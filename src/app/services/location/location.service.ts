import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class LocationService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public deleteLocation(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}`,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorDeleteLocation(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}`,
      {
        headers: reqHeader
      }
    );
  }

  public userDeleteLocation(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}`,
      {
        headers: reqHeader
      }
    );
  }

  public updateLocationName(locationId, locationName): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}`,
      {
        locationName
      },
      {
        headers: reqHeader
      }
    );
  }

  public navigatorUpdateLocationName(
    locationId,
    locationName
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}`,
      {
        locationName
      },
      {
        headers: reqHeader
      }
    );
  }

  public userUpdateLocationName(locationId, locationName): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}`,
      {
        locationName
      },
      {
        headers: reqHeader
      }
    );
  }

  public updateLocationAddress(
    addressId,
    street: string,
    city: string,
    state: string,
    zip: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/address/${addressId}`,
      {
        street,
        city,
        state,
        zip
      },
      {
        headers: reqHeader
      }
    );
  }

  public navigatorUpdateLocationAddress(
    addressId,
    street: string,
    city: string,
    state: string,
    zip: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/address/${addressId}`,
      {
        street,
        city,
        state,
        zip
      },
      {
        headers: reqHeader
      }
    );
  }

  public userUpdateLocationAddress(
    addressId,
    street: string,
    city: string,
    state: string,
    zip: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/address/${addressId}`,
      {
        street,
        city,
        state,
        zip
      },
      {
        headers: reqHeader
      }
    );
  }

  public addEmail(locationId, email, contactType): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}/email/add`,
      {
        email,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public navigatorAddEmail(locationId, email, contactType): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}/email/add`,
      {
        email,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public userAddEmail(locationId, email, contactType): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}/email/add`,
      {
        email,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public deleteEmail(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/email/${emailId}`,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorDeleteEmail(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/email/${emailId}`,
      {
        headers: reqHeader
      }
    );
  }

  public userDeleteEmail(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/email/${emailId}`,
      {
        headers: reqHeader
      }
    );
  }

  public addContactNumber(locationId, number, contactType): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}/contactnumber/add`,
      {
        number,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public navigatorAddContactNumber(
    locationId,
    number,
    contactType
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}/contactnumber/add`,
      {
        number,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public userAddContactNumber(
    locationId,
    number,
    contactType
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}/contactnumber/add`,
      {
        number,
        contactType
      },
      {
        headers: reqHeader
      }
    );
  }

  public deleteContactNumber(contactNumberId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/contactnumber/${contactNumberId}`,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorDeleteContactNumber(contactNumberId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/contactnumber/${contactNumberId}`,
      {
        headers: reqHeader
      }
    );
  }

  public userDeleteContactNumber(contactNumberId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/contactnumber/${contactNumberId}`,
      {
        headers: reqHeader
      }
    );
  }

  public updateCounties(locationId, countyIds: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}/counties`,
      {
        countyIds
      },
      {
        headers: reqHeader
      }
    );
  }

  public navigatorUpdateCounties(
    locationId,
    countyIds: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}/counties`,
      {
        countyIds
      },
      {
        headers: reqHeader
      }
    );
  }

  public userUpdateCounties(locationId, countyIds: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}/counties`,
      {
        countyIds
      },
      {
        headers: reqHeader
      }
    );
  }

  public deleteService(identifier: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/service/${identifier}`,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorDeleteService(identifier: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/service/${identifier}`,
      {
        headers: reqHeader
      }
    );
  }

  public userDeleteService(identifier: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/service/${identifier}`,
      {
        headers: reqHeader
      }
    );
  }

  public addServiceToLocation(
    locationId,
    createLocationSrvcLinkRequests
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}/service/add`,
      createLocationSrvcLinkRequests,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorAddServiceToLocation(
    locationId,
    createLocationSrvcLinkRequests
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/location/${locationId}/service/add`,
      createLocationSrvcLinkRequests,
      {
        headers: reqHeader
      }
    );
  }

  public userAddServiceToLocation(
    locationId,
    createLocationSrvcLinkRequests
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/location/${locationId}/service/add`,
      createLocationSrvcLinkRequests,
      {
        headers: reqHeader
      }
    );
  }

  public addLocationToProvider(
    providerInfoId,
    createLocationRequest
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}/location/add`,
      createLocationRequest,
      {
        headers: reqHeader
      }
    );
  }

  public navigatorAddLocationToProvider(
    providerInfoId,
    createLocationRequest
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/${providerInfoId}/location/add`,
      createLocationRequest,
      {
        headers: reqHeader
      }
    );
  }

  public userAddLocationToProvider(
    providerInfoId,
    createLocationRequest
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/${providerInfoId}/location/add`,
      createLocationRequest,
      {
        headers: reqHeader
      }
    );
  }

  public updateLocationInternalStatus(locationId, internal): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/${locationId}/internal`,
      { internal },
      {
        headers: reqHeader
      }
    );
  }

  public updateGroupedServicesInternalStatus(
    identifier,
    internal
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/location/service/${identifier}/internal`,
      { internal },
      {
        headers: reqHeader
      }
    );
  }
}
