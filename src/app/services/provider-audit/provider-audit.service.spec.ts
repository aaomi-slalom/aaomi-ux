import { TestBed } from '@angular/core/testing';
import { ProviderAuditService } from './provider-audit.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ProviderAuditService', () => {
  let service: ProviderAuditService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderAuditService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderAuditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
