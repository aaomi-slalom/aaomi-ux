import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProviderAuditService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllAuditLogsByProviderInfoId(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/audit`,
      { headers: reqHeader }
    );
  }
}
