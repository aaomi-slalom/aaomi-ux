import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class PermissionService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllPermissions(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/permissions/view`,
      {
        headers: reqHeader
      }
    );
  }
}
