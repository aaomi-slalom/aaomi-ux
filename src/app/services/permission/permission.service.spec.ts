import { TestBed } from '@angular/core/testing';
import { PermissionService } from './permission.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('PermissionService', () => {
  let service: PermissionService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PermissionService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(PermissionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
