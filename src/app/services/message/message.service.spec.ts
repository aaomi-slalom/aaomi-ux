import { TestBed } from "@angular/core/testing";
import { MessageService } from "./message.service";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe("MessageService", () => {
  let service: MessageService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MessageService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(MessageService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
