import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class MessageService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllSentMessagesByUsername(
    username: string,
    currentPage: number,
    pageSize: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/messages/sent`,
      { username, currentPage, pageSize },
      {
        headers: reqHeader,
      }
    );
  }

  public getAllReceivedMessagesByUsername(
    username: string,
    currentPage: number,
    pageSize: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/messages/received`,
      { username, currentPage, pageSize },
      { headers: reqHeader }
    );
  }

  public getAllAccountGroups(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/messages/accountgroups`,
      { headers: reqHeader }
    );
  }

  public getAccountGroupsExternalUser(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/view/accountgroups/external`,
      { headers: reqHeader }
    );
  }

  public saveMessage(
    sender: string,
    subject: string,
    message: string,
    recipients: string[]
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/message`,
      { sender, subject, message, recipients },
      { headers: reqHeader }
    );
  }

  public saveMessageExternalUser(
    subject: string,
    message: string,
    accountGroups: []
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/message/externaluser`,
      { subject, message, accountGroups },
      { headers: reqHeader }
    );
  }

  public viewSpecificMessage(messageId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/message/${messageId}`,
      { headers: reqHeader }
    );
  }

  public deleteReceivedMessage(
    messageId: number,
    username: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/${username}/receivedmessage/${messageId}`,
      { headers: reqHeader }
    );
  }

  public deleteSentMessage(
    messageId: number,
    username: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/${username}/sentmessage/${messageId}`,
      { headers: reqHeader }
    );
  }
}
