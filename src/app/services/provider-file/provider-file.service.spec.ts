import { TestBed } from '@angular/core/testing';
import { ProviderFileService } from './provider-file.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ProviderFileService', () => {
  let service: ProviderFileService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderFileService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
