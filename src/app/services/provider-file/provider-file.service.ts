import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProviderFileService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllFilesByProviderInfoId(providerInfoId): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/files`
    );
  }

  public createProviderFile(providerInfoId, fileName, file): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/file`,
      {
        providerInfoId,
        fileName,
        file
      },
      {
        headers: reqHeader
      }
    );
  }

  public deleteProviderFileById(providerFileId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/file/${providerFileId}`,
      {
        headers: reqHeader
      }
    );
  }
}
