import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class FormTrimService {
  constructor() {}

  public trimForm(formValues): any {
    Object.keys(formValues).map(
      key => (formValues[key] = formValues[key].trim())
    );

    return formValues;
  }

  public processNamesToDisplay(dataName: string, index: number): string {
    const rawDataArr = dataName.slice(index).split("_");
    const dataDisplay = rawDataArr
      .map(word => word[0].toUpperCase() + word.slice(1).toLowerCase())
      .join(" ");

    return dataDisplay;
  }
}
