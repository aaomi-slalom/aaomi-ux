import { TestBed } from '@angular/core/testing';
import { QueryService } from './query.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('QueryService', () => {
  let service: QueryService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [QueryService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(QueryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
