import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class QueryService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public queryProviderInfosByConditions(queryProviderRequest): Observable<any> {
    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/providers/search`,
      queryProviderRequest
    );
  }

  public generateQueryReport(queryProviderRequest): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/providers/search/report`,
      queryProviderRequest,
      { headers: reqHeader }
    );
  }

  public internalQueryProviderInfosByConditions(
    queryProviderRequest
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/providers/search/internal`,
      queryProviderRequest,
      { headers: reqHeader }
    );
  }

  public getPublicLocationByLocationId(locationId): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/public/provider/${locationId}`
    );
  }

  public getInternalLocationByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/internal/provider/${locationId}`,
      { headers: reqHeader }
    );
  }
}
