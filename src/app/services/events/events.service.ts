import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth/auth/auth.service";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EventsService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllEvents(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/events`
    );
  }

  public getAllMyEvents(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/events/currentuser`,
      { headers: reqHeader }
    );
  }

  public createEvent(
    title: string,
    description: string,
    website: string,
    cost: string,
    free: boolean,
    image: string,
    startDateTime: Date,
    endDateTime: Date,
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/event`,
      { title, description, website, cost, free, image, startDateTime, endDateTime },
      { headers: reqHeader }
    );
  }

  public updateEvent(
    eventId: BigInteger,
    title: string,
    description: string,
    website: string,
    cost: string,
    free: boolean,
    image: string,
    startDate: Date,
    endDate: Date,
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/event/${eventId}`,
      { title, description, website, cost, free, image, startDate, endDate },
      { headers: reqHeader }
    );
  }

  public deleteEventByEventId(eventId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/event/${eventId}`,
      { headers: reqHeader }
    );
  }

  public getEventByEventId(eventId): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/event/${eventId}`
    );
  }
}
