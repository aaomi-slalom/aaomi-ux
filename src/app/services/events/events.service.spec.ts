import { TestBed } from "@angular/core/testing";
import { EventsService } from "./events.service";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe("EventsService", () => {
  let service: EventsService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventsService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(EventsService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
