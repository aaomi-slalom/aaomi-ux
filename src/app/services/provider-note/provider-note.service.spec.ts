import { TestBed } from "@angular/core/testing";
import { ProviderNoteService } from "./provider-note.service";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe("ProviderNotesService", () => {
  let service: ProviderNoteService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderNoteService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderNoteService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
