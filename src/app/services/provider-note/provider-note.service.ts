import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth/auth/auth.service";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";


@Injectable({
  providedIn: "root",
})
export class ProviderNoteService {


  constructor(private auth: AuthService, private http: HttpClient) { }

  public getAllNotesByProviderInfoId(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/notes`,
      { headers: reqHeader }
    );
  }

  public getAllNoteOptionsByProviderInfoId(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/${providerInfoId}/note/options`,
      { headers: reqHeader }
    );
  }

  public createNote(
    providerInfoId,
    noteLinkId: string,
    type: string,
    note: string,
    title: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/note`,
      { providerInfoId, noteLinkId, type, note, title },
      { headers: reqHeader }
    );
  }

  public deleteNoteByNoteId(noteId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/note/${noteId}`,
      { headers: reqHeader }
    );
  }

  public editNote(
    noteId: any,
    title: string,
    note: string,

  ): Observable<any> {
    const reqHeader = this.auth.createHeader();
    return this.http.put<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/note/${noteId}`,
      { noteId, note, title },
      { headers: reqHeader }
    );
  }
}


