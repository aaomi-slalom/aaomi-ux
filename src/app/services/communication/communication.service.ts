import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CommunicationService {
  private emitChangeSource: Subject<any> = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();
  private data: any = {};

  constructor() {}

  public emitChange(): void {
    this.emitChangeSource.next();
  }

  public setData(option, value): void {
    this.data[option] = value;
  }

  public getData(): any {
    return this.data;
  }

  public hasData(key): boolean {
    return key in this.data;
  }

  public clear(): void {
    const appDisclaimer = this.data["appDisclaimer"];
    const appSearchSupportNote = this.data["appSearchSupportNote"];
    const appTechnicalSupportNote = this.data["appTechnicalSupportNote"];
    const appBackgroundImage = this.data["appBackgroundImage"];

    this.data = {
      appDisclaimer,
      appSearchSupportNote,
      appTechnicalSupportNote,
      appBackgroundImage
    };
  }
}
