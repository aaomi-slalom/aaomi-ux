import { TestBed } from '@angular/core/testing';
import { DeliveryService } from './delivery.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('DeliveryService', () => {
  let service: DeliveryService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DeliveryService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(DeliveryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
