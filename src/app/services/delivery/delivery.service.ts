import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class DeliveryService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllDeliveryOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/delivery/options`
    );
  }
}
