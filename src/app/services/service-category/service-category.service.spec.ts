import { TestBed } from '@angular/core/testing';
import { ServiceCategoryService } from './service-category.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('ServiceCategoryService', () => {
  let service: ServiceCategoryService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ServiceCategoryService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ServiceCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
