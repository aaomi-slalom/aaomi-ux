import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ServiceCategoryService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllServiceCategories(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/categories`,
      {
        headers: reqHeader,
      }
    );
  }

  public getAllServiceCategoryOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/category/options`
    );
  }

  public createServiceCategory(
    category: string,
    description: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/category`,
      { category, description },
      { headers: reqHeader }
    );
  }

  public updateServiceCategory(serviceCategoryId, category, description) {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/category/${serviceCategoryId}`,
      { category, description },
      { headers: reqHeader }
    );
  }

  public deleteServiceCategory(serviceCategoryId) {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/category/${serviceCategoryId}`,
      { headers: reqHeader }
    );
  }
}
