import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class RoleService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllRoles(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/roles/view`, {
      headers: reqHeader
    });
  }

  public updateDefaultGroupOnRole(
    role: string,
    defaultGroup: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/role/group/update`,
      { role, defaultGroup },
      { headers: reqHeader }
    );
  }
}
