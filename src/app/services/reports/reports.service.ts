import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth/auth/auth.service";
import { environment as ENV } from "../../../environments/environment";
import { Observable } from "rxjs";

const CSV_EXTENSION = '.csv';
const CSV_TYPE = 'text/plain;charset=utf-8';

@Injectable({
  providedIn: 'root'
})

export class ReportsService {


  constructor(private auth: AuthService, private http: HttpClient) { }

  public getUserActivity(
    formattedFromDate: string,
    formattedToDate: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/guest-users/queries?fromDate=${formattedFromDate}&toDate=${formattedToDate}`,
      { headers: reqHeader }

    );
  }

  public getGuestUsers(
    formattedFromDate: string,
    formattedToDate: string
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/guest-users?fromDate=${formattedFromDate}&toDate=${formattedToDate}`,
      { headers: reqHeader }

    );
  }

  public exportToCsv(rows: object[], fileName: string, columns?: string[]): string {
    if (!rows || !rows.length) {
      return;
    }

    const separator = ',';
    const keys = Object.keys(rows[0]).filter(k => {
      if (columns?.length) {
        return columns.includes(k);
      } else {
        return true;
      }
    });
    const csvContent =
      keys.join(separator) +
      '\n' +
      rows.map(row => {
        return keys.map(k => {
          let cell = row[k] === null || row[k] === undefined ? '' : row[k];
          cell = cell instanceof Date
            ? cell.toLocaleString()
            : cell.toString().replace(/"/g, '""');
          if (cell.search(/("|,|\n)/g) >= 0) {
            cell = `"${cell}"`;
          }
          return cell;
        }).join(separator);
      }).join('\n');
    this.saveAsFile(csvContent, `${fileName}${CSV_EXTENSION}`, CSV_TYPE);
  }

  private saveAsFile(buffer: any, fileName: string, fileType: string): void {
    const data: Blob = new Blob([buffer], { type: fileType });
    FileSaver.saveAs(data, fileName);
  }
}
