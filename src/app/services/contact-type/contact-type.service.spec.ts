import { TestBed } from '@angular/core/testing';
import { ContactTypeService } from './contact-type.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('ContactTypeService', () => {
  let service: ContactTypeService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ContactTypeService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ContactTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
