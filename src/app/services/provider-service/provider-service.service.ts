import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProviderServiceService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllServices(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/admin/services`, {
      headers: reqHeader
    });
  }

  public getAllNonInternalServices(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/public/services`,
      {
        headers: reqHeader
      }
    );
  }

  public getAllServiceOptions(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/service/options`,
      {
        headers: reqHeader
      }
    );
  }

  public getAllPublicServiceOptions(): Observable<any> {
    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/service/options`
    );
  }

  public createService(
    category: string,
    service: string,
    description: string,
    internal: boolean
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service`,
      {
        category,
        service,
        description,
        internal
      },
      {
        headers: reqHeader
      }
    );
  }

  public updateService(
    serviceId: number,
    category: string,
    service: string,
    description: string,
    internal: boolean
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/${serviceId}`,
      {
        category,
        service,
        description,
        internal
      },
      {
        headers: reqHeader
      }
    );
  }

  public deleteService(serviceId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/service/${serviceId}`,
      {
        headers: reqHeader
      }
    );
  }

  public getUserToLinkWithProvider(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/account/${username}`,
      {
        headers: reqHeader
      }
    );
  }
}
