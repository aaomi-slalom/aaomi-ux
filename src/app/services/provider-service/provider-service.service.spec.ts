import { TestBed } from '@angular/core/testing';
import { ProviderServiceService } from './provider-service.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('ProviderServiceService', () => {
  let service: ProviderServiceService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderServiceService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
