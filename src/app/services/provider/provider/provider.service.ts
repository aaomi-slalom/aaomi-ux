import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment as ENV } from "../../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ProviderService {
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getProviderReport(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/internal/providers/report`,
      {
        headers: reqHeader
      }
    );
  }

  public getStandaloneReport(queryProviderRequest): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/internal/providers/standalone/report`,
      queryProviderRequest,
      { headers: reqHeader }
    );
  }

  public getContactReport(queryProviderRequest): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/internal/providers/contact/report`,
      queryProviderRequest,
      { headers: reqHeader }
    );
  }

  public getGrantReport(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/internal/providers/grant/report`,
      {
        headers: reqHeader
      }
    );
  }

  public getAllProvidersByProviderName(
    providerNameSearch: string,
    currentPage: number,
    pageSize: number
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/providers/view`,
      { providerNameSearch, currentPage, pageSize },
      {
        headers: reqHeader
      }
    );
  }

  public getProviderById(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}`,
      {
        headers: reqHeader
      }
    );
  }

  public getUserProviderById(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/${providerInfoId}`,
      {
        headers: reqHeader
      }
    );
  }

  public createProvider(provider: any): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider`,
      provider,
      { headers: reqHeader }
    );
  }

  public navigatorCreateProvider(provider: any): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider`,
      provider,
      { headers: reqHeader }
    );
  }

  public createOwnProvider(provider: any): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/profile`,
      provider,
      { headers: reqHeader }
    );
  }

  public updateProviderDemographicsById(
    providerInfoId,
    demographics
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/demographics/${providerInfoId}`,
      demographics,
      { headers: reqHeader }
    );
  }

  public navigatorUpdateProviderDemographicsById(
    providerInfoId,
    demographics
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/navigator/provider/demographics/${providerInfoId}`,
      demographics,
      { headers: reqHeader }
    );
  }

  public userUpdateProviderDemographicsById(
    providerInfoId,
    demographics
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/user/provider/demographics/${providerInfoId}`,
      demographics,
      { headers: reqHeader }
    );
  }

  public deleteProviderById(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/${providerInfoId}`,
      { headers: reqHeader }
    );
  }

  public checkProviderName(providerName: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/providernames/${providerName}`,
      { headers: reqHeader }
    );
  }

  public checkUpdateProviderName(
    providerNameUpdate: string,
    providerInfoId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/provider/providernames/${providerNameUpdate}/${providerInfoId}`,
      { headers: reqHeader }
    );
  }

  public checkHasProviderProfile(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/check/provider/profile`,
      { headers: reqHeader }
    );
  }

  public getAllAccountProviderProfiles(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/provider/profiles`, {
      headers: reqHeader
    });
  }
}
