import { TestBed } from "@angular/core/testing";
import { ProviderService } from "./provider.service";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe("ProviderService", () => {
  let service: ProviderService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
