import { Injectable } from "@angular/core";
import { ProviderService } from "../provider/provider.service";
import { CanActivate, Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class ProviderGuardService implements CanActivate {
  constructor(
    private providerService: ProviderService,
    private router: Router
  ) {}

  canActivate(): boolean {
    this.providerService.checkHasProviderProfile().subscribe(response => {
      if (!response.hasProviderProfile) {
        this.router.navigate(["provider/create"]);
        return false;
      }
    });

    return true;
  }
}
