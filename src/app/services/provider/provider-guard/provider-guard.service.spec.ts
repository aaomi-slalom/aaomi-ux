import { TestBed } from '@angular/core/testing';
import { ProviderGuardService } from './provider-guard.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ProviderGuardService', () => {
  let service: ProviderGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderGuardService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
