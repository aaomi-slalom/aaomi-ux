import { TestBed } from '@angular/core/testing';
import { ProviderCreateGuardService } from './provider-create-guard.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";

describe('ProviderCreateGuardService', () => {
  let service: ProviderCreateGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProviderCreateGuardService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ProviderCreateGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
