import { Injectable } from "@angular/core";
import { ProviderService } from "../provider/provider.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class ProviderCreateGuardService {
  constructor(
    private providerService: ProviderService,
    private router: Router
  ) {}

  canActivate(): boolean {
    this.providerService.checkHasProviderProfile().subscribe(response => {
      if (response.hasProviderProfile) {
        this.router.navigate(["providers/show"]);
        return false;
      }
    });

    return true;
  }
}
