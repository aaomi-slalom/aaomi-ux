import { Injectable } from "@angular/core";
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApprovalService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllUnapprovedAccounts(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/approval/accounts`, {
      headers: reqHeader
    });
  }

  public approveAccountByUsername(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/account/${username}`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveAccountByUsername(username: string): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/disapproval/account/${username}`,
      {
        headers: reqHeader
      }
    );
  }

  public getAllUnapprovedProviders(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/approval/providers`, {
      headers: reqHeader
    });
  }

  public getAllUnapprovedModifiedProviders(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/unapproved/modified/providers`,
      {
        headers: reqHeader
      }
    );
  }

  public approveProviderByProviderInfoId(providerInfoId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/provider/${providerInfoId}`,
      {
        headers: reqHeader
      }
    );
  }

  public approveModifiedProviderDemographicsByProviderInfoId(
    providerInfoId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/demographics/${providerInfoId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveModifiedProviderDemographicsByProviderInfoId(
    providerInfoId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/demographics/${providerInfoId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveDeleteLocationByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/${locationId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveDeleteLocationByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/${locationId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveModifiedLocationNameByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/name/${locationId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveModifiedLocationNameByLocationId(
    locationId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/name/${locationId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveModifiedAddressByAddressId(addressId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/address/${addressId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveModifiedAddressByAddressId(addressId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/address/${addressId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveAddedEmailByEmailId(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/added/location/email/${emailId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public approveDeleteEmailByEmailId(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/email/${emailId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveDeleteEmailByEmailId(emailId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/email/${emailId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveAddedContactNumberByContactNumberId(
    contactNumberId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/added/location/contactnumber/${contactNumberId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public approveDeleteContactNumberByContactNumberId(
    contactNumberId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/contactnumber/${contactNumberId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveDeleteContactNumberByContactNumberId(
    contactNumberId
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/contactnumber/${contactNumberId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveAddedServiceByIdentifier(identifier): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/added/location/service/${identifier}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public approveDeleteServiceByIdentifier(identifier): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/service/${identifier}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveDeleteServiceByIdentifier(identifier): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/delete/location/service/${identifier}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public approveModifiedCountiesByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/counties/${locationId}/approve`,
      {
        headers: reqHeader
      }
    );
  }

  public disapproveModifiedCountiesByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/modified/location/counties/${locationId}/disapprove`,
      {
        headers: reqHeader
      }
    );
  }

  public getAllUnapprovedLocations(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/approval/locations`, {
      headers: reqHeader
    });
  }

  public getUnapprovedLocationById(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/unapproved/location/${locationId}`,
      {
        headers: reqHeader
      }
    );
  }

  public approveLocationByLocationId(locationId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/approval/location/${locationId}`,
      {
        headers: reqHeader
      }
    );
  }
}
