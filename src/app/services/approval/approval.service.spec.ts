import { TestBed } from '@angular/core/testing';
import { ApprovalService } from './approval.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ApprovalService', () => {
  let service: ApprovalService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApprovalService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ApprovalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
