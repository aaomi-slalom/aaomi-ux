import { TestBed } from '@angular/core/testing';

import { UsPhoneNumberService } from './us-phone-number.service';

describe('UsPhoneNumberService', () => {
  let service: UsPhoneNumberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsPhoneNumberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
