import { Injectable } from "@angular/core";
import PhoneNumber from "awesome-phonenumber";

@Injectable({
  providedIn: "root"
})
export class UsPhoneNumberService {
  constructor() {}

  formatPhoneNumber(number) {
    const phoneNumber = new PhoneNumber(number, "US");
    return phoneNumber.getNumber("national");
  }
}
