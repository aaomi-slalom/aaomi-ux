import { TestBed } from "@angular/core/testing";

import { AuthService } from "../auth/auth.service";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';

describe("AuthService", () => {
  let service: AuthService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: Router, useValue: mockRouter }],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(AuthService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
