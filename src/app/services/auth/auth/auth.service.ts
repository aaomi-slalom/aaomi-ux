import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { JwtHelperService } from "@auth0/angular-jwt";
import { environment as ENV } from "../../../../environments/environment";
import { CommunicationService } from "../../communication/communication.service";
import { Observable, interval, BehaviorSubject, Subscription } from "rxjs";
import { AuthResponseDto } from "src/app/dtos/AuthResponseDto";
import Swal from "sweetalert2";
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})
export class AuthService {
  jwtHelper: JwtHelperService = new JwtHelperService();
  private isLoginAboutToExpireSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isLoginAboutToExpire$: Observable<boolean> = this.isLoginAboutToExpireSubject.asObservable();
  private intervalSubscription: Subscription;
  private isAuthTimerStarted: boolean;

  constructor(
    private http: HttpClient,
    private router: Router,
    private communicationService: CommunicationService,
  ) { }

  public checkUsername(username: string): Observable<any> {
    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/username/availability`,
      {
        username
      }
    );
  }

  public checkEmailAndEnabled(email: string): Observable<any> {
    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/email/enabled/availability`,
      {
        email
      }
    );
  }

  public register(
    role: string,
    username: string,
    email: string,
    password: string,
    confirmPassword: string
  ): Observable<any> {
    return this.http.post<any>(`${ENV.SPRING_BOOT_API_URL}/register`, {
      role,
      username,
      email,
      password,
      confirmPassword
    });
  }

  public resendVerification(email: string): Observable<any> {
    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/register/verification/resend`,
      {
        email
      }
    );
  }

  public login(
    usernameOrEmail: string,
    password: string
  ): Observable<AuthResponseDto> {
    this.communicationService.clear();
    return this.http.post<AuthResponseDto>(
      `${ENV.SPRING_BOOT_API_URL}/authenticate`,
      {
        usernameOrEmail,
        password,
      }
    );
  }

  public sendResetRequest(email: string): Observable<any> {
    return this.http.post<any>(`${ENV.SPRING_BOOT_API_URL}/password/reset`, {
      email
    });
  }

  public verifyResetLink(id: number, token: string): Observable<any> {
    return this.http.post<any>(`${ENV.SPRING_BOOT_API_URL}/reset/verify`, {
      id,
      token
    });
  }

  public updatePassword(
    email: string,
    password: string,
    confirmPassword: string,
    accountId: number,
    passwordResetToken: string
  ): Observable<any> {
    return this.http.post<any>(`${ENV.SPRING_BOOT_API_URL}/password/update`, {
      email,
      password,
      confirmPassword,
      accountId,
      passwordResetToken
    });
  }

  public changeAccountPassword(
    oldPassword: string,
    newPassword: string,
    confirmNewPassword: string
  ): Observable<any> {
    const reqHeader = this.createHeader();
    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/password/change`,
      { oldPassword, newPassword, confirmNewPassword },
      { headers: reqHeader }
    );
  }

  private getToken(): string {
    const validToken = [
      localStorage.getItem("token"),
      sessionStorage.getItem("token")
    ].filter(token => token != null)[0];

    return validToken;
  }

  private getTokenExp(): number {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);

    return tokenPayload.exp;
  }

  public logout(): void {
    sessionStorage.removeItem("token");
    localStorage.removeItem("token");
    this.communicationService.clear();
    this.router.navigate(["login"]);
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();

    return !this.jwtHelper.isTokenExpired(token);
  }

  public validateCredentialsOnServer(): void {
    const reqHeader = this.createHeader();

    this.http
      .get<any>(`${ENV.SPRING_BOOT_API_URL}/credentials/validate`, {
        headers: reqHeader
      })
      .subscribe(
        () => { },
        error => {
          this.logout();
          throw error;
        }
      );
  }

  private continueSession(): void {
    const reqHeader = this.createHeader();
    this.http
      .get<any>(`${ENV.SPRING_BOOT_API_URL}/refresh-token`, {
        headers: reqHeader
      })
      .pipe(
        tap(
          data => (data),
        )
      )
      .subscribe(
        data => {
          sessionStorage.setItem("token", data.jwt)
        },
        error => {
          this.logout();
          throw error;
        })
    this.startTimer();
  }


  public hasPermissions(expectedPermissions: string[]): boolean {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);

    let permissionChecker = (userPermissions, expectedPermissions) =>
      expectedPermissions.every(permission =>
        userPermissions.includes(permission)
      );

    return permissionChecker(tokenPayload.permissions, expectedPermissions);
  }

  public getRole(): string {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);
    return tokenPayload.roles[0];
  }

  public getUsername(): string {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);

    return tokenPayload.sub;
  }

  public hasRoles(expectedRoles: string[]): boolean {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);

    let roleChecker = (userRoles, expectedRoles) =>
      expectedRoles.every(role => userRoles.includes(role));

    return roleChecker(tokenPayload.roles, expectedRoles);
  }

  public hasGroups(expectedGroups: string[]): boolean {
    const token = this.getToken();
    const tokenPayload = this.jwtHelper.decodeToken(token);

    let groupChecker = (userGroups, expectedGroups) =>
      expectedGroups.every(group => userGroups.includes(group));

    return groupChecker(tokenPayload.groups, expectedGroups);
  }

  public createHeader(): HttpHeaders {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT",
      Authorization: "Bearer " + this.getToken()
    });
  }

  public createHeaderWithoutToken(): HttpHeaders {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });
  }

  public returningUser() {
    return false;
  }

  public getIsAuthTimerStarted() {
    return this.isAuthTimerStarted;
  }

  public startTimer() {
    this.intervalSubscription = interval(10000).subscribe(num => {
      if (this.getToken() != null) {
        this.isAuthTimerStarted = true;
        var now = Math.round((new Date).getTime() / 1000);
        var delta = this.getTokenExp() - now;
        if (delta <= 300 && delta >= 0) {
          swal.fire({
            icon: 'warning',
            title: 'Your Session Will Expire Soon',
            text: 'please click refresh to remain logged in',
            confirmButtonText: 'Refresh',
            confirmButtonColor: '#28a745',
            showCancelButton: true,
            cancelButtonText: 'Logout',

          }).then((result) => {
            if (result.value) {
              this.intervalSubscription.unsubscribe();

              this.continueSession();
              swal.fire({
                text: 'Your session has been refreshed',
                showConfirmButton: false,
                timer: 2000
              })

            } else if (result.dismiss === Swal.DismissReason.cancel) {
              this.logout();
            }
          })

          this.isLoginAboutToExpireSubject.next(false);
        }
        else if (delta <= 0) {
          swal.close();
          swal.fire({
            text: 'Your session has ended',
          })
          sessionStorage.removeItem("token");
          localStorage.removeItem("token");
          this.isLoginAboutToExpireSubject.next(false);
        }
        else {
          this.isLoginAboutToExpireSubject.next(false);
        }

      }
    });
  }
}

