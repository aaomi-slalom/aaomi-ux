import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class GroupGuardService {
  constructor(private auth: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedGroups = route.data.expectedGroups;

    if (!this.auth.isAuthenticated() || !this.auth.hasGroups(expectedGroups)) {
      this.auth.logout();
      return false;
    }
    
    if (!this.auth.getIsAuthTimerStarted()) {
      this.auth.startTimer();
    }

    return true;
  }
}
