import { TestBed } from '@angular/core/testing';
import { GroupGuardService } from './group-guard.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('GroupGuardService', () => {
  let service: GroupGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GroupGuardService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(GroupGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
