import { state } from "@angular/animations";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class PermissionGuardService implements CanActivate {
  static Router: any;
  constructor(private auth: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const expectedPermissions = route.data.expectedPermissions;

    if (
      !this.auth.isAuthenticated() ||
      !this.auth.hasPermissions(expectedPermissions)
    ) {
      this.auth.logout();
      return false;
    }

    if (!this.auth.getIsAuthTimerStarted()) {
      this.auth.startTimer();
    }
    console.log("permission service state.url: ", state.url)
    this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
    return true;
  }
}
