import { TestBed, async } from "@angular/core/testing";

import { PermissionGuardService } from "../permission-guard/permission-guard.service";
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("PermissionGuardService", () => {
  let service: PermissionGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(PermissionGuardService);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
