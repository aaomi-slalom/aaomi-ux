import { TestBed } from "@angular/core/testing";

import { AuthGuardService } from "./auth-guard.service";
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("AuthGuardService", () => {
  let service: AuthGuardService;

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(AuthGuardService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
