import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class RoleGuardService {
  constructor(private auth: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRoles = route.data.expectedRoles;

    if (!this.auth.isAuthenticated() || !this.auth.hasRoles(expectedRoles)) {
      this.auth.logout();
      return false;
    }

    // do we still need this?
    if (!this.auth.getIsAuthTimerStarted()) {
      this.auth.startTimer();
    }

    return true;
  }
}
