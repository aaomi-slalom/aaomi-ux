import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RoleGuardService } from "./role-guard.service";
import { Router } from '@angular/router';

describe("RoleGuardService", () => {
  let service: RoleGuardService;
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Router, useValue: mockRouter },
      ],
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(RoleGuardService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
