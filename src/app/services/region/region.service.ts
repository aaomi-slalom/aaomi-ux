import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class RegionService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getAllRegionOptions(): Observable<any> {
    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/region/options`);
  }

  public getExistingRegions(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(`${ENV.SPRING_BOOT_API_URL}/region/view`, {
      headers: reqHeader
    });
  }

  public getAvailableCountyOptionsForNewRegion(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/region/view/counties`,
      { headers: reqHeader }
    );
  }

  public getAvailableCountyOptions(regionId): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/region/${regionId}/view/counties`,
      { headers: reqHeader }
    );
  }

  public createNewRegion(region: String, countyDtos: []): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/region/save`,
      { region, countyDtos },
      {
        headers: reqHeader
      }
    );
  }

  public updateExistingRegion(
    regionId: number,
    region: String,
    countyDtos: []
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/region/${regionId}/update`,
      { region, countyDtos },
      {
        headers: reqHeader
      }
    );
  }

  public deleteRegion(regionId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/region/delete/${regionId}`,
      {
        headers: reqHeader
      }
    );
  }
}
