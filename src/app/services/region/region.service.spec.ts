import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router";
import { RegionService } from "./region.service";

describe("RegionServiceService", () => {
  let service: RegionService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegionService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(RegionService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
