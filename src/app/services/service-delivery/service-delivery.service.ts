import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment as ENV } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ServiceDeliveryService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public getExistingServiceDeliveries(): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.get<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/delivery/options`,
      {
        headers: reqHeader,
      }
    );
  }

  public createNewServiceDelivery(delivery: String): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.post<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/delivery/create`,
      { delivery },
      {
        headers: reqHeader,
      }
    );
  }

  public updateExistingServiceDelivery(
    serviceDeliveryId: number,
    delivery: String
  ): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.patch<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/delivery/${serviceDeliveryId}/update`,
      { delivery },
      { headers: reqHeader }
    );
  }

  public deleteServiceDelivery(serviceDeliveryId: number): Observable<any> {
    const reqHeader = this.auth.createHeader();

    return this.http.delete<any>(
      `${ENV.SPRING_BOOT_API_URL}/admin/provider/delivery/${serviceDeliveryId}/delete`,
      {
        headers: reqHeader,
      }
    );
  }
}
