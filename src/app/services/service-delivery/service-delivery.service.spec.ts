import { TestBed } from '@angular/core/testing';
import { ServiceDeliveryService } from './service-delivery.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from "@angular/router"

describe('ServiceDeliveryService', () => {
  let service: ServiceDeliveryService;

  const mockRouter = {
    navigate: jasmine.createSpy("navigate"),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ServiceDeliveryService, { provide: Router, useValue: mockRouter }]
    });
    service = TestBed.inject(ServiceDeliveryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
