import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RegistrationComponent } from "./components/registration/registration.component";
import { RegistrationVerificationComponent } from "./components/registration-verification/registration-verification.component";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { PasswordResetComponent } from "./components/password-reset/password-reset.component";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ProfileCreateComponent } from "./components/profile-create/profile-create.component";
import { LandingComponent } from "./components/landing/landing.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ProviderCreateComponent } from "./components/provider-create/provider-create.component";
import { ChangePasswordComponent } from "./components/change-password/change-password.component";
import { ProfileShowComponent } from "./components/profile-show/profile-show.component";
import { ProfileEditComponent } from "./components/profile-edit/profile-edit.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BsDatepickerModule, DatepickerModule } from "ngx-bootstrap/datepicker";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { UserRolesComponent } from "./components/user-roles/user-roles.component";
import { SystemPermissionsComponent } from "./components/system-permissions/system-permissions.component";
import { AccountGroupsComponent } from "./components/account-groups/account-groups.component";
import { GroupUsersComponent } from "./components/group-users/group-users.component";
import { GroupPermissionsComponent } from "./components/group-permissions/group-permissions.component";
import { GroupHomeViewComponent } from "./components/group-home-view/group-home-view.component";
import { UsersComponent } from "./components/users/users.component";
import { UsersUserComponent } from "./components/users-user/users-user.component";
import { ServiceCategoriesComponent } from "./components/service-categories/service-categories.component";
import { ProviderServicesComponent } from "./components/provider-services/provider-services.component";
import { InsurancesComponent } from "./components/insurances/insurances.component";
import { AgeGroupsComponent } from "./components/age-groups/age-groups.component";
import { ProvidersComponent } from "./components/providers/providers.component";
import { ProviderInfoCreateComponent } from "./components/provider-info-create/provider-info-create.component";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { ProvidersProviderHomeComponent } from "./components/providers-provider-home/providers-provider-home.component";
import { ProvidersProviderInfoShowComponent } from "./components/providers-provider-info-show/providers-provider-info-show.component";
import { ProviderNotesComponent } from "./components/provider-notes/provider-notes.component";
import { EventsComponent } from "./components/events/events.component";
import { EventViewComponent } from "./components/events/event-view.component";
import { UpcomingEventsComponent } from "./components/events/upcoming-events.component";
import { ProviderFilesComponent } from "./components/provider-files/provider-files.component";
import { PublicQueryComponent } from "./components/public-query/public-query.component";
import { SystemSettingsComponent } from "./components/system-settings/system-settings.component";
import { ProvidersProviderLocationAddComponent } from "./components/providers-provider-location-add/providers-provider-location-add.component";
import { UserProviderShowComponent } from "./components/user-provider-show/user-provider-show.component";
import { UserProvidersHomeComponent } from "./components/user-providers-home/user-providers-home.component";
import { UserProvidersComponent } from "./components/user-providers/user-providers.component";
import { RegionsComponent } from "./components/regions/regions.component";
import { ProviderAuditComponent } from "./components/provider-audit/provider-audit.component";
import { ProviderLinkAccountsComponent } from "./components/provider-link-accounts/provider-link-accounts.component";
import { ServiceDeliveriesComponent } from "./components/service-deliveries/service-deliveries.component";
import { ApprovalsHomeComponent } from "./components/approvals-home/approvals-home.component";
import { MessagingComponent } from "./components/messaging/messaging.component";
import { QueryLocationShowComponent } from "./components/query-location-show/query-location-show.component";
import { UserProviderLocationAddComponent } from "./components/user-provider-location-add/user-provider-location-add.component";
import { ProviderWaitTimesComponent } from "./components/provider-wait-times/provider-wait-times.component";
import { ApprovalModifiedProviderComponent } from "./components/approval-modified-provider/approval-modified-provider.component";
import { PublicHolderComponent } from "./components/public-holder/public-holder.component";
import { AboutPageComponent } from "./components/about-page/about-page.component";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { SecuredHtmlPipe } from './pipes/secured-html/secured-html.pipe';
import { ReportsComponent } from './components/reports/reports.component';
import { MatTableExporterModule } from "mat-table-exporter";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    RegistrationVerificationComponent,
    LoginComponent,
    HomeComponent,
    PasswordResetComponent,
    ForgotPasswordComponent,
    ProfileCreateComponent,
    LandingComponent,
    ProviderCreateComponent,
    ChangePasswordComponent,
    ProfileShowComponent,
    ProfileEditComponent,
    NotFoundComponent,
    UserRolesComponent,
    SystemPermissionsComponent,
    AccountGroupsComponent,
    GroupUsersComponent,
    GroupPermissionsComponent,
    GroupHomeViewComponent,
    UsersComponent,
    UsersUserComponent,
    ServiceCategoriesComponent,
    ProviderServicesComponent,
    InsurancesComponent,
    AgeGroupsComponent,
    ProvidersComponent,
    ProviderInfoCreateComponent,
    ProvidersProviderHomeComponent,
    ProvidersProviderInfoShowComponent,
    ProviderNotesComponent,
    EventsComponent,
    EventViewComponent,
    UpcomingEventsComponent,
    ProviderFilesComponent,
    PublicQueryComponent,
    SystemSettingsComponent,
    ProvidersProviderLocationAddComponent,
    UserProviderShowComponent,
    UserProvidersHomeComponent,
    UserProvidersComponent,
    RegionsComponent,
    ProviderAuditComponent,
    ProviderLinkAccountsComponent,
    ServiceDeliveriesComponent,
    MessagingComponent,
    QueryLocationShowComponent,
    ApprovalsHomeComponent,
    UserProviderLocationAddComponent,
    ProviderWaitTimesComponent,
    ApprovalModifiedProviderComponent,
    PublicHolderComponent,
    AboutPageComponent,
    SecuredHtmlPipe,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    FontAwesomeModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    AngularEditorModule,
    MatTableExporterModule,
    MatIconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatFormFieldModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    MatProgressBarModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule
    
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
