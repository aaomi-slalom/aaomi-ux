export interface AuthResponseDto {
    jwt: string;
}
