// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SPRING_BOOT_API_URL: "http://localhost:8080/api",
  EMPTY_PROFILE_IMG_URL:
    "https://raisinconservation.weebly.com/uploads/1/7/1/6/17161926/personplaceholder-lightgreen.png",
  EMPTY_LOGO_IMG_URL:
    "https://pngimage.net/wp-content/uploads/2018/06/your-logo-png-9.png",
  DEFAULT_BG_IMG: "/assets/images/bg1.jpg"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
