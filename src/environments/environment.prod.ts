export const environment = {
  production: true,
  SPRING_BOOT_API_URL: "http://b6712b08895816430810a6cd973f8ce9-1793963706.us-east-2.elb.amazonaws.com/api",
  EMPTY_PROFILE_IMG_URL:
    "https://raisinconservation.weebly.com/uploads/1/7/1/6/17161926/personplaceholder-lightgreen.png",
  EMPTY_LOGO_IMG_URL:
    "https://pngimage.net/wp-content/uploads/2018/06/your-logo-png-9.png",
  DEFAULT_BG_IMG: "/assets/images/bg1.jpg"
};
