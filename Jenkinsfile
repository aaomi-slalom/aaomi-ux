def MVN_PROJECT_VERSION = ''

pipeline{
	options{
		disableConcurrentBuilds()
		buildDiscarder(logRotator(numToKeepStr: '10'))
	}
	
	environment{
		APP_VERSION = "1.0"
		PROJECT_NAME = "Autism Alliance of Michigan - Core"
		PATH="$PATH:/apps/apache-maven-3.6.3/bin:/apps/gradle-6.3/bin:/apps/jdk1.8.0_241/bin:/apps/node-v12.16.2-linux-x64/bin"
	}
	
	agent any
	
	stages{
	
		stage('BUILD'){
			steps{
			 sh '''
			    npm install --save-dev @angular-devkit/build-angular
			 	ng build --prod=true --base-href .
			 '''		
			}
		}
		
		stage('PACKAGE'){
			steps{
			 sh '''
			   mkdir -p target
			   cd dist
			   tar cvzf ../target/aaomi-ux.tar.gz aaomi-ux
			 '''		
			}
			post{
			    success{
		            archiveArtifacts artifacts: 'target/*.tar.gz', fingerprint: true
			    }
				always{
					emailext body: '''${SCRIPT, template="groovy-html.template"}''',
					mimeType: 'text/HTML',
					to: 'sreenidhi.gundlupet@slalom.com',
					subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
				}
			}
		}

		stage('Deploy'){
			when {
			   not {
				   branch 'master'
			   }
			}
		    steps{
				timeout(time:5, unit:'MINUTES'){
					input(message: "Do you want to Deploy?")
				}
				sh '''
				    cp target/aaomi-ux.tar.gz /apps/apache-tomcat-9.0.34/webapps/.
					cd /apps/apache-tomcat-9.0.34/webapps/
					rm -rf aaomi-ux
					tar -zxvf aaomi-ux.tar.gz
					rm aaomi-ux.tar.gz
					cd /apps/apache-tomcat-9.0.34/bin
					sh shutdown.sh
					sh startup.sh
					
				'''
			}
			post{
				always{
					emailext body: '''${SCRIPT, template="groovy-html.template"}''',
					mimeType: 'text/HTML',
					to: 'sreenidhi.gundlupet@slalom.com',
					subject: "Successfully Deployed Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
				}
			}
		}
		
		stage('Deploy PROD'){
			when {
			      branch 'master'
			}
		    steps{
				timeout(time:5, unit:'MINUTES'){
					input(message: "Do you want to Deploy?")
				}
				sh '''
					ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 /apps/ux/apache-tomcat-9/bin/shutdown.sh
				    ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 rm -rf /apps/ux/apache-tomcat-9/webapps/aaomi-ux
					scp -i ~/.ssh/id_rsa target/aaomi-ux.tar.gz ubuntu@54.81.120.47:/apps/ux/apache-tomcat-9/webapps/.
					ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 tar -zxvf /apps/ux/apache-tomcat-9/webapps/aaomi-ux.tar.gz -C /apps/ux/apache-tomcat-9/webapps/
					ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 rm /apps/ux/apache-tomcat-9/webapps/aaomi-ux.tar.gz
					ssh -i ~/.ssh/id_rsa ubuntu@54.81.120.47 /apps/ux/apache-tomcat-9/bin/startup.sh
					
				'''
			}
			post{
				always{
					emailext body: '''${SCRIPT, template="groovy-html.template"}''',
					mimeType: 'text/HTML',
					to: 'sreenidhi.gundlupet@slalom.com',
					subject: "Successfully Deployed Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
				}
			}
		}
	}
	post{
		always{
			cleanWs()
			dir("${env.WORKSPACE}@tmp"){
				deleteDir()
			}
		}
	}
}
